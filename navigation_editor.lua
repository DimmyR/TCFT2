--[[

Navigation computer route editor

]]


TCFT2.PloziNav.Editor = TCFT2.PloziNav.Editor or {}
TCFT2.PloziNav.Editor.Data = nil
TCFT2.PloziNav.Editor.EditMode = false
TCFT2.PloziNav.Editor.EditPath = {}
TCFT2.PloziNav.Editor.CurrentSystem = 1
TCFT2.PloziNav.Editor.CurrentSector = 1

TCFT2.PloziNav.Editor.MapMode = 0 -- 0 is u	niversemap 1 is sector map

-- Display editor
TCFT2.PloziNav.Editor.ShowEditor = function()
	
	if (TCFT2.PloziNav.Editor.Data == nil) then
		TCFT2.PloziNav.Editor.LoadData()
	end
	TCFT2.PloziNav.UI.EditDialog:popup(iup.CENTER, iup.CENTER)
end

-- Load map
TCFT2.PloziNav.Editor.LoadMap = function()
	local mappath, clickid
	if (TCFT2.PloziNav.Editor.MapMode == 0) then
		--TCFT2.PloziNav.UI.EditDialog:map()
		TCFT2.PloziNav.UI.navmap:loadmap(1, "lua/maps/universemap.lua", 0)
		clickid = TCFT2.PloziNav.Editor.CurrentSystem - 1
	else
		mappath = string.format("lua/maps/system%02dmap.lua", TCFT2.PloziNav.Editor.CurrentSystem)
		--TCFT2.PloziNav.UI.EditDialog:map()
		TCFT2.PloziNav.UI.navmap:loadmap(2, mappath, TCFT2.PloziNav.Editor.CurrentSystem - 1)
		clickid = TCFT2.PloziNav.Editor.CurrentSector
		-- TCFT2.PloziNav.UI.navmap
	end
	TCFT2.PloziNav.UI.navmap.clickedid = clickid
	TCFT2.PloziNav.UI.navmap.currentid = GetCurrentSectorid()
end

-- Zoom button clicked
TCFT2.PloziNav.Editor.Zoom = function(btn)
	if (TCFT2.PloziNav.Editor.MapMode==1) then
		TCFT2.PloziNav.Editor.MapMode = 0
		btn.title = "Zoom to System"
		TCFT2.PloziNav.UI.Savebtn.active="NO"
		TCFT2.PloziNav.Editor.EditPath = {}
		-- TCFT2.PloziNav.UI.navmap.zoom = 1
	else
		TCFT2.PloziNav.Editor.MapMode = 1
		btn.title = "Zoom to Universe"
		-- TCFT2.PloziNav.UI.navmap.zoom = 10
	end
	TCFT2.PloziNav.Editor.LoadMap()
end

-- Save Route to db...
TCFT2.PloziNav.Editor.SaveRouteToDb = function()
	local from = TCFT2.PloziNav.Editor.EditPath[1]
	local to = TCFT2.PloziNav.Editor.EditPath[#TCFT2.PloziNav.Editor.EditPath]
	local newRoute = {}
	for i,val in ipairs(TCFT2.PloziNav.Editor.EditPath) do
		if (i>1) then
			table.insert(newRoute, val)
		end
	end
	TCFT2.PloziNav.Routes[from.."-"..to] = newRoute
	TCFT2.PloziNav.Editor.LoadData()
	TCFT2.PloziNav.Editor.LoadSystemRoutes()
end

-- Save Route button pressed
TCFT2.PloziNav.Editor.SaveRoute = function(btn)
	local from = TCFT2.PloziNav.Editor.EditPath[1]
	local to = TCFT2.PloziNav.Editor.EditPath[#TCFT2.PloziNav.Editor.EditPath]
	if (TCFT2.PloziNav.Routes[from.."-"..to] ~= nil) then
		TCFT2.ConfirmDialog("Confirm save", 
			"Route already exists - replace?",
			"Yes",
			TCFT2.PloziNav.Editor.SaveRouteToDb,
			"No",
			nil
		)
	else
		TCFT2.PloziNav.Editor.SaveRouteToDb()
	end
end

-- Mouse over a sector or system
TCFT2.PloziNav.Editor.Map_Mouseover = function(index, str)
	local note = ""
	local id = tonumber(index) or index
	local system
	local conqueringfaction
	local desc = ""
	local system_notes_heading = '\n[Sector Notes]\n'

	if (TCFT2.PloziNav.Editor.MapMode == 0) then
		if SystemNotes[id+1] and #SystemNotes[id+1].name > 0  then -- system
			-- need to format for system text, not \n terminated
			note = '\n'..system_notes_heading..(SystemNotes[id+1].name or "")
		end
	elseif (TCFT2.PloziNav.Editor.MapMode == 1) then
		system = GetSystemID(id)
		if SystemNotes[system] and SystemNotes[system][id] then-- sector
			note = system_notes_heading..(SystemNotes[system][id]:gsub("\\n","\n") or "")
		end
		note = (GetBotSightedInfoForSector(id) or "")..note
		conqueringfaction = GetConqueredStatus(id)
	end
	if conqueringfaction then
		desc = "Currently controlled by "..FactionArticle[conqueringfaction].." "..FactionNameFull[conqueringfaction].."\n\n"..(str and string.gsub(str, "|", "\n") or "")..note
	else
		desc = (str and string.gsub(str, "|", "\n") or "")..note
	end
	TCFT2.PloziNav.Editor.SectorInfo.value = desc
	-- desc.index = index
end

TCFT2.PloziNav.Editor.Map_Click = function(index, modifiers)
	if (TCFT2.PloziNav.Editor.MapMode == 0) then
		TCFT2.PloziNav.Editor.CurrentSystem = index + 1
		TCFT2.PloziNav.Editor.LoadSystemRoutes()
	else 
		TCFT2.PloziNav.Editor.CurrentSector = index
		if string.byte(modifiers, 1) == iup.K_S then
			-- Shift-Click
			table.insert(TCFT2.PloziNav.Editor.EditPath, index)
			TCFT2.PloziNav.UI.Savebtn.active = "YES"
		else
			-- Normal click
			TCFT2.PloziNav.Editor.EditPath = { index }
			TCFT2.PloziNav.UI.Savebtn.active = "NO"
		end
		TCFT2.PloziNav.UI.navmap:setpath(GetFullPath(TCFT2.PloziNav.Editor.EditPath[1], TCFT2.PloziNav.Editor.EditPath))
	end
end

-- Load current navigation data
TCFT2.PloziNav.Editor.LoadData = function()
	TCFT2.PloziNav.Editor.Data = {}
	for system,system_data in pairs(TCFT2.PloziNav.Routes) do
		local pos = string.find(system, '-', 1)
		local from = tonumber(string.sub(system, 1, pos-1)) or -1
		local to = tonumber(string.sub(system, pos+1)) or -1
		local sysid = GetSystemID(from)
		local data = {}
		data.desc = ShortLocationStr(from) .. " -> " .. ShortLocationStr(to)
		data.from = from
		data.to = to
		data.data = system_data
		TCFT2.PloziNav.Editor.Data[sysid] = TCFT2.PloziNav.Editor.Data[sysid] or {}
		table.insert(TCFT2.PloziNav.Editor.Data[sysid], data)
 	end
	local cmp = function(a,b)
		local ax1 = AbbrLocationStr(a.from)
		local ax2 = AbbrLocationStr(a.to)
		local bx1 = AbbrLocationStr(b.from)
		local bx2 = AbbrLocationStr(b.to)
		if (ax1==bx1) then
			return ax2 < bx2
		end
		return ax1 < bx1
	end
	for sid,sysdata in pairs(TCFT2.PloziNav.Editor.Data) do
		table.sort(sysdata, cmp)
	end
end

-- Load routes for selected system - TCFT2.PloziNav.
TCFT2.PloziNav.Editor.LoadSystemRoutes = function()
	TCFT2.PloziNav.UI.RouteList[1] = nil
	local num = 1
	if (TCFT2.PloziNav.Editor.Data[TCFT2.PloziNav.Editor.CurrentSystem] ~= nil) then
		for _,sysdata in ipairs(TCFT2.PloziNav.Editor.Data[TCFT2.PloziNav.Editor.CurrentSystem]) do
			TCFT2.PloziNav.UI.RouteList[num] = sysdata.desc
			num = num + 1
		end
	else
		TCFT2.PloziNav.UI.RouteList[num] = "No routes"
	end
end

-- Click on a route - set in on the navmap
TCFT2.PloziNav.Editor.RouteClick = function(list, text, index, selection)
	local record
	if (TCFT2.PloziNav.Editor.Data[TCFT2.PloziNav.Editor.CurrentSystem] ~= nil) then
		if (TCFT2.PloziNav.Editor.Data[TCFT2.PloziNav.Editor.CurrentSystem][index] ~= nil) then
			record = TCFT2.PloziNav.Editor.Data[TCFT2.PloziNav.Editor.CurrentSystem][index]
			TCFT2.PloziNav.UI.navmap:setpath(GetFullPath(record.from, record.data))
		end
	end
end

local function print_r(tbl)
	for key,val in pairs(tbl) do
		if (type(val)=='table') then
			PloziTools.msg("*** " .. key)
			print_r(val)
		else
			if (type(val)~='function') then
				PloziTools.msg(key .. " = " .. val)
			end
		end
	end
end

-- Refresh data from server
TCFT2.PloziNav.Editor.RefreshNavData2 = function()
	TCFT2.PloziNav.LoadServerNavData()
end

TCFT2.PloziNav.Editor.RefreshNavData = function()
	TCFT2.ConfirmDialog("Confirm Refresh", "This will replace all your local changes\nwith data from the server.\nContinue refreshing from server?", "YES", TCFT2.PloziNav.Editor.RefreshNavData2, "NO", nil)
end

TCFT2.PloziNav.Editor.SendDataToServer2 = function()
	TCFT2.SendData({ cmd="1210", data=spickle(TCFT2.PloziNav.Routes) })
end

TCFT2.PloziNav.Editor.SendDataToServer = function(self)
	TCFT2.ConfirmDialog("Send Data To Server", 
		"This will replace the navigation data on the server\nwith your local data.\nContinue?",
		"Yes", TCFT2.PloziNav.Editor.SendDataToServer2,
		"No", nil		
	)
end

-- Delete selected Route
TCFT2.PloziNav.Editor.DeleteRoute2 = function()
	local tmpRoutes = {}
	for sys,data in pairs(TCFT2.PloziNav.Routes) do
		if (sys ~= TCFT2.PloziNav.Editor.SystemToDelete) then
			tmpRoutes[sys] = data
		end
	end
	TCFT2.PloziNav.Routes = tmpRoutes
	TCFT2.PloziNav.Editor.LoadData()
	TCFT2.PloziNav.Editor.LoadSystemRoutes()
end

TCFT2.PloziNav.Editor.DeleteRoute = function(self)
	local idx = tonumber(TCFT2.PloziNav.UI.RouteList.value)
	if (idx == 0) then
		TCFT2.MessageDialog("Delete route", "You have to select a route to delete.")
	else
		if (TCFT2.PloziNav.Editor.Data[TCFT2.PloziNav.Editor.CurrentSystem] ~= nil) then
			if (TCFT2.PloziNav.Editor.Data[TCFT2.PloziNav.Editor.CurrentSystem][idx] ~= nil) then
				local delrec = TCFT2.PloziNav.Editor.Data[TCFT2.PloziNav.Editor.CurrentSystem][idx]
				if (delrec) then
					local pos = delrec.from .. '-' .. delrec.to
					local delSystems = ShortLocationStr(delrec.from) .. " -> " .. ShortLocationStr(delrec.to)
					TCFT2.PloziNav.Editor.SystemToDelete = pos
					TCFT2.ConfirmDialog("Confirm Delete", "Delete route:\n" .. delSystems, "YES", TCFT2.PloziNav.Editor.DeleteRoute2, "NO", nil)
				end
			end
		end

	end
end

TCFT2.PloziNav.Editor.Initialize = function()
	TCFT2.PloziNav.Editor.CurrentSystem = GetCurrentSystemid() or 1
	TCFT2.PloziNav.Editor.CurrentSector = GetCurrentSectorid() or 1
end


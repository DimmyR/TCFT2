--[[
Guild functions
]]

local function createGuildActivityTab()
	local container

	TCFT2.GuildBank.Activity = TCFT2.MatrixList:new()
	TCFT2.GuildBank.Activity:addcolumn(TCFT2.MatrixList.column.new("date", "Date", "ARIGHT", { "S:D:date" }, 10))
	TCFT2.GuildBank.Activity:addcolumn(TCFT2.MatrixList.column.new("activity", "Activity", "ALEFT", { "S:A:activity" }, 90))
	TCFT2.GuildBank.Activity:init()
	
	TCFT2.GuildBank.ActivityPage = iup.label { title="Guild Activity Page 1", expand="HORIZONTAL", font=Font.H3*HUD_SCALE }
	
	TCFT2.GuildBank.ActivityFirstBtn = iup.stationbutton { 
		title="<< First",
		action = function(self)
			TCFT2.GuildActivity.CurrentPage = 1
			TCFT2.GuildBank.UpdateActivity()
		end,
		hotkey=iup.K_f,
	}

	TCFT2.GuildBank.ActivityPrevBtn = iup.stationbutton { 
		title="< Previous",
		action = function(self)
			TCFT2.GuildActivity.CurrentPage = TCFT2.GuildActivity.CurrentPage - 1
			if (TCFT2.GuildActivity.CurrentPage<1) then TCFT2.GuildActivity.CurrentPage = 1 end
			TCFT2.GuildBank.UpdateActivity()
		end,
		hotkey=iup.K_p,
	}

	TCFT2.GuildBank.ActivityNextBtn = iup.stationbutton { 
		title="Next >",
		action = function(self)
			TCFT2.GuildActivity.CurrentPage = TCFT2.GuildActivity.CurrentPage + 1
			TCFT2.GuildBank.UpdateActivity()
		end,
		hotkey=iup.K_n,
	}
	
	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			TCFT2.GuildBank.ActivityFirstBtn,
			TCFT2.GuildBank.ActivityPrevBtn,
			TCFT2.GuildBank.ActivityNextBtn,
			iup.fill { size=10 },
			TCFT2.GuildBank.ActivityPage,
		},
		iup.fill { size=10 },
		iup.hbox {
			expand="YES",
			TCFT2.GuildBank.Activity,
		},
	}
	
	container.tabtitle = "Guild Activity"
	container.hotkey = iup.K_l
	function container:OnHelp()
	   TCFT2.ShowHelp("GUILD_ACTIVITY")
	end

	function container:OnActivate(newtab, oldtab)
		if (TCFT2.GuildActivity.FirstUpdate==false) then
			TCFT2.GuildBank.UpdateActivity()
			TCFT2.GuildActivity.FirstUpdate = true
		end
	end

	return container
end

local function createGuildBankLogTab()
	local container

	TCFT2.GuildBank.Log = TCFT2.MatrixList:new()
	TCFT2.GuildBank.Log:addcolumn(TCFT2.MatrixList.column.new("date", "Date", "ARIGHT", { "S:D:date" }, 10))
	TCFT2.GuildBank.Log:addcolumn(TCFT2.MatrixList.column.new("name", "Name", "ALEFT", { "S:A:name" }, 14))
	TCFT2.GuildBank.Log:addcolumn(TCFT2.MatrixList.column.new("withdraw", "Withdraw", "ARIGHT", { "N:D:withdraw" }, 8, { commavalue=true, replace={ ["0"]="" } }))
	TCFT2.GuildBank.Log:addcolumn(TCFT2.MatrixList.column.new("deposit", "Deposit", "ARIGHT", { "N:D:deposit" }, 8, { commavalue=true, replace={ ["0"]="" } }))
	TCFT2.GuildBank.Log:addcolumn(TCFT2.MatrixList.column.new("balance", "Balance", "ARIGHT", { "N:D:balance" }, 9, { commavalue=true, replace={ ["0"]="" } }))
	TCFT2.GuildBank.Log:addcolumn(TCFT2.MatrixList.column.new("desc", "Description", "ALEFT", { "S:A:desc" }, 51))
	TCFT2.GuildBank.Log:init()
	
	TCFT2.GuildBank.LogPage = iup.label { title="Guild Bank Log Page 1", expand="HORIZONTAL", font=Font.H3*HUD_SCALE }

	TCFT2.GuildBank.LogDumpLinenumbers = false
	
	TCFT2.GuildBank.LogFirstBtn = iup.stationbutton { 
		title="<< First",
		action = function(self)
			TCFT2.GuildBankLog.CurrentPage = 1
			TCFT2.GuildBank.UpdateBankLog()
		end,
		hotkey=iup.K_f,
	}

	TCFT2.GuildBank.LogPrevBtn = iup.stationbutton { 
		title="< Previous",
		action = function(self)
			TCFT2.GuildBankLog.CurrentPage = TCFT2.GuildBankLog.CurrentPage - 1
			if (TCFT2.GuildBankLog.CurrentPage<1) then TCFT2.GuildBankLog.CurrentPage = 1 end
			TCFT2.GuildBank.UpdateBankLog()
		end,
		hotkey=iup.K_p,
	}

	TCFT2.GuildBank.LogNextBtn = iup.stationbutton { 
		title="Next >",
		action = function(self)
			TCFT2.GuildBankLog.CurrentPage = TCFT2.GuildBankLog.CurrentPage + 1
			TCFT2.GuildBank.UpdateBankLog()
		end,
		hotkey=iup.K_n,
	}
	
	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			TCFT2.GuildBank.LogFirstBtn,
			TCFT2.GuildBank.LogPrevBtn,
			TCFT2.GuildBank.LogNextBtn,
			iup.fill { size=10 },
			TCFT2.GuildBank.LogPage,
		},
		iup.fill { size=10 },
		iup.hbox {
			expand="YES",
			TCFT2.GuildBank.Log,
		},
		iup.hbox {
			margin="2x15",
			iup.fill {},
			iup.stationtoggle { title="Include line-numbers in log", value="NO", action=function(self, state) if (state==1) then TCFT2.GuildBank.LogDumpLinenumbers=true else TCFT2.GuildBank.LogDumpLinenumbers=false end end },
			iup.fill { size=10 },
			iup.stationbutton { title="Dump to log", action=function() TCFT2.GuildBank.DumpToLog() end }
		}
	}
	
	container.tabtitle = "Guild Bank Log"
	container.hotkey = iup.K_b
	function container:OnHelp()
	   TCFT2.ShowHelp("GUILD_BANKLOG")
	end
	function container:OnActivate(newtab, oldtab)
		if (TCFT2.GuildBankLog.FirstUpdate==false) then
			TCFT2.GuildBank.UpdateBankLog()
			TCFT2.GuildBankLog.FirstUpdate = true
		end
	end

	return container
end


local function createGuildMemberTab()
	local container
	
	TCFT2.Guild.MemberList = TCFT2.MatrixList:new()
	TCFT2.Guild.MemberList:addcolumn(TCFT2.MatrixList.column.new("rank", "Rank", "ALEFT", { "S:A:rank", "S:A:name" }, 15))
	TCFT2.Guild.MemberList:addcolumn(TCFT2.MatrixList.column.new("name", "Name", "ALEFT", { "S:A:name" }, 70))
	TCFT2.Guild.MemberList:addcolumn(TCFT2.MatrixList.column.new("joindate", "Joined", "ARIGHT", { "N:D:joindate" }, 15))
	TCFT2.Guild.MemberList:init()

	TCFT2.Guild.MemberEditButtons = iup.hbox {
		iup.fill {},
		iup.stationbutton {
			title="Create New Member",
			action=TCFT2.Guild.CreateMember,
		},
		iup.fill { size=5 },
		iup.stationbutton {
			title="Edit Member",
			action=TCFT2.Guild.EditMember,
		},
		iup.fill { size=5 },
		iup.stationbutton {
			title="Delete Member",
			action=TCFT2.Guild.DeleteMember,
		},
		visible="NO",
		alignment="ARIGHT",
		margin="0x5",
	}
	
	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			iup.label { title="Guild Members" },
		},
		iup.hbox {
			TCFT2.Guild.MemberList,
		},
		TCFT2.Guild.MemberEditButtons,
		expand="YES",
	}
	
	container.tabtitle = "Guild Members"
	container.hotkey = iup.K_r
	function container:OnHelp()
	   TCFT2.ShowHelp("GUILD_MEMBERS")
	end
	function container:OnActivate(newtab, oldtab)
		if (TCFT2.Guild.MembersFirstUpdate==false) then
			TCFT2.Guild.UpdateGuildMembers()
			TCFT2.Guild.MembersFirstUpdate = true
		end
	end

	return container
end


TCFT2.CreateGuildTab = function()
	local GuildActivityTab = createGuildActivityTab()
	local GuildBankLogTab = createGuildBankLogTab()
	local GuildMemberTab = createGuildMemberTab()
	return TCFT2.subtabs { GuildMemberTab, GuildActivityTab, GuildBankLogTab, 
		OnActivate=function(self, newtab, oldtab)
			if (TCFT2.Guild.MembersFirstUpdate==false) then
					TCFT2.Guild.UpdateGuildMembers()
					TCFT2.Guild.MembersFirstUpdate = true
			end
		end
	}
end


local function createEditDialog()
	local container
	
	TCFT2.EditMemberDialogTitle = iup.label { title="Member", expand="HORIZONTAL", alignment="ACENTER" }
	TCFT2.EditMemberRank = iup.stationsubsublist { dropdown="YES" }
	TCFT2.EditMemberName = iup.text { size="250x" }
	TCFT2.EditMemberJoin = iup.text { size="250x" }
	
	TCFT2.EditMemberRank[1] = nil
	for idx=1, #TCFT2.Guild.MemberRanks+1 do
		TCFT2.EditMemberRank[idx] = TCFT2.Guild.MemberRanks[idx-1]
	end
	
	
	local savebtn = iup.stationbutton {
		title ="Save",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			local member = {}
			member.name = TCFT2.EditMemberName.value
			member.rank = TCFT2.EditMemberRank.value - 1
			member.joindate = TCFT2.EditMemberJoin.value
			member.id = tonumber(TCFT2.EditMemberDialog.saveid) or 0
			if (member.name=="") then
				TCFT2.MessageDialog("Edit Member", "Name missing.")
			else
				TCFT2.Guild.SaveMember(member)
				HideDialog(TCFT2.EditMemberDialog)
			end
		end
	}

	local cancelbtn = iup.stationbutton {
		title ="Cancel",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			HideDialog(TCFT2.EditMemberDialog)
		end
	}

	container = iup.dialog {
		iup.hbox {
			iup.fill {},
			iup.vbox {
				iup.fill {},
				iup.stationhighopacityframe {
					expand="NO",
					-- size="%60x",
					iup.stationhighopacityframebg {
						iup.vbox {
							margin="15x15",
							iup.hbox {
								TCFT2.EditMemberDialogTitle,
							},
							iup.fill { size="15" },
							iup.vbox {
								iup.hbox {
									iup.label { size="100x", title="Rank" },
									TCFT2.EditMemberRank,
								},
								iup.hbox {
									iup.label { size="100x", title="Name" },
									TCFT2.EditMemberName,
								},
								iup.hbox {
									iup.label { size="100x", title="Joined" },
									TCFT2.EditMemberJoin,
								},
							},
							iup.fill { size="15" },
							iup.hbox {
								iup.fill {}, 
								savebtn,
								iup.fill { size=5 },
								cancelbtn,
								iup.fill {}, 
							},
						},
					},
				},
				iup.fill {},
			},
			iup.fill {},
		},
		border="NO",menubox="NO",resize="NO",
		defaultesc=cancelbtn,
		defaultenter=savebtn,
		topmost="YES",
		modal="NO",
		alignment="ACENTER",
		bgcolor="0 0 0 92 *",
		fullscreen="YES",
		show_cb = function(self)
		end
	}
	return container
end

TCFT2.EditMemberDialog = TCFT2.EditMemberDialog or createEditDialog()


















--[[
TCFT2 - UI stuff
]]


TCFT2.HelpTexts = { }

TCFT2.HelpTexts["MAIN_COMM"] = TCFT2.colors.YELLOW .. "TCFT - Comm" .. TCFT2.colors.NORMAL .. [[


This window contain the main communication interface in TCFT2. Private messages, TCFT2 and Guild chat are displayed here.

Commands available in main TCFT2 chat:
/who
	Displays users logged in to TCFT2 server

/whoguild
	Displays guild members currently logged in to VO

/version
	Displays current TCFT2 version

/clear
	Clears the message window

/msg
	Sends a private message to another user. Syntax is /msg <user> <message>

You can switch between TCFT2 and Guild chat with CRTL-T and CTRL-G. To answer last received private message use CTRL-P.

You can also see the current connection state to the server at the bottom of the TCFT2 screen in all tabs.
Same with Load Sector, Connect/Disconnect and Close buttons. Load Sector loads the ores for current sector. You can also set this to be done automatically in Settings.

Commands available in the main VO chat area:
/tc2
	Loads the main TCFT2 window

/say_tc2 <message>
	Can be used to chat to TCFT2 while in main chat.

/tc2 navreload
	Reload the navigation data from the server.

/tc2 navedit
	Open up the editor window for navigation routes. You need special access to be able to send edited data to the server.

/tc2 lastseen <playername>
	If no playername are given then all players seen in current sector last 48 hours are displayed. 
	If playername are given then the list will display all locations last seen for player last 48 hours.

/tc2 scanner on|off|toggle
	Turns scanner on/off or toggle the state.

/tc2 cargolist
	Display a list with cargo found in range. Then you can target the cargo by selecting the list.

/tc2 autoeject on
	Turn on AutoEject Mining Tool

/tc2 autoeject off
	Turn off AutoEject Mining Tool

/tc2 autoeject toggle
	Toggle the state of AutoEject Mining Tool

]]


TCFT2.HelpTexts["TRADING_STATIONCOMPARE"] = TCFT2.colors.YELLOW .. "Trading - Station Compare" .. TCFT2.colors.NORMAL .. [[


Here you can compare tradeable items between stations and systems. You get an overview of items with sell/buy price, mass, volume, xp and profit pr cu.
The list also displays wich stations that buy and sell the listed items.

When you have compared two stations it is easy to also compare the return route to check if there are profitable items to sell when returning. Just click "Swap And Compare" button and you get the compare the other way.
]]

TCFT2.HelpTexts["TRADING_ITEMLOOKUP"] = TCFT2.colors.YELLOW .. "Trading - Item Lookup" .. TCFT2.colors.NORMAL .. [[


Here you can search for trade items and where they are bought, sold or wanted. Then you select the itme you want to look up.

You can filter on item type like weapon, battery or commodities. You can also filter on bought, sold and wanted in addition to wich system you want to search in.

The result gives you an overview of station who sell, prices, and when the data was last updated.
]]

TCFT2.HelpTexts["TRADING_TRADEROUTES"] = TCFT2.colors.YELLOW .. "Trading - Trade Routes" .. TCFT2.colors.NORMAL .. [[


Here you can search for good trade-routes between stations and/or systems.
If you dont want to include weapon, ships or batteries just check the boxes with "No xxx" where xxx are weapon, ships and batteries.
You can also select minimum profit pr cu for the route - from 100 to 500c pr cu.

Select from, to and eventually filters - then click "Searh For Routes" to get the result. If you click "Swap and Search" you will get trade-routes the other way of wich you first selected.
]]

TCFT2.HelpTexts["TRADING_STATIONS"] = TCFT2.colors.YELLOW .. "Trading - Stations" .. TCFT2.colors.NORMAL .. [[


This gives you a list of stations that have trade-data that are out of date and need updates.
Click "Refresh List" to update the table.
]]

TCFT2.HelpTexts["TRADING_TOOLS"] = TCFT2.colors.YELLOW .. "Trading - Tools" .. TCFT2.colors.NORMAL .. [[


Different Trading Tools.

Price calculator let you test what quantity you should sell to get a given profit pr cu. You can give the profit in credits or perecent of max-price (that is the price for one item).
]]


TCFT2.HelpTexts["MINING_ORESEARCH"] = TCFT2.colors.YELLOW .. "Mining - Ore Search" .. TCFT2.colors.NORMAL .. [[


Here you can search for the location of known ores. Select System and Ore-name, eventually select density filter and click "Find Ores".
The list gives you an overview of sector, ateroid/Ice Crystal number, mineral name and density. If you click on a line in the list also the range will be updated if the asteroid/Ice Crystal are in range.
]]

TCFT2.HelpTexts["MINING_MININGMAP"] = TCFT2.colors.YELLOW .. "Mining - Mining Map" .. TCFT2.colors.NORMAL .. [[


This is a map with information of known ores in current system. If the system-data are not already loaded you will get the opportunity to load it when you enter the mining map. You can also load the system later by clicking the "Load System" button.

When system-data are loaded you get an overview of known ores in the system. The density are displayed on the map as colors - described at the bottom of the page.
You can get the average density for all ores or you can select individual ores to get density overview for that particular ore.

Known bots sighted in the sectors are also displayed in the info at right together with info about ore density.
]]

TCFT2.HelpTexts["MINING_ORELIST"] = TCFT2.colors.YELLOW .. "Mining - Mining Map" .. TCFT2.colors.NORMAL .. [[


This window gives you an overview of ores pr system. It displays a list with ore-name, number of asteriods containing that ore and what system it is in.
Useful for finding what system to visit for mining a specific ore.
]]

TCFT2.HelpTexts["SETTINGS_GENERAL"] = TCFT2.colors.YELLOW .. "Settings - General" .. TCFT2.colors.NORMAL .. [[


Username and password
	Username and password required to access the server. PM a Council member in forum or in game to get a username.

Automatically connect on VO startup
	This tells TCFT2 if it should try to automatically connect when you log on to VO.

Automatically load data for current sector
	If this is checked TCFT2 will automatically load all avialable data for the sector when you are entering.
	If this is unchecked you have to load it manually using ]] .. TCFT2.colors.YELLOW .. "Load Sector" .. TCFT2.colors.NORMAL .. [[ button to be able to access the data.

Display TCFT2 Buttons
	If you want TCFT2 buttons in PDA, Station and Capship

Echo important messages to main chat
	If this is checked informative messages regarding TCFT2 are also sent to main chat in addition to  the TCTF-tab display.

Echo TCFT2 chat to main chat
	If this is checked TCFT2 messages are output to main chat in addition to  the TCTF-tab display. To chat from main chat use /say_tc2 <message>

Notify me when guild members log on and off
	If this is checked you will get a message when guild member log on or off.

Display location in chat messages
	If this is checked then your location will be added to TCFT2 messages displaying for other users where you are located.


Automatically repair ship when docking
	When this is checked then your ship will be automatically repaired when you dock to a station.

Automativally reload ammo when docking
	When this is checked all your weapons that requre ammo will automatically be reloaded when you dock to a station.

]]

TCFT2.HelpTexts["SETTINGS_ASTEROIDSCANNER"] = TCFT2.colors.YELLOW .. "Settings - Asteroid Scanner" .. TCFT2.colors.NORMAL .. [[


The asteroid-scanner when turned on will automatically scan the university for minerals and save the data. 
When you leave the sector or dock to a station then the data will be submitted to the server for other to use it too.

Search Timeout
	This tells the scanner how long to wait until move to next object after a failed scan (because of out of range etc.)

Scan Timeout
	This value tells the scanner how long to keep object targeted before moving on.
	This value have to be long enough for the scanner to be able to scan the object.

The values are set in milliseconds - 1000ms = 1 second.
The lower the values can be the faster you can scan the universe, but too small values might hang up your game or miss data from objects.
Play with the values until you find the ones that fit you.

You can turn the scanner on and off with the command /tc2 scanner on|off or toggle it with the command /tc2 scanner toggle.

If you have the TCFT2 buttons active you will have a scanbner toggle button in your PDA when not docked. Another alternative is to define a keyboard bind for the scanner in settings.
]]

TCFT2.HelpTexts["SETTINGS_MININGTOOLS"] = TCFT2.colors.YELLOW .. "Settings - Mining Tools" .. TCFT2.colors.NORMAL .. [[


AutoEject let you automatically eject specific ore while mining. You can select wich ore you want to autoeject and if you want autoeject to be active at startup/login.
You can turn autoeject on and off with the command /tc2 autoeject on|off or toggle it with /tc2 autoeject toggle.

If you have the TCFT2 buttons active you will have a toggle button for autoeject in your PDA when not docked. Another alternative is to define a keyboard bind to the commands in settings.
]]

TCFT2.HelpTexts["SETTINGS_KEYBINDINGS"] = TCFT2.colors.YELLOW .. "Settings - Key bindings" .. TCFT2.colors.NORMAL .. [[


Here you can define key bindings to common commands in TCFT2:
Show TCFT2, TCFT2 chat, show cargolist, toggle scanner on/off and toggle autoeject on/off.

After you have put your keys in the fields you should do "Test Binds" to check that you dont have any conflicts that will overwrite other bindings that you have already defined.
]]

TCFT2.HelpTexts["SETTINGS_RELAY"] = TCFT2.colors.YELLOW .. "Settings - IRC Relayer" .. TCFT2.colors.NORMAL .. [[


These settings allow you to contribute to the IRC relay for the guild chat.  The Relay Server picks a random user who has the plugin turned on for relay, and when he logs off, it picks another.

Server:
	Server name or IP for the IRC Relay Server that will relay guild chat from your client to the #tgft channel on IRC.

Server Port:
	Numeric port for the IRC Relay Server. Default: 11000

Server Password:
	Password that will authenticate you as a valid relayer to the relay server. Don't change the default one unless told to do so in the forums. That password will change occasionally for security.


* In order to be able to view guild messages outside the game, you need to register an IRC account and join channel #tgft on server IRC.SLASHNET.ORG

Brief instructions on setting up and registering your IRC account (these can also be found on the forums):
	1. Go to http://www.slashnet.org/webclient
	2. Use your in-game name to get in (use underscores for spaces).
	3. Type the following:   /msg NickServ REGISTER password email
	4. You should get an email with the AUTH code within a minute
	5. Type the following:   /msg NickServ AUTH authcode
	6. Type the following:   /msg NickServ IDENTIFY password
	7. Type the following:   /msg NickServ SET HIDE EMAIL ON
	8. Type the following:   /JOIN #tgft
	9. Type the following:   /msg NickServ AJOIN ADD #tgft (this will auto-join you to the channel every time you login)

Next time you log into IRC:
	1. Go to http://www.slashnet.org/webclient
	2. Login with your nick.
	3. /msg NickServ IDENTIFY password
	4. You will be automatically added to the guild channel.

You can also use an IRC client:
	Android: Atomic
	Windows: HexChat  (http://hexchat.github.io)
	Mac: No idea, I don't use Mac. (DimmyR)

]]

TCFT2.HelpTexts["GUILD_ACTIVITY"] = TCFT2.colors.YELLOW .. "Guild - Activity" .. TCFT2.colors.NORMAL .. [[

Here you find the guild activity log.
]]

TCFT2.HelpTexts["GUILD_BANKLOG"] = TCFT2.colors.YELLOW .. "Guild - Bank log" .. TCFT2.colors.NORMAL .. [[

Here you find the guild bank log. I contains a list of withdrawals and deposits to the guild bank 
]]

TCFT2.HelpTexts["INVENTORY_ALL"] = TCFT2.colors.YELLOW .. "Inventory - Inventory list" .. TCFT2.colors.NORMAL .. [[

Here you get an overview of your inventory all over the universe. You can select a specific item to display or you can list all items.
You get information on item location and number of items at that location.
]]

TCFT2.ShowHelp = function(key)

	TCFT2.HelpDlgMsg = TCFT2.HelpDlgMsg or iup.stationsubmultiline {
		readonly="YES",
		expand="YES",
		border="NO",
		bgcolor="40 40 40 192 *",
	}
	
	TCFT2.HelpDlgOkBtn = TCFT2.HelpDlgOkBtn or iup.stationbutton {
		title = "Close Help",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			HideDialog(TCFT2.HelpDlg)
		end
	}

	TCFT2.HelpDlg = TCFT2.HelpDlg or iup.dialog {
		iup.stationhighopacityframe {
			iup.stationhighopacityframebg {
				iup.vbox { margin="5x5",
					iup.hbox {
						iup.label { title="TCFT2 Help", alignment="ACENTER", expand="HORIZONTAL" },
					},
					iup.fill { size="10" },
					iup.hbox {
						TCFT2.HelpDlgMsg,
					},
					iup.fill { size="10" },
					iup.hbox {
						iup.fill {}, 
						TCFT2.HelpDlgOkBtn,
						iup.fill {},
					},
				},
			},
		},
		bgcolor="40 40 40 192 *",
		border="NO",menubox="NO",resize="NO",
		defaultesc=TCFT2.HelpDlgOkBtn,
		defaultenter=TCFT2.HelpDlgOkBtn,
		fullscreen="YES",
		topmost="YES",
		modal="YES",
	}
	local message = (TCFT2.HelpTexts[key] or "No help available for this content.")
	TCFT2.HelpDlgMsg.value = message
	PopupDialog(TCFT2.HelpDlg,iup.CENTER, iup.CENTER)
end

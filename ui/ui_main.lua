--[[
TCFT2
(c) By Plozi
Version 1.0.0

** Changelog

]]


-- TCFT Main tab - communication etc
local function createTcftMainTab()
	local container
	TCFT2.txt_status = TCFT2.txt_status or iup.multiline { value = "", expand = 'YES', readonly = 'YES', bgcolor="0 0 0 *", border="NO" }
	TCFT2.txt_tcftchatline = iup.multiline { value="", expand="HORIZONTAL", size="x"..Font.Default, SCROLLBAR="NO" }
	TCFT2.chatlabel = iup.label { title = TCFT2.colors.GREEN2 .. "TCFT2: " }
	TCFT2.rdo_chattype = {}
	TCFT2.rdo_chattype[1] = iup.stationradio { title="TCFT2", expand="NO", action=function(self,state) if (state==1) then TCFT2.SetChatType(1) end end, gap="5" }
	TCFT2.rdo_chattype[2] = iup.stationradio { title="Guild", expand="NO", action=function(self,state) if (state==1) then TCFT2.SetChatType(2) end end, gap="5" }
	TCFT2.rdo_chattype.gap = 5
	TCFT2.ChatTypeRadio = iup.radio { iup.hbox(TCFT2.rdo_chattype), value=TCFT2.rdo_chattype[1] }
	
	function TCFT2.txt_tcftchatline:action(key, str)
		if (key==9) then
			self.value = tabcomplete(TCFT2.txt_tcftchatline.value, TCFT2.txt_tcftchatline.caret)
			return iup.IGNORE
		end
		-- TCFT2.AddStatus(key)
		if (key==7) then -- Guild chat
			TCFT2.SetChatType(2)
			return iup.IGNORE
		elseif (key==20) then -- tcft chat
			TCFT2.SetChatType(1)
			return iup.IGNORE
		end
		if (key==16) then -- CTRL-P reply to last private msg
			if (TCFT2.txt_tcftchatline.value == '?' and TCFT2.LastPrivMsgUser ~= "") then
				TCFT2.txt_tcftchatline.value = "/msg \"" .. TCFT2.LastPrivMsgUser .. "\" "
			else
				return iup.IGNORE
			end
		end
		if (key==13) then
			TCFT2.TCFTChat()
		end
	end

	container = iup.vbox {
		bgcolor="200 200 200 *",
		iup.hbox {
			margin="5x5",
			TCFT2.txt_status,
		},
		iup.hbox {
			margin="3x2",
			iup.label { title="Chat to " }, TCFT2.ChatTypeRadio,
		},
		iup.hbox {
			margin = "2x2",
			TCFT2.chatlabel,
			TCFT2.txt_tcftchatline,
		},
		OnActivate = function()
			-- iup.SetFocus(TCFT2.txt_tcftchatline)
		end,
	}
	container.tabtitle = "Comm"
	container.hotkey = iup.K_c
	function container:OnHelp()
	   TCFT2.ShowHelp("MAIN_COMM")
	end
	return container
end


-- Create the main TCFT tab
local function createTCFTTab()
	local tcftMainTab = createTcftMainTab()
    local curtab = tcftMainTab
	return TCFT2.subtabs { tcftMainTab,
		OnActivate = function(self, newtab, oldtab)
			TCFT2.MainTabs:SetTabTextColor(2, "255 255 255")
		end
	}
end


TCFT2.TCFTTab = createTCFTTab()
TCFT2.TCFTTab.tabtitle = "TCFT"
TCFT2.TCFTTab.hotkey = iup.K_c

-- TCFT2.TCFTTab:GetTabButton(1).size= "%10x"


TCFT2.TradeTab = TCFT2.CreateTradeTab()
TCFT2.TradeTab.tabtitle = "Trading"
TCFT2.TradeTab.hotkey=iup.K_t
-- TCFT2.TradeTab:GetTabButton(1).size= "%10x"

TCFT2.MiningTab = TCFT2.CreateMiningTab()
TCFT2.MiningTab.tabtitle = "Mining"
TCFT2.MiningTab.hotkey=iup.K_m
-- TCFT2.MiningTab:GetTabButton(1).size= "%10x"

TCFT2.SettingsTab = TCFT2.CreateSettingsTab()
TCFT2.SettingsTab.tabtitle = "Settings"
TCFT2.SettingsTab.hotkey = iup.K_e
-- TCFT2.SettingsTab:GetTabButton(1).size= "%10x"

TCFT2.InventoryTab = TCFT2.CreateInventoryTab()
TCFT2.InventoryTab.tabtitle = "Inventory"
TCFT2.InventoryTab.hotkey = iup.K_i

TCFT2.GuildTab = TCFT2.CreateGuildTab()
TCFT2.GuildTab.tabtitle = "Guild"
TCFT2.GuildTab.hotkey = iup.K_g

TCFT2.PlayersTab = TCFT2.CreatePlayersTab()
TCFT2.PlayersTab.tabtitle = "Last Seen"
TCFT2.PlayersTab.hotkey = iup.K_a

TCFT2.MainTabs = iup.pda_root_tabs {
	TCFT2.TCFTTab,
	TCFT2.TradeTab,
	TCFT2.MiningTab,
	TCFT2.InventoryTab,
	TCFT2.GuildTab,
	TCFT2.PlayersTab,
	TCFT2.SettingsTab,
	tabchange_cb = function(self, newtab, oldtab)
		if (oldtab.OnDeactivate) then
			oldtab:OnDeactivate(newtab, oldtab)
		end
		if (newtab.OnActivate) then
			newtab:OnActivate(newtab, oldtab)
		end
		TCFT2.CurMainTab = newtab
	end,
}


TCFT2.statusline = TCFT2.statusline or iup.label { title = TCFT2.colors.RED .. "Not connnected.", expand="HORIZONTAL" }

TCFT2.connectButton = TCFT2.connectButton or iup.stationbutton {
	title=" Connect ",
	action=function()
		TCFT2.Connection.CheckConnection()
	end,
	size = tostring(Font.Default*8).."x",
}

TCFT2.loadSectorButton = TCFT2.loadSectorButton or iup.stationbutton {
	title=" Load Sector ",
	action=function()
		TCFT2.Mining.LoadSector()
	end,
	size = tostring(Font.Default*8).."x",
}

TCFT2.reloadinterfacebtn = TCFT2.reloadinterfacebtn or iup.stationbutton {
	title=" Reload Interface ",
	action=function()
		ReloadInterface()
	end,
	size = tostring(Font.Default*8).."x",
}


-- Create interface and display main page
TCFT2.DisplayTCFT2 = function()

	local tcCancelBtn = iup.stationbutton {
		title="Close TCFT2",
		action=function()
			HideDialog(TCFT2.MainWindow)
		end,
		size = tostring(Font.Default*8).."x",
	}
	
	TCFT2.bottom = TCFT2.bottom or iup.vbox {
		iup.hbox {
			TCFT2.statusline,
			iup.fill {},
			TCFT2.reloadinterfacebtn,
			iup.fill { size="%2" },
			TCFT2.loadSectorButton,
			iup.fill { size="%2" },
			TCFT2.connectButton,
			tcCancelBtn,
		},
	}

	TCFT2.MainWindow = TCFT2.MainWindow or iup.dialog {
		iup.stationhighopacityframe {
			--iup.stationhighopacityframebg {
				iup.vbox {
                    -- iup.label { title="T C F T  2", expand="HORIZONTAL", alignment="ACENTER" },
                   iup.zbox {
                        expand="YES",
                        all="YES",
                        iup.hbox {
                            alignment="NE",
                            iup.fill{}, iup.label { title = "", image="plugins/TCFT2/images/tcft2_logo.png" },
                        },
                        iup.vbox {
                            alignment="ACENTER",
                            iup.fill { size=3 }, -- 15 to 3 -- tokkano
                            	iup.label { title="T C F T  2 ", expand="HORIZONTAL", alignment="ARIGHT" },
                        },
                        iup.vbox {
                            iup.fill { size="1" }, -- 35 to 1 -- tokkana
        				    TCFT2.MainTabs,
        				},
                    },
					iup.hbox {
						margin="3x1",
						expand="YES",
						TCFT2.bottom,
					},
				},
			--},
		},
		topmost="YES",
		resize="NO",
		fullscreen="YES",
		expand="NO",
		modal="YES",
		border="NO",
		alignment="ATOP",
		defaultesc=tcCancelBtn,
		show_cb = function(self)
			if (GetCurrentStationType()~=1 and PlayerInStation()) then
				HideDialog(StationDialog)
			end
			-- Update systems dropdown
			TCFT2.Mining.MineralSearch.SearchSystem[1] = "Current sector"
			for i=1,#TCFT2.SystemNames do
				TCFT2.Trading.LookupSystem[i+1] = TCFT2.SystemNames[i]
				TCFT2.Mining.MiningMap.LookupSystem[i] = TCFT2.SystemNames[i]
				TCFT2.Mining.MineralSearch.SearchSystem[i+1] = TCFT2.SystemNames[i]
			end
			ProcessEvent("TCFT2_SHOW")
		end,
		hide_cb = function(self)
			if (GetCurrentStationType()~=1 and PlayerInStation()) then
				ShowDialog(StationDialog)
			end
			ProcessEvent("TCFT2_HIDE")
		end,
		map_cb = function(self)
			ProcessEvent("TCFT2_MAIN_MAP")
		end,
		bgcolor="40 40 40 192 *"
	}
	PopupDialog(TCFT2.MainWindow, iup.CENTER, iup.CENTER)
end


-- Configmdialog
TCFT2.ConfirmDialog = function(title, message, okbtn, okbtnfunc, cancelbtn, cancelbtnfunc)
	TCFT2.ConfirmDlgCancelBtn = TCFT2.ConfirmDlgCancelBtn or iup.stationbutton {
		title = cancelbtn or "Cancel",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
	}
	function TCFT2.ConfirmDlgCancelBtn:action(self)
		HideDialog(TCFT2.ConfirmDlg)
		if (cancelbtnfunc ~= nil) then cancelbtnfunc(self) end
	end
	
	
	 TCFT2.ConfirmDlgOkBtn = TCFT2.ConfirmDlgOkBtn or iup.stationbutton {
		title = okbtn or "OK",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
	}
	function TCFT2.ConfirmDlgOkBtn:action(self)
		HideDialog(TCFT2.ConfirmDlg)
		if (okbtnfunc ~= nil) then okbtnfunc(self) end
	end
	

	TCFT2.ConfirmDlgTitle = TCFT2.ConfirmDlgTitle or iup.label { title="", alignment="ACENTER", expand="YES", font=Font.H2*HUD_SCALE }
	TCFT2.ConfirmDlgMsg = TCFT2.ConfirmDlgMsg or iup.label { title="", alignment="ALEFT", expand="YES", margin="5x15" }

	TCFT2.ConfirmDlg = TCFT2.ConfirmDlg or iup.dialog{
		iup.hbox {
			iup.fill {},
			iup.vbox {
				iup.fill {},
				iup.stationhighopacityframe{
					expand="NO",
					iup.stationhighopacityframebg{
						iup.vbox { margin="15x15",
							iup.hbox {
								TCFT2.ConfirmDlgTitle,
							},
							iup.fill { size="15" },
							iup.hbox {
								TCFT2.ConfirmDlgMsg,
							},
							iup.fill { size="15" },
							iup.hbox {
								iup.fill {}, 
								TCFT2.ConfirmDlgOkBtn,
								iup.fill { size="3" },
								TCFT2.ConfirmDlgCancelBtn,
								iup.fill {},
							},
						},
					},
				},
				iup.fill {},
			},
			iup.fill {},
		},
		border="NO",menubox="NO",resize="NO",
		defaultesc=TCFT2.ConfirmDlgCancelBtn,
		defaultenter=TCFT2.ConfirmDlgOkBtn,
		topmost="YES",
		modal="YES",
		bgcolor="0 0 0 92 *",
		fullscreen="YES",
	}
	TCFT2.ConfirmDlgCancelBtn.title = cancelbtn
	TCFT2.ConfirmDlgOkBtn.title = okbtn
	TCFT2.ConfirmDlgTitle.title = title
	TCFT2.ConfirmDlgMsg.title = message
	PopupDialog(TCFT2.ConfirmDlg,iup.CENTER, iup.CENTER)
end

-- Messagemdialog
TCFT2.MessageDialog = function(title, message)
	local okbtn = iup.stationbutton {
		title ="OK",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			HideDialog(TCFT2.MessageDlg)
		end
	}

	TCFT2.MessageDlgTitle = TCFT2.MessageDlgTitle or iup.label { title="", alignment="ACENTER", expand="YES", font=Font.H2*HUD_SCALE }
	TCFT2.MessageDlgMsg = TCFT2.MessageDlgMsg or iup.label { title="", alignment="ALEFT", expand="YES", margin="5x15" }
	
	TCFT2.MessageDlg = TCFT2.MessageDlg or iup.dialog{
		iup.hbox {
			iup.fill {},
			iup.vbox {
				iup.fill {},
				iup.stationhighopacityframe{
					expand="NO",
					iup.stationhighopacityframebg{
						iup.vbox { margin="15x15",
							iup.hbox {
								TCFT2.MessageDlgTitle,
							},
							iup.fill { size="15" },
							iup.hbox {
								TCFT2.MessageDlgMsg,
							},
							iup.fill { size="15" },
							iup.hbox {
								iup.fill {}, 
								okbtn,
								iup.fill {}, 
							},
						},
					},
				},
				iup.fill {},
			},
			iup.fill {},
		},
		border="NO",menubox="NO",resize="NO",
		defaultesc=okbtn,
		topmost="YES",
		modal="YES",
		alignment="ACENTER",
		bgcolor="0 0 0 92 *",
		fullscreen="YES",
	}
	TCFT2.MessageDlgTitle.title = title
	TCFT2.MessageDlgMsg.title = message
	PopupDialog(TCFT2.MessageDlg,iup.CENTER, iup.CENTER)
end

-- Errordialog
TCFT2.ErrorDialog = function(title, message)
	local okbtn = iup.stationbutton {
		title ="OK",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			HideDialog(TCFT2.ErrorDlg)
		end
	}

	TCFT2.ErrorDlgTitle = TCFT2.ErrorDlgTitle or iup.label { title="", alignment="ACENTER", expand="YES", font=Font.H2*HUD_SCALE }
	TCFT2.ErrorDlgMsg = TCFT2.ErrorDlgMsg or iup.label { title="", alignment="ALEFT", expand="YES", margin="5x15" }
	
	TCFT2.ErrorDlg = TCFT2.ErrorDlg or iup.dialog{
		iup.hbox {
			iup.fill {},
			iup.vbox {
				iup.fill {},
				iup.stationhighopacityframe{
					expand="NO",
					iup.stationhighopacityframebg{
						iup.vbox { margin="15x15",
							iup.hbox {
								TCFT2.ErrorDlgTitle,
							},
							iup.fill { size="15" },
							iup.hbox {
								TCFT2.ErrorDlgMsg,
							},
							iup.fill { size="15" },
							iup.hbox {
								iup.fill {}, 
								okbtn,
								iup.fill {}, 
							},
						},
					},
				},
				iup.fill {},
			},
			iup.fill {},
		},
		border="NO",menubox="NO",resize="NO",
		defaultesc=okbtn,
		topmost="YES",
		modal="YES",
		alignment="ACENTER",
		bgcolor="0 0 0 92 *",
		fullscreen="YES",
	}
	TCFT2.ErrorDlgTitle.title = TCFT2.colors.RED .. title
	TCFT2.ErrorDlgMsg.title = message
	PopupDialog(TCFT2.ErrorDlg, iup.CENTER, iup.CENTER)
end


TCFT2.ProgressDialog = function(header, message, cancelfunc, min, max)
	TCFT2.ProgressDialogCancel = TCFT2.ProgressDialogCancel or iup.stationbutton {
		title ="CANCEL",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			HideDialog(TCFT2.ProgressDlg)
			if (cancelfunc) then
				cancelfunc()
			end
		end
	}
	
	TCFT2.ProgressDialogPercent = TCFT2.ProgressDialogPercent or iup.label { title="0%", alignment="ACENTER", expand="NO", size=Font.Default * 4 }
	TCFT2.ProgressDialogHeader = TCFT2.ProgressDialogHeader or iup.label { title="", font=Font.H3*HUD_SCALE }
	TCFT2.ProgressDialogMsg = TCFT2.ProgressDialogMsg or iup.label { title="" }
	TCFT2.ProgressDialogProgressBar = TCFT2.ProgressDialogProgressBar or iup.stationprogressbar {
		LOWERCOLOR="64 255 64 128 *",
		--MIDDLEABOVECOLOR="0 255 0",
		--MIDDLEBELOWCOLOR="0 0 255",
		UPPERCOLOR="128 128 128 128 *",
		MINVALUE=(min or 0),
		MAXVALUE=(max or 100),
		type="HORIZONTAL",
		-- mode="TRINARY",
		expand="HORIZONTAL",
		--size="180x12",
		size="x12",
	}

	TCFT2.ProgressDlg = TCFT2.ProgressDlg or iup.dialog {
		iup.vbox {
			iup.fill {},
			iup.hbox {
				iup.fill {},
				iup.stationhighopacityframe {
					iup.stationhighopacityframebg {
						expand="NO",
						iup.vbox {
							margin="30x20",
							alignment="ACENTER",
							iup.hbox {
								TCFT2.ProgressDialogHeader,
							},
							iup.fill { size=20 },
							iup.hbox {
								iup.fill {},
								iup.zbox {
									alignment="ACENTER",
									TCFT2.ProgressDialogProgressBar,
									TCFT2.ProgressDialogPercent,
									all="YES",
								},
								iup.fill {},
							},
							iup.fill { size=15 },
							iup.hbox {
								TCFT2.ProgressDialogMsg,
							},
							iup.fill { size=15 },
							iup.hbox {
								TCFT2.ProgressDialogCancel,
							},
						},				
					},
				},
				iup.fill {},
			},
			iup.fill {},
		},
		bgcolor="0 0 0 128 *",
		border="NO",menubox="NO",resize="NO",
		defaultesc=TCFT2.ProgressDialogCancel,
		topmost="YES",
		modal="NO",
		alignment="ACENTER",
		fullscreen="YES",
	}
	TCFT2.ProgressDialogProgressBar.value=0
	TCFT2.ProgressDialogPercent.title = "0%"
	TCFT2.ProgressDialogHeader.title = header
	TCFT2.ProgressDialogMsg.title = message
	ShowDialog(TCFT2.ProgressDlg, iup.CENTER, iup.CENTER)
end

TCFT2.ProgressDialogUpdate = function(event, value)
	value = (tonumber(value) or 0)
	TCFT2.ProgressDialogProgressBar.value = value>100 and 100 or value
	TCFT2.ProgressDialogPercent.title=string.format("%d%%", (value>100 and "100" or value))
end

TCFT2.ProgressDialogHide = function()
	HideDialog(TCFT2.ProgressDlg)
	TCFT2.ProgressDialogProgressBar.value = "0%"
end


TCFT2.InitButtons = function()
	-- PDA	
--local parent = iup.GetParent(iup.GetParent(iup.GetParent(iup.GetParent(PDASecondaryInfo))))
local parent = iup.GetParent(iup.GetParent(PDASecondaryInfo))
PDASecondaryInfo.SIZE="208x100"
PDASecondaryInfo.ALIGNMENT="ACENTER"
if (parent~=nil) then
		local chlds = {}
		local chld = iup.GetNextChild(parent)
		repeat
			if (chld~=nil) then
				table.insert(chlds, chld)
				chld = iup.GetNextChild(parent, chld)
			end
		until chld==nil
		TCFT2.PDASecInfo = iup.pdasubframe_nomargin{
			iup.hbox{
				iup.vbox{
					iup.stationbutton {
						title = "TCFT2",
						EXPAND="HORIZONTAL",
						action = function(self)
							TCFT2.DisplayTCFT2()
						end,
						hotkey = iup.K_2
					},
					iup.fill { size="2" },
					iup.stationbutton {
						title = "Toggle Scanner", 
						EXPAND="HORIZONTAL",
						action = function(self)
							TCFT2.Mining.Scanner.Toggle()
						end,
					},
					iup.fill { size="2" },
					iup.stationbutton {
						title = "Toggle AutoEject", 
						EXPAND="HORIZONTAL",
						action = function(self)
							TCFT2.Mining.AutoEjectToggle()
						end,
					},
					margin="5x5",
				},
				expand="NO",
				size="%13x",
			},
			expand="NO",
			secinfoparent=parent,
			secinfochilds=chlds,
			displayed=false,
		}
	end

	-- Station
	local parent = iup.GetParent(iup.GetParent(iup.GetParent(iup.GetParent(StationSecondaryInfo))))
	local parent = iup.GetParent(iup.GetParent(StationSecondaryInfo))
	if (parent~=nil) then
		local chlds = {}
		local chld = iup.GetNextChild(parent)
		repeat
			if (chld~=nil) then
				table.insert(chlds, chld)
				chld = iup.GetNextChild(parent, chld)
			end
		until chld==nil
		TCFT2.StationSecInfo = iup.pdasubframe_nomargin{
			iup.hbox{
				iup.vbox{
					iup.stationbutton {
						title = "TCFT2", 
						EXPAND="HORIZONTAL",
						action = function(self)
							TCFT2.DisplayTCFT2()
						end,
						hotkey = iup.K_2
					},
					margin="5x5",
				},
				expand="NO",
				size="%13x",
			},
			expand="NO",
			secinfoparent=parent,
			secinfochilds=chlds,
			displayed=false,
		}
	end
	
	-- CapShip
	local parent = iup.GetParent(iup.GetParent(iup.GetParent(iup.GetParent(CapShipSecondaryInfo))))
	if (parent~=nil) then
		local chlds = {}
		local chld = iup.GetNextChild(parent)
		repeat
			if (chld~=nil) then
				table.insert(chlds, chld)
				chld = iup.GetNextChild(parent, chld)
			end
		until chld==nil
		TCFT2.CapshipSecInfo = iup.pdasubframe_nomargin{
			iup.hbox{
				iup.vbox{
					iup.stationbutton {
						title = "TCFT2", 
						EXPAND="HORIZONTAL",
						action = function(self)
							TCFT2.DisplayTCFT2()
						end,
						hotkey = iup.K_2
					},
					margin="5x5",
				},
				expand="NO",
				size="%13x",
			},
			expand="NO",
			secinfoparent=parent,
			secinfochilds=chlds,
			displayed=false,
		}
	end
end


TCFT2.AddButtons = function(secinfo)
	if (secinfo==nil) then return end
	if (secinfo.displayed) then return end
	for idx, chld in ipairs(secinfo.secinfochilds) do
		iup.Detach(chld)
	end
	iup.Append(secinfo.secinfoparent, secinfo)
	for idx, chld in ipairs(secinfo.secinfochilds) do
		iup.Append(secinfo.secinfoparent, chld)
	end
	secinfo.displayed=true
	iup.Refresh(secinfo.secinfoparent)
end

TCFT2.RemoveButtons = function(secinfo)
	if (secinfo==nil) then return end
	if (not secinfo.displayed) then return end
	iup.Detach(secinfo)
	secinfo.displayed=false
	iup.Refresh(secinfo.secinfoparent)
end


RegisterEvent(TCFT2.ProgressDialogUpdate, "TCFT2_UPDATEPROGRESSBAR")
RegisterEvent(TCFT2.ProgressDialogHide, "TCFT2_PROGRESSBARDONE")

-- TCFT2 UI
-- Settings Tab

-- General settings
local function createGeneralSettingsTab()
	local container
	TCFT2.txt_username = iup.text { value="", expand="YES", size="250x" }
	TCFT2.txt_password = iup.text { value="", expand="YES", size="250x", password="YES" }
	TCFT2.chk_chatecho = iup.stationtoggle{ value="OFF", title="Echo important messages to main chat", action=function(self, state) TCFT2.Settings.Data.chatecho = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_tcftchattomainchat = iup.stationtoggle{ value="ON", title="Echo TCFT chat to main chat", action=function(self, state) TCFT2.Settings.Data.tcftchattomainchat = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_autoconnect = iup.stationtoggle{ value="OFF", title="Automatically connect on VO startup", action=function(self, state) print(state) TCFT2.Settings.Data.autoconnect = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_displayguildlog = iup.stationtoggle{ value="ON", title="Notify me when guild members logs on and off", action=function(self, state) TCFT2.Settings.Data.displayguildlog = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_autorepair = iup.stationtoggle{ value="ON", title="Automatically repair ship when docking", action=function(self, state) TCFT2.Settings.Data.autorepair = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_autorefill = iup.stationtoggle{ value="ON", title="Automatically reload ammo when docking", action=function(self, state) TCFT2.Settings.Data.autorefill = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_broadcastlocation = iup.stationtoggle{ value="ON", title="Display your location in chat messages", action=function(self, state) TCFT2.Settings.Data.broadcastlocation = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_autoloadsectordata = iup.stationtoggle{ value="ON", title="Automatically load data for current sector", action=function(self, state) TCFT2.Settings.Data.autoloadsectordata = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_autonotifylastseen = iup.stationtoggle{ value="ON", title="Automatically display notification on lastseen update", action=function(self, state) TCFT2.Settings.Data.chk_autonotifylastseen = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_autonotifyhud = iup.stationtoggle{ value="ON", title="Automatically display notification on lastseen HUD", action=function(self, state) TCFT2.Settings.Data.chk_autonotifyhud = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_stationbuttons = iup.stationtoggle{ value="ON", title="Display TCFT2 buttons in Station", action=function(self, state) TCFT2.Settings.Data.stationbuttons = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_pdabuttons = iup.stationtoggle{ value="ON", title="Display TCFT2 buttons in PDA (when not in station)", action=function(self, state) TCFT2.Settings.Data.pdabuttons = (state==1 and "ON") or "OFF" end }
	TCFT2.chk_capshipbuttons = iup.stationtoggle{ value="ON", title="Display TCFT2 buttons in Capship", action=function(self, state) TCFT2.Settings.Data.capshipbuttons = (state==1 and "ON") or "OFF" end }

	container = iup.vbox {
		margin = "5x5",
		iup.hbox {
			iup.label { title="General Settings", font=Font.H3*HUD_SCALE },
			iup.fill {}
		},
		iup.fill { size="10" },
		iup.hbox {
			expand="NO",
			iup.vbox {
				expand="NO",
				gap="4",
				iup.hbox {
					iup.label { title="Username: ", size="100x" },
					TCFT2.txt_username,
				},
				iup.hbox {
					iup.label { title="Password: ", size="100x" },
					TCFT2.txt_password,
				},
				iup.fill { size="10" },
				iup.hbox {
					expand="ON",
					alignment="ARIGHT",
					iup.fill {},
				},
				TCFT2.chk_autoconnect,
				TCFT2.chk_autoloadsectordata,
				iup.fill{size="5"},
				TCFT2.chk_stationbuttons,
				TCFT2.chk_pdabuttons,
				TCFT2.chk_capshipbuttons,
			},
			iup.fill { size="50" },
			iup.vbox {
				TCFT2.chk_chatecho,
				TCFT2.chk_tcftchattomainchat,
				iup.fill { size="10"},
				TCFT2.chk_displayguildlog,
				TCFT2.chk_broadcastlocation,
				iup.fill { size="10"},
				TCFT2.chk_autorepair,
				TCFT2.chk_autorefill,
				iup.fill { size="10"},
				TCFT2.chk_autonotifylastseen,
				iup.fill { size="10"},
				TCFT2.chk_autonotifyhud,
			},
		},
		iup.fill { size=5 },
		iup.stationbutton {
			expand="NO",
			title="Save Settings",
			action=function()
				TCFT2.Settings.Save()
			end,
		},
		margin="20x20",
		gap="5",
	}
	container.tabtitle = "General"
	container.hotkey = iup.K_g
	function container:OnHelp()
		TCFT2.ShowHelp("SETTINGS_GENERAL")
	end
	return container
end

-- Roid Scanner setup (former AutoScanner)
local function createRoidScannerSettingsTab()
	local container

	TCFT2.Mining.Scanner.txt_searchtimeout = iup.text{ 
		size="150x", 
		ALIGNMENT="ARIGHT",
	}
	 
	TCFT2.Mining.Scanner.txt_scantimeout = iup.text{ 
		size="150x", 
		ALIGNMENT="ARIGHT",
	}

	container = iup.vbox {
		margin = "5x5",
		iup.hbox {
			iup.label { title="Asteroid Scanner Settings", font=Font.H3*HUD_SCALE },
			iup.fill {}
		},
		iup.fill { size="10" },
		iup.hbox {
			iup.label{ title="Search timeout", size="150x" },
			TCFT2.Mining.Scanner.txt_searchtimeout,
		},
		iup.hbox {
			iup.label{ title="Scan timeout", size="150x" },
			TCFT2.Mining.Scanner.txt_scantimeout,
		},
		margin="20x20",
		gap="5",
		iup.fill { size=5 },
		iup.stationbutton {
			expand="NO",
			title="Save Settings",
			action=function()
				TCFT2.Settings.Save()
			end,
		},
	}
	container.tabtitle = "Asteroid Scanner"
	container.hotkey = iup.K_a
	function container:OnHelp()
		TCFT2.ShowHelp("SETTINGS_ASTEROIDSCANNER")
	end
	return container
end

-- Info for keyboard binds
local binds_info = [[Keyboard Binds let you map hotkeys to commands. For example you could map the key T to /tc2 wich will open TCFT2 window by hitting the T-key.
That would eventually overwrite the bind for say_sector and you had to use /say_sector <message> everytime you wanted to talk to sector.

The function ]] .. TCFT2.colors.YELLOW .. "Test Binds" .. TCFT2.colors.NORMAL .. [[ will check that there are no binds on the keys you have choosen to bind in these settings.
So be sure to run the test before you save settings - else you risk overwriting existing binds.
			]]

-- Keyboard binds
local function createBindsSettingsTab()
	local container

	TCFT2.Binds.TC2 = iup.text { size="25x" }
	TCFT2.Binds.SAY_TC2 = iup.text { size="25x" }
	TCFT2.Binds.CARGOLIST = iup.text { size="25x" }
	TCFT2.Binds.SCANNER_TOGGLE = iup.text { size="25x" }
	TCFT2.Binds.AUTOEJECT_TOGGLE = iup.text { size="25x" }
	-- TCFT2.Binds.AUTOEJECT_TOGGLE = iup.text { size="25x" }
	TCFT2.Binds.LAST_SEEN_PROMPT = iup.text { size="25x" }
	
	TCFT2.Binds.TestBindsButton = iup.stationbutton {
		expand="NO",
		title="Test Binds",
		action=function()
			TCFT2.Settings.TestBinds()
		end,
	}


	container = iup.vbox {
		margin = "5x5",
		iup.hbox {
			iup.label { title="Keyboard Bind Settings", font=Font.H3*HUD_SCALE },
			iup.fill {}
		},
		iup.fill { size="3" }, -- 10 to 3 -- tokkana
		iup.hbox {
			iup.label { title = "Use " .. TCFT2.colors.YELLOW .. "Test Binds" .. TCFT2.colors.NORMAL .. " to test for conflicts." },
		},
		iup.fill { size="3" }, -- 10 to 3 -- tokkana
		iup.hbox {
			iup.label { title="Keyboard Binds", font=Font.H3*HUD_SCALE },
		},
		iup.fill { size="3" }, -- 10 to 3 -- tokkana
		iup.hbox {
			iup.label{ title="Show TCFT2", size="250x" },
			TCFT2.Binds.TC2,
		},
		iup.hbox {
			iup.label{ title="TCFT2 Chat", size="250x" },
			TCFT2.Binds.SAY_TC2,
		},
		iup.hbox {
			iup.label{ title="Show Cargolist", size="250x" },
			TCFT2.Binds.CARGOLIST,
		},
		iup.hbox {
			iup.label{ title="Toggle scanner on/off", size="250x" },
			TCFT2.Binds.SCANNER_TOGGLE,
		},
		iup.hbox {
			iup.label{ title="Toggle AutoEject on/off", size="250x" },
			TCFT2.Binds.AUTOEJECT_TOGGLE,
		},
		iup.hbox {
			iup.label{ title="Last Seen Prompt", size="250x" },
			TCFT2.Binds.LAST_SEEN_PROMPT,
		},
		iup.fill { size="3" }, -- 10 to 3 -- tokkana
		iup.hbox {
			TCFT2.Binds.TestBindsButton,
		},
		iup.fill { size=5 },
		iup.stationbutton {
			expand="NO",
			title="Save Settings",
			action=function()
				TCFT2.Settings.Save()
			end,
		},
		margin="5x5", -- 20x20 to 5x5 -- tokkana
		gap="5",
	}
	container.tabtitle = "Keyboard Binds"
	container.hotkey = iup.K_b
	function container:OnHelp()
		TCFT2.ShowHelp("SETTINGS_KEYBINDINGS")
	end
	return container
end

-- Mining tools
local function createMiningToolsSettingsTab()
	local container
	local ores = {
		"Heliocene Ore", "Pentric Ore", "Apicene Ore", "Pyronic Ore", "Denic Ore", "Lanthanic Ore", "Xithricite Ore", "Aquean Ore", "Silicate Ore", "Carbonic Ore", "Ferric Ore", "Ishik Ore", "VanAzek Ore"
	}
	
	TCFT2.AutoEjectOresKeep = {}
	
	local vbox = iup.vbox {}
	for idx, ore in ipairs(ores) do
		TCFT2.AutoEjectOresKeep[ore] = iup.stationtoggle{ value="OFF", title="Keep " .. ore, 
			action=function(self, state)
				TCFT2.Settings.Data.AutoEjectKeep[ore] = (state==1 and "ON") or "OFF"
			end,

		}
		iup.Append(vbox, TCFT2.AutoEjectOresKeep[ore])
	end
	
	TCFT2.AutoEjectActive = iup.stationtoggle{ value="OFF", title="AutoEject Active at startup", action=function(self, state) TCFT2.Settings.Data.AutoEjectActive = (state==1 and "ON") or "OFF" end }
	TCFT2.MiningSoundList = iup.stationsublist { dropdown='yes', size='%10x', expand='NO', visible_items=10 }
	TCFT2.MiningSoundList[1] = nil
	
	TCFT2.MiningSoundListAutoeject = iup.stationsublist { dropdown='yes', size='%10x', expand='NO', visible_items=10 }
	TCFT2.MiningSoundListAutoeject[1] = nil
	
	-- TCFT2.Sounds
	for idx,label in ipairs(TCFT2.SoundLabels) do
		TCFT2.MiningSoundList[idx] = label
		TCFT2.MiningSoundListAutoeject[idx] = label
	end
	
	container = iup.vbox {
		--margin="5x1", -- 5 to 1 -- tokkana
		iup.hbox {
		--	iup.label { title="Mining Tools", font=Font.H3 }, -- tokkana
        	iup.fill {},
		},
		--iup.fill { size="10" }, -- tokkana
		iup.hbox {
			iup.vbox {
				--iup.fill { size="8" }, -- tokkana
				iup.hbox {
					iup.label{ title="AutoEject", size="90x" }
				},
				--iup.fill { size="10" }, -- tokkana
				TCFT2.AutoEjectActive,
			--	iup.fill { size="5" }, -- tokkana
				vbox,
			},
			iup.vbox {
				--iup.fill { size="8" }, -- tokkana
				iup.hbox {
					iup.label{ title="Sounds", size="90x" }
				},
				--iup.fill { size="10" }, -- tokkana
				iup.hbox {
					iup.label { title="Play sound when cargo full ", size=Font.Default*12 }, iup.fill { size=5 }, TCFT2.MiningSoundList, iup.fill { size=5 },
					iup.stationbutton {
						title = "Test",
						action = function()
							local soundkey = TCFT2.MiningSoundList[TCFT2.MiningSoundList.value]
							if (soundkey) and (soundkey~="None") then
								local sound = TCFT2.Sounds[soundkey]
								if (sound) then
									gksound.GKPlaySound(sound, 1.0)
								end
							end
						end,
					},
				},
				iup.hbox {
					iup.label { title="Play sound when AutoEjecting ", size=Font.Default*12 }, iup.fill { size=5 }, TCFT2.MiningSoundListAutoeject, iup.fill { size=5 },
					iup.stationbutton {
						title = "Test",
						action = function()
							local soundkey = TCFT2.MiningSoundListAutoeject[TCFT2.MiningSoundListAutoeject.value]
							if (soundkey) and (soundkey~="None") then
								local sound = TCFT2.Sounds[soundkey]
								if (sound) then
									gksound.GKPlaySound(sound, 1.0)
								end
							end
						end,
					},
				},
			},
			gap="50",
		},
		iup.fill { size=20 },
		iup.stationbutton {
			expand="NO",
			title="Save Settings",
			action=function()
				TCFT2.Settings.Save()
			end,
		},
	}
	
	container.tabtitle = "Mining Tools"
	container.hotkey = iup.K_o
	function container:OnHelp()
		TCFT2.ShowHelp("SETTINGS_MININGTOOLS")
	end
	return container
end




TCFT2.CreateSettingsTab = function()
	local generalTab = createGeneralSettingsTab()
	local roidscannerTab = createRoidScannerSettingsTab()
	local bindsSettingsTab = createBindsSettingsTab()
	local miningtoolsSettingsTab = createMiningToolsSettingsTab()
	return TCFT2.subtabs { generalTab, roidscannerTab, miningtoolsSettingsTab, bindsSettingsTab,
		OnActivate = function(self, newtab, oldtab)
			TCFT2.Settings.UpdateUI()
		end
	}
end

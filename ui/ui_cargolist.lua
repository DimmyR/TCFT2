--[[
TCFT2
(c) By Plozi
Version 1.0.0

** Changelog

]]


TCFT2.CargoList.CreateMainWindow = function()

	if (TCFT2.CargoList.ItemList==nil) then
		TCFT2.CargoList.ItemList = TCFT2.MatrixList:new()
		TCFT2.CargoList.ItemList:addcolumn(TCFT2.MatrixList.column.new("itemname", "Item Name", "ALEFT", { "S:A:itemname", "N:A:distance" }, 60))
		TCFT2.CargoList.ItemList:addcolumn(TCFT2.MatrixList.column.new("distance", "Distance", "ARIGHT", { "N:A:distance", "S:A:itemname" }, 20, { postfix=" m", format="%0.1f" } ))
		TCFT2.CargoList.ItemList:addcolumn(TCFT2.MatrixList.column.new("cargocu", "Cargo CU", "ARIGHT", { "N:A:cargocu" }, 20, { postfix=" cu", format="%d" }))
		TCFT2.CargoList.ItemList:set_sortcol(2)
		TCFT2.CargoList.ItemList:init()

		TCFT2.CargoList.ItemList:setclickcb(
			function(self, line, col) -- click
				if (line==0) then
					TCFT2.CargoList.SortCol = col
					TCFT2.CargoList.UpdateCargoList()				
					return iup.IGNORE
				else
					local tmp = TCFT2.CargoList.ItemList:getitem(line)
					if (tmp~=nil) and (tmp.objid~=nil) and (tmp.nodeid~=nil) then
						if(targetless) then targetless.api.radarlock = true end
						radar.SetRadarSelection(tmp.nodeid, tmp.objid)
						if(targetless) then targetless.api.radarlock = false end
						TCFT2.CargoList.UserSelectedItem = true
					end
				end
			end
		)
	end
	
	TCFT2.CargoList.closeBtn = TCFT2.CargoList.closeBtn or iup.stationbutton {
		title = "Close",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			HideDialog(TCFT2.CargoList.MainWindow)
		end
	}

	TCFT2.CargoList.refreshBtn = TCFT2.CargoList.refreshBtn or iup.stationbutton {
		title = "Refresh (ENTER)",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			TCFT2.CargoList.Refresh()
		end,
	}
	
	TCFT2.CargoList.status = TCFT2.CargoList.status or iup.text { value="", expand="HORIZONTAL", readonly="YES", border="NO", bgcolor="40 40 40 128 *", marign="2x4" }

	local focusgrabber = iup.canvas {size="1x1", border="NO", expand="NO" }
	TCFT2.CargoList.MainWindow = TCFT2.CargoList.MainWindow or iup.dialog {
		iup.stationhighopacityframe {
			iup.vbox {
				margin="5x5",
				iup.hbox {
					iup.fill{}, iup.label { title="TCFT2 Cargolist", font=Font.H3*HUD_SCALE }, iup.fill{},
				},
				iup.hbox {
					TCFT2.CargoList.ItemList,
				},
				focusgrabber, 
				iup.hbox {
					iup.vbox {
						iup.fill { size="10"},
						iup.hbox {
							iup.fill{ size="4" }, TCFT2.CargoList.status, iup.fill{}, TCFT2.CargoList.refreshBtn, iup.fill{ size="4" }, TCFT2.CargoList.closeBtn,
						},
					},					
				},
			},
		},
		size="%40x%60",
		bgcolor="0 0 0 0 *",
		border="NO",menubox="NO",resize="NO",
		defaultesc=TCFT2.CargoList.closeBtn,
		defaultenter=TCFT2.CargoList.refreshBtn,
		defaultfocus=focusgrabber,
		topmost="YES",
		modal="YES",
		alignment="ACENTER",
		bgcolor="40 40 40 128 *",
		show_cb = function(self)
			TCFT2.CargoList.refreshBtn.active="YES"
			TCFT2.CargoList.SelectedNodeId, TCFT2.CargoList.SelectedObjectId = radar.GetRadarSelectionID()
			if (not TCFT2.CargoList.SectorScanned) then
				TCFT2.CargoList.Refresh()
			end
			iup.SetFocus(TCFT2.CargoList.refreshBtn)
		end,
		hide_cb = function()
			TCFT2.CargoList.Timer:Kill()
			if (not TCFT2.CargoList.UserSelectedItem) then
				if(targetless) then targetless.api.radarlock = true end
				if (TCFT2.CargoList.SelectedObjectId==nil) then
					gkinterface.GKProcessCommand("RadarNone")
				else
					radar.SetRadarSelection(TCFT2.CargoList.SelectedNodeId, TCFT2.CargoList.SelectedObjectId)
				end
				if(targetless) then targetless.api.radarlock = false end
			end
		end,
		map_cb = function()
			TCFT2.CargoList.ItemList:map()
		end,
	}
end

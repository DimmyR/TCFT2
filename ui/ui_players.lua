--[[
TCFT2 By Plozi
Player Stats functions
]]

TCFT2.ProgressBar = {
	create = function(color, width, fontsize, min, max)
		local pbar = {}
		pbar.bar = iup.stationprogressbar {
			-- LOWERCOLOR="64 255 64 128 *",
			LOWERCOLOR=color,
			--MIDDLEABOVECOLOR="0 255 0",
			--MIDDLEBELOWCOLOR="0 0 255",
			UPPERCOLOR="128 128 128 128 *",
			-- UPPERCOLOR=color,
			MINVALUE=(min or 0),
			MAXVALUE=(max or 100),
			type="HORIZONTAL",
			-- mode="TRINARY",
			expand="HORIZONTAL",
			--size="180x12",
			size="x"..tonumber(width),
			value=0,
		}
		pbar.label = iup.label { title="", alignment="ACENTER", font=(fontsize or Font.H6*HUD_SCALE), expand="HORIZONTAL" }
		pbar.zbox = iup.zbox { pbar.bar, iup.vbox { iup.fill{size=3}, pbar.label }, all="YES", alignment="ACENTER,ACENTER" }

		function pbar:settitle(title)
			self.label.title = title
		end

		function pbar:setvalue(val)
			self.bar.value = val
		end

		function pbar:iup()
			return self.zbox
		end

		function pbar:setminmax(min, max)
			self.bar.minvalue = (min or 0)
			self.bar.maxvalue = (max or 100)
		end
		
		return pbar
	end
}


-- PlayersTab
local function createPlayersByNationTab()
	local container

	-- local bar = getBar("64 255 64 128 *", "128 128 128 128 *", 0, 100)
	TCFT2.Players.LastSeenBars = {}
	for where=1, 5 do -- Itani to grey + overall = 5
		TCFT2.Players.LastSeenBars[where] = {}
		for fac=1, 3 do -- Factions
			TCFT2.Players.LastSeenBars[where][fac] = TCFT2.ProgressBar.create(FactionColor_RGB[fac] .. " 140 *", 4, Font.H6*HUD_SCALE)
		end
	end

	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			iup.label { title="Last Seen stats last 48 hours", font=Font.H3*HUD_SCALE }, 
			iup.fill { size=5 },
			iup.stationbutton {
				title = "Update", 
				hotkey = iup.K_u,
				action = function()
					TCFT2.Players.UpdateLastSeen48()
				end,
			},
			iup.fill {},
		},
		iup.fill { size="10" },
		iup.hbox {
			iup.hudrightframe {
				expand="VERTICAL",
				size="%47x",
				iup.vbox {
					margin="10x10",
					expand="YES",
					iup.label { title="Number of players spotted", font=Font.H3*HUD_SCALE },
					iup.fill { size=5 }, -- 35 to 5 -- tokkana
					iup.hbox {
						iup.label { title="In Total", font=Font.H3*HUD_SCALE, size="150x" },
						iup.vbox {
							TCFT2.Players.LastSeenBars[5][1]:iup(),
							TCFT2.Players.LastSeenBars[5][2]:iup(),
							TCFT2.Players.LastSeenBars[5][3]:iup(),
							gap=4,
						},
					},
					iup.fill { size=5 }, -- 35 to 5 -- tokkana
					iup.hbox {
						iup.vbox {
							iup.label { title="Itani space", font=Font.H3*HUD_SCALE, size="150x" },
						},
						iup.vbox {
							TCFT2.Players.LastSeenBars[1][1]:iup(),
							TCFT2.Players.LastSeenBars[1][2]:iup(),
							TCFT2.Players.LastSeenBars[1][3]:iup(),
							gap=4,
						},
					},
					iup.fill { size=5 }, -- 35 to 5 -- tokkana
					iup.hbox {
						iup.label { title="Serco space", font=Font.H3*HUD_SCALE, size="150x" },
						iup.vbox {
							TCFT2.Players.LastSeenBars[2][1]:iup(),
							TCFT2.Players.LastSeenBars[2][2]:iup(),
							TCFT2.Players.LastSeenBars[2][3]:iup(),
							gap=4,
						},
					},
------------ tokkana
					--iup.fill { size=5 }, -- 35 to 5 -- tokkana
					--iup.hbox {
					--	iup.label { title="UIT space", font=Font.H3, size="150x" },
					--	iup.vbox {
					--		TCFT2.Players.LastSeenBars[3][1]:iup(),
					--		TCFT2.Players.LastSeenBars[3][2]:iup(),
					--		TCFT2.Players.LastSeenBars[3][3]:iup(),
					--		gap=4,
					--	},
					--},
					--iup.fill { size=5 }, -- 35 to 5 -- tokkana
					--iup.hbox {
					--	iup.label { title="Grey space", font=Font.H3, size="150x" },
					--	iup.vbox {
					--		TCFT2.Players.LastSeenBars[4][1]:iup(),
					--		TCFT2.Players.LastSeenBars[4][2]:iup(),
					--		TCFT2.Players.LastSeenBars[4][3]:iup(),
					--		gap=4,
					--	},
					--},
------------ tokkana
					iup.fill {},
				},
			},
			iup.fill { size=10 },
------------ tokkana
iup.hudrightframe {
				expand="VERTICAL",
				size="%47x",
				iup.vbox {
					margin="10x10",
					expand="YES",
					iup.label { title="Number of players spotted", font=Font.H3*HUD_SCALE },
					iup.fill { size=5 }, -- 35 to 5 -- tokkana
					iup.hbox {
						iup.label { title="UIT space", font=Font.H3*HUD_SCALE, size="150x" },
						iup.vbox {
							TCFT2.Players.LastSeenBars[3][1]:iup(),
							TCFT2.Players.LastSeenBars[3][2]:iup(),
							TCFT2.Players.LastSeenBars[3][3]:iup(),
							gap=4,
						},
					},
					iup.fill { size=5 }, -- 35 to 5 -- tokkana
					iup.hbox {
						iup.label { title="Grey space", font=Font.H3*HUD_SCALE, size="150x" },
						iup.vbox {
							TCFT2.Players.LastSeenBars[4][1]:iup(),
							TCFT2.Players.LastSeenBars[4][2]:iup(),
							TCFT2.Players.LastSeenBars[4][3]:iup(),
							gap=4,
						},
					},
					iup.fill {},
				},
			},
------------ tokkana
			--iup.hudrightframe {
			--	expand="VERTICAL",
			--	size="%47x",
			--	iup.vbox {
			--		margin="5x5",
			--		expand="YES",
			--		iup.label { title="Frequence" },
			--	},
			--},
		},
	}

	container.tabtitle = "Last Seen Last 48h"
	-- container.hotkey = iup.K_s
	function container:OnHelp()
	   TCFT2.ShowHelp("LASTSEEN_GENERAL")
	end
	return container
end



TCFT2.CreatePlayersTab = function()
	local playersByNationTab = createPlayersByNationTab()
	-- local playersByGuildTab = createPlayersByGuildTab()
	return TCFT2.subtabs { playersByNationTab }

end


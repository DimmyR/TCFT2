-- TCFT2 UI
-- Mining Tab

local function createMineralSearchTab()
	local container

	TCFT2.Mining.MineralSearch.OreNameList = iup.stationsublist { dropdown='NO', size='%20x', expand='YES', bgcolor="0 0 0 128 *", border="NO" }
	TCFT2.Mining.MineralSearch.FilterList = iup.stationsublist { "None", "Above 90%", "Above 80%", "Above 70%", "Above 60%", "Above 50%", "Above 40%", "Above 30%", "Above 20%", "Above 10%"; dropdown='YES', expand='HORIZONTAL', bgcolor="0 0 0 128 *" }
	TCFT2.Mining.MineralSearch.SearchSystem = iup.stationsublist { dropdown="YES", expand="HORIZONTAL", visible_items=13} -- visible_items=25 to 13 -- tokkana
	
	TCFT2.Mining.MineralSearch.SearchMineralButton = iup.stationbutton { 
		title="Find Minerals",
		size = tostring(Font.Default*8).."x",
		action = function(self)
			TCFT2.Mining.MineralSearch.DoSearch()
		end,
		active="YES"
	}
	
	TCFT2.Mining.MineralSearch.OreList = TCFT2.MatrixList:new()
	TCFT2.Mining.MineralSearch.OreList:addcolumn(TCFT2.MatrixList.column.new("sector", "Sector", "ALEFT", { "S:A:sector", "N:A:objectid", "S:A:orename" }, 35))
	TCFT2.Mining.MineralSearch.OreList:addcolumn(TCFT2.MatrixList.column.new("objectid", "Roid #", "ARIGHT", { "N:A:objectid", "N:A:density" }, 10))
	TCFT2.Mining.MineralSearch.OreList:addcolumn(TCFT2.MatrixList.column.new("orename", "Mineral", "ALEFT", { "S:A:orename", "N:A:density" }, 25))
	TCFT2.Mining.MineralSearch.OreList:addcolumn(TCFT2.MatrixList.column.new("density", "Density", "ARIGHT", { "N:D:density" }, 10, { postfix=" %", format="%0.1f" }))
	TCFT2.Mining.MineralSearch.OreList:addcolumn(TCFT2.MatrixList.column.new("range", "Range", "ARIGHT", { "N:D:range" }, 10, { postfix=" m", replace={ ["-1"]="--" }}))
	TCFT2.Mining.MineralSearch.OreList:addcolumn(TCFT2.MatrixList.column.new("temp", "Temp", "ARIGHT", { "N:D:temp" }, 10, { postfix=" K", format="%0.1f", replace={ ["-1.0"]="--" }}))
	
	TCFT2.Mining.MineralSearch.OreList:setenteritemcb(function(self, line, col) -- Item is selected
		local record = TCFT2.Mining.MineralSearch.OreList.data.items[line]
		if (record) then
			local sectorid = tonumber(record.sectorid)
			if (sectorid==GetCurrentSectorid()) then -- We are in this sector - update temp and range
				gkinterface.GKProcessCommand("RadarNone")
				if(targetless) then targetless.api.radarlock = true end
				local objid = tonumber(record.objectid)
				radar.SetRadarSelection(3, objid) --lisa HA!  I think I found it!
				radar.SetRadarSelection(2, objid) --lisa HA!  I think I found it! It's (NodeID, objid)
				--lisa.  Added a second line for nodeid3.  easy way to do this... roids have been 2 or 3.
				-- seems to just ignore it if it doesn't exist.
				if (targetless) then targetless.api.radarlock = false end
				local dist = GetTargetDistance()
				if (dist) then
					local range = string.format("%0.1d", dist)
					TCFT2.Mining.MineralSearch.OreList.data.items[line].range = range
					TCFT2.Mining.MineralSearch.OreList:setcell(line, 5, range .. " m ")
					if (TCFT2.Mining.RoidInfo[objid]) then
						local temp = TCFT2.Mining.RoidInfo[objid].temperature or -1
						if (temp>-1) then
							TCFT2.Mining.MineralSearch.OreList:setcell(line, 6, string.format("%0.1f", tonumber(temp)) .. " K ")
						end
					end
				end
				
			end
		end
	end)
	
	TCFT2.Mining.MineralSearch.OreList:init()
	
	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			iup.label { title="Search For Mineral", font=Font.H3*HUD_SCALE }, iup.fill { size=10 },
			iup.stationbutton { title="Load full list for current sector", action=TCFT2.Mining.MineralSearch.DoSearchAllThisSector },
			iup.fill {},
		},
		iup.fill { size="10" },
		iup.hbox {
			iup.vbox {
				size="%20x", expand="VERTICAL",
				iup.fill { size="8" },
				iup.hbox {
					iup.label{ title="Search in ", size="90x" }, iup.fill{size="3"}, TCFT2.Mining.MineralSearch.SearchSystem,
				},
				iup.fill { size="6" },
				TCFT2.Mining.MineralSearch.OreNameList,
				iup.fill { size="6" },
				iup.hbox {
					iup.label{ title="Density Filter ", size="120x" }, iup.fill{size="3"}, TCFT2.Mining.MineralSearch.FilterList,
				},
				iup.fill { size="8" },
				iup.hbox {
					iup.fill{},
					TCFT2.Mining.MineralSearch.SearchMineralButton,
				}
			},
			iup.vbox {
				expand="YES",
				TCFT2.Mining.MineralSearch.OreList,
			},
			gap="5",
		},
	}
	
	container.tabtitle = "Ore Search"
	container.hotkey = iup.K_s
	function container:OnHelp()
	   TCFT2.ShowHelp("MINING_ORESEARCH")
	end
	return container
end
	

local function createMiningMapTab()
	local container
	
	local xres = gkinterface.GetXResolution()
	local yres = gkinterface.GetYResolution()
	
	local xs = xres * 0.7
	local ys = yres * 0.6
	local sz = xs
	if (xs>ys) then
		sz = ys
	else
		sz = xs
	end
	-- print(Font.Default)
	-- print(xres .. "x" .. yres)
	-- 1.583
	-- 1.77
	
	-- Font.Default - 1920 : 22
	-- 1280 : 17
	
	TCFT2.Mining.MiningMap.navmap = TCFT2.Mining.MiningMap.navmap or iup.navmap{
		size=sz.."x"..sz,
		--size="%55x%55",
		expand="NO", 
		ALIGNMENT="ACENTER" ,
		mouseover_cb = function(self, index, str)
			TCFT2.Mining.MiningMap.Map_Mouseover(index, str)
		end,
		click_cb = function(self, index, modifiers)
			--PloziNav.Editor.Map_Click(index, modifiers)
		end,
	}
	
	TCFT2.Mining.MiningMap.MineralList = TCFT2.Mining.MiningMap.MineralList or iup.stationhighopacitysublist{
		expand='VERTICAL',
		size="%20x",
		action = function(self, text, index, selection)
			TCFT2.Mining.MiningMapClick(text, index, selection)
		end,
		bgcolor="40 40 40 192 *",
	}
	
	TCFT2.Mining.MiningMap.Info = TCFT2.Mining.MiningMap.Info or iup.stationsubmultiline {
		readonly="YES",
		size="%20x",
		expand="VERTICAL",
		border="NO",
		bgcolor="40 40 40 192 *",
	}

	TCFT2.Mining.MiningMap.LookupSystem = iup.stationsublist { dropdown="YES", size="%10x", expand="NO", visible_items=15} -- visible_items=25 to 15 -- tokkana
	TCFT2.Mining.MiningMap.LoadSystemButtonTimer = TCFT2.Mining.MiningMap.LoadSystemButtonTimer or Timer()
	
	TCFT2.Mining.MiningMap.LoadSystemButton = TCFT2.Mining.MiningMap.LoadSystemButton or iup.stationbutton { 
		title="Load System",
		-- size = tostring(Font.Default*8).."x",
		action = function(self)
			if (TCFT2.Connection.isLoggedin ~= true) then
				TCFT2.ErrorDialog("Error", "You are not logged in.")
			else 
				TCFT2.Mining.MiningMap.LoadSystem()
			end
		end,
		active="YES"
	}

	container = iup.hbox {
		margin="4x4",
		iup.hbox {
			iup.vbox {
				iup.hbox {
					TCFT2.Mining.MiningMap.LookupSystem,
					iup.fill { size=5 },
					TCFT2.Mining.MiningMap.LoadSystemButton,
				},
				--iup.fill { size="10" }, -- tokkana
				--iup.label { title="Known Minerals in loaded system", font=Font.H3 }, -- tokkana
				iup.fill{ size="5" },
				TCFT2.Mining.MiningMap.MineralList,
			},
			iup.fill{},
			iup.vbox {
				alignment="ACENTER",
				iup.hbox {
					TCFT2.Mining.MiningMap.navmap,
				},
				iup.hbox {
					expand="NO",
					iup.label { title = "Mineral Density map", alignment="ACENTER", expand="NO", font=Font.H3*HUD_SCALE },
				},
				iup.fill { size="2" }, -- 20 to 2 -- tokkana
				iup.hbox {
					expand="NO",
					iup.vbox {
						alignment="ALEFT",
						iup.hbox {
							iup.label{ title="  ", size="15x", bgcolor="255 20 20" }, iup.fill{size="5"}, iup.label { title="Density below 25%" },
						},
						iup.fill { size="3" }, -- 10 to 3 -- tokkana
						iup.hbox {
							iup.label{ title="  ", size="15x", bgcolor="255 127 20" }, iup.fill{size="5"}, iup.label { title="Density 25-50%" },
						},
					},
					iup.fill { size="15"},
					iup.vbox {
						alignment="ALEFT",
						iup.hbox {
							iup.label{ title="  ", size="15x", bgcolor="255 255 40" }, iup.fill{size="5"}, iup.label { title="Density 50-75%" },
						},
						iup.fill { size="3" }, -- 10 to 3 -- tokkana
						iup.hbox {
							iup.label{ title="  ", size="15x", bgcolor="0 255 0" }, iup.fill{size="5"}, iup.label { title="Density above 75%" },
						},

					},				
				},
			},
			iup.fill{},
			iup.vbox {
				iup.label { title="Sector info", font=Font.H3*HUD_SCALE },
				iup.fill{ size="5" },
				TCFT2.Mining.MiningMap.Info,
			},
		},
	}
	container.tabtitle = "Mining Map"
	container.hotkey = iup.K_a
	function container:OnHelp()
	   TCFT2.ShowHelp("MINING_MININGMAP")
	end
	return container
end

-- List of ores pr system
local function createOrelistTab()
	TCFT2.Mining.OrelistPrSystem = TCFT2.Mining.OrelistPrSystem or {}
	local container

	TCFT2.Mining.OrelistPrSystem.UpdateBtn = iup.stationbutton { 
		title="Update List",
		size = tostring(Font.Default*8).."x",
		action = function(self)
			TCFT2.Mining.OrelistPrSystem.UpdateList()
		end,
		active="YES"
	}
	
	TCFT2.Mining.OrelistPrSystem.OreList = TCFT2.MatrixList:new()
	TCFT2.Mining.OrelistPrSystem.OreList:addcolumn(TCFT2.MatrixList.column.new("system", "System", "ALEFT", { "S:A:system", "S:A:orename", "N:A:numroids" }, 45))
	TCFT2.Mining.OrelistPrSystem.OreList:addcolumn(TCFT2.MatrixList.column.new("orename", "Mineral", "ALEFT", { "S:A:orename", "N:D:numroids" }, 45))
	TCFT2.Mining.OrelistPrSystem.OreList:addcolumn(TCFT2.MatrixList.column.new("numroids", "# of asteroids", "ARIGHT", { "S:A:numroids", "S:A:system", "S:A:orename" }, 10))
	
	TCFT2.Mining.OrelistPrSystem.OreList:init()
	
	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			iup.label { title="Ore per System", font=Font.H3*HUD_SCALE }, iup.fill { size=10 }, TCFT2.Mining.OrelistPrSystem.UpdateBtn, iup.fill {},
		},
		iup.fill { size="6" },
		TCFT2.Mining.OrelistPrSystem.OreList,
		iup.fill { size="6" },
		gap="5",
	}
	
	container.tabtitle = "Ore By System"
	container.hotkey = iup.K_o
	function container:OnHelp()
	   TCFT2.ShowHelp("MINING_OREPRSYSTEM")
	end
	return container
end


TCFT2.CreateMiningTab = function()
	local MineralSearchTab = createMineralSearchTab()
	local MiningMapTab = createMiningMapTab()
	local OrelistTab = createOrelistTab()
	local curtab = MineralSearchTab

	local ret = TCFT2.subtabs { MineralSearchTab, MiningMapTab, OrelistTab,
		OnActivate = function(self, newtab, oldtab)
			if (TCFT2.Connection.isLoggedin==true) then
				TCFT2.SendData({ cmd="610" }) -- Update orelist
			end
		end
	}
	return ret
end

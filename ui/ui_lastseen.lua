--[[
TCFT2
(c) By Plozi
Version 1.0.0

** Changelog

]]


TCFT2.LastSeen.CreateMainWindow = function()

	if (TCFT2.LastSeen.List==nil) then
		TCFT2.LastSeen.List = TCFT2.MatrixList:new()
		TCFT2.LastSeen.List:addcolumn(TCFT2.MatrixList.column.new("guildtag", "Guild", "ALEFT", { "S:A:guildtag", "S:A:name" }, 10))
		TCFT2.LastSeen.List:addcolumn(TCFT2.MatrixList.column.new("name", "Player Name", "ALEFT", { "S:A:name", "S:D:date" }, 27))
		TCFT2.LastSeen.List:addcolumn(TCFT2.MatrixList.column.new("date", "Last Seen", "ARIGHT", { "S:D:date" }, 15))
		TCFT2.LastSeen.List:addcolumn(TCFT2.MatrixList.column.new("location", "Location", "ALEFT", { "S:A:location", "S:D:date", "S:A:name" }, 22))
		TCFT2.LastSeen.List:addcolumn(TCFT2.MatrixList.column.new("shipname", "Ship", "ALEFT", { "S:A:shipname", "S:D:date" }, 26))
		TCFT2.LastSeen.List:set_sortcol(2)
		TCFT2.LastSeen.List:init()
	end
	
	TCFT2.LastSeen.Type = TCFT2.LastSeen.Type or iup.stationsublist { "Current Sector", "Player Name", "Guild", "Display ALL"; dropdown="YES",
		action=function(self, text, idx, state)
			if (state==1) then
				local num = tonumber(TCFT2.LastSeen.Type.value)
				if (num==1) or (num==4) then
					TCFT2.LastSeen.InputData.active="NO"
				else
					TCFT2.LastSeen.InputData.active="YES"
				end
			end
		end,
	}
	TCFT2.LastSeen.InputData = TCFT2.LastSeen.InputData or iup.text { value="", size="%25x", active="NO",
		action = function(self, ch, str)
			if (ch==13) then
				TCFT2.LastSeen.Update()
			end
		end,
	} 
	TCFT2.LastSeen.closeBtn = TCFT2.LastSeen.closeBtn or iup.stationbutton {
		title = "Close",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			HideDialog(TCFT2.LastSeen.MainWindow)
		end
	}

	local focusgrabber = iup.canvas {size="1x1", border="NO", expand="NO" }
	TCFT2.LastSeen.MainWindow = TCFT2.LastSeen.MainWindow or iup.dialog {
		iup.stationhighopacityframe {
			iup.vbox {
				margin="5x5",
				iup.hbox {
					iup.fill{}, iup.label { title="TCFT2 Last Seen List", font=Font.H3*HUD_SCALE }, iup.fill{},
				},
				iup.hbox {
					margin="10x10",
					iup.label { title="List By: " },
					TCFT2.LastSeen.Type,
					iup.fill { size=3 },
					TCFT2.LastSeen.InputData,
					iup.fill { size=3 },
					iup.stationbutton {
						title="Update",
						hotkey=iup.K_u,
						action=function()
							TCFT2.LastSeen.Update()
						end,
					},
				},
				iup.hbox {
					TCFT2.LastSeen.List,
				},
				focusgrabber, 
				iup.hbox {
					iup.vbox {
						iup.fill { size="10"},
						iup.hbox {
							iup.fill {}, TCFT2.LastSeen.closeBtn, iup.fill {},
						},
					},					
				},
			},
		},
		size="%80x%80",
		bgcolor="40 40 40 150 *",
		border="NO",menubox="NO",resize="NO",
		defaultesc=TCFT2.LastSeen.closeBtn,
		defaultenter=TCFT2.LastSeen.refreshBtn,
		defaultfocus=focusgrabber,
		topmost="YES",
		modal="YES",
		alignment="ACENTER",
		bgcolor="40 40 40 128 *",
		show_cb = function(self)
			TCFT2.LastSeen.List:map()
			if ((tonumber(TCFT2.LastSeen.Type.value) or 0)<1) then TCFT2.LastSeen.Type.value = 1 end
		end,
		hide_cb = function()
		end,
		map_cb = function()
			
		end,
	}
end


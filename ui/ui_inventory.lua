--[[
ui.inventory
]]

local function createAllInventoryTab()
	local container

	TCFT2.Inventory.LookupList = TCFT2.MatrixList:new()
	TCFT2.Inventory.LookupList:addcolumn(TCFT2.MatrixList.column.new("location", "Location", "ALEFT", { "S:A:location", "S:A:station" }, 25))
	TCFT2.Inventory.LookupList:addcolumn(TCFT2.MatrixList.column.new("station", "Station", "ALEFT", { "S:A:station" }, 25))
	TCFT2.Inventory.LookupList:addcolumn(TCFT2.MatrixList.column.new("itemname", "Item Name", "ALEFT", { "S:A:itemname" }, 40))
	TCFT2.Inventory.LookupList:addcolumn(TCFT2.MatrixList.column.new("quantity", "Quantity", "ARIGHT", { "N:A:quantity" }, 10))

	TCFT2.Trading.LookupItemType = iup.stationsublist { dropdown='yes', size='%20x', expand='NO', visible_items=15 }
	TCFT2.Inventory.TotalItems = iup.label { title="0", wordwrap="NO", expand="HORIZONTAL" }
	TCFT2.Inventory.LookupList:init()
	
	local RefreshButton = iup.stationbutton { 
		title="Refresh",
		action = function(self)
			TCFT2.Inventory.RefreshList()
		end,
		hotkey=iup.K_r,
	}
	
	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			iup.label { title="Inventory List", font=Font.H3*HUD_SCALE }, iup.fill { size=25 }, 
			iup.label { title="Display item " },
			TCFT2.Trading.LookupItemType,
			iup.fill { size=15 },
			RefreshButton, iup.fill {},
		},
		iup.fill { size=10 },
		iup.hbox {
			expand="YES",
			TCFT2.Inventory.LookupList,
		},
		iup.fill { size=10 },
		iup.hbox {
			alignment="ARIGHT",
			expand="YES",
			margin="5x0",
			iup.label { title="Total number of items ", expand="HORIZONTAL", alignment="ARIGHT" },
			TCFT2.Inventory.TotalItems,
		},
	}
	
	container.tabtitle = "Inventory List"
	container.hotkey = iup.K_l
	function container:OnHelp()
	   TCFT2.ShowHelp("INVENTORY_ALL")
	end

	return container
end

TCFT2.CreateInventoryTab = function()
	local AllInventoryTab = createAllInventoryTab()
	
	return TCFT2.subtabs { AllInventoryTab,
		OnActivate = function(self, newtab, oldtab)
			TCFT2.Inventory.UpdateItemlist()
		end
	}
end

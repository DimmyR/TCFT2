--[[
PloziNav
(c) By Plozi
Version 2.0.0

** Changelog

2.0.0 - PloziNav Data store on server and editable directly from the plugin
1.0.5 - Now also roundtrips should be plotted correctly - good for proc missions

** IMPLEMENTED INTO TCFT2 **

]]

-- Navigation computer
TCFT2.PloziNav = TCFT2.PloziNav or {}

-- Navigation Computer Editor
dofile('navigation_editor.lua')
-- Load UI definitions
dofile('ui/ui_navigation.lua')
-- Load the navigation data file
-- dofile('PloziNav.Data.lua')
TCFT2.PloziNav.Routes = {}
TCFT2.PloziNav.LastRoute = {}
TCFT2.PloziNav.DestinationSector = 0
TCFT2.PloziNav.UserInfo = {}

-- Add controls to nav screen
TCFT2.PloziNav.AddControls = function(tab)
	local panel = TCFT2.PloziNav.UI.CreatePanel()
	local container = iup.GetBrother(iup.GetBrother(iup.GetNextChild(tab)))
	container:append(
		iup.pdasubframe_nomargin{
			iup.hbox {
				iup.vbox {
					iup.hbox {
						iup.fill {},
						iup.label { title="Plozi's Navigation Computer" },
						iup.fill {},
					},
					iup.hbox {
						iup.fill {},
						panel,
						iup.fill {},
						margin="0x5",
					},
					margin="5x5",
				},
			},
			expand="HORIZONTAL",
		}
	)
end

TCFT2.PloziNav.GetRoute = function(from, to)
	local res = {}
	if TCFT2.PloziNav.Routes[from.."-"..to] then
		res = TCFT2.PloziNav.Routes[from.."-"..to]
	end
	return res
end

-- SplitSectorID - return SystemID, X, Y

-- PlotRoute
TCFT2.PloziNav.PlotRoute = function()
	local currSectorId = GetCurrentSectorid()
	local destSectorId = NavRoute.GetFinalDestination()

	local currRoute = NavRoute.GetCurrentRoute()
	local currFullRoute = GetFullPath(currSectorId, currRoute)

	if destSectorId==nil then
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Unknown destination sector.", true)
		return
	end
	if destSectorId==currSectorId and #currFullRoute<1 then
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Start and destination sector are the same - nothing to plot.", true)
		return
	end

	local fullRoute = {}
	local newRoute = {}
	local done = {}
	local prev = 0
	for i,v in ipairs(currFullRoute) do
		if prev ~= v then
			table.insert(fullRoute, v)
			prev = v
		end
	end
	local to = ""
	local from = ""
	for i=1,#fullRoute-1 do
		from = fullRoute[i]
		to = fullRoute[i+1]
		if from~=nil and to~=nil then
			local points = TCFT2.PloziNav.GetRoute(from, to)
			if (#points) then
				for i,v in ipairs(points) do
					table.insert(newRoute, tonumber(v))		
				end
			else
				table.insert(newRoute, from)
			end
		end
	end
	table.insert(newRoute, to) -- Be sure to add the wery last location

	if #newRoute>0 then
		NavRoute.clear()		
		-- NavRoute.SetFinalDestination(destSectorId)
		NavRoute.SetFullRoute(newRoute)
		TCFT2.PloziNav.LastRoute = {}
		for _, hop in ipairs(newRoute) do -- Save route
			table.insert(TCFT2.PloziNav.LastRoute, hop)
		end
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "New route plotted (" .. ShortLocationStr(currSectorId) .. " -> " .. ShortLocationStr(destSectorId) .. ")", true)
	else
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Could not plot route. Unknown error.", true)
		return
	end
end

-- Repeat last route
TCFT2.PloziNav.RepeatLast = function()
	NavRoute.clear()
	NavRoute.SetFullRoute(TCFT2.PloziNav.LastRoute)
end


TCFT2.PloziNav.LoadServerNavData = function()
	local broadcast = (TCFT2.PloziNav.UI.EditDialog.visible=="YES" and false) or true
	--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Attempting to load navigation data from server...", broadcast)
	TCFT2.SendData({ cmd="1200" })
end

-- Initialize PloziNav
TCFT2.PloziNav.Initialize = function()
	TCFT2.PloziNav.Editor.Initialize()
	--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Navigation computer initialized.", true)
	TCFT2.PloziNav.LoadServerNavData()
end

--[[
Route editing functions
]]

-- Initialize - add controls to the nav-tabs
TCFT2.PloziNav.AddControls(PDAShipNavigationTab)
TCFT2.PloziNav.AddControls(StationPDAShipNavigationTab)
TCFT2.PloziNav.AddControls(CapShipPDAShipNavigationTab)


--[[
TCFT2 - UI stuff
]]

--[[
***************
SCANNER SECTION
***************
]]

TCFT2.Mining = {}
TCFT2.Mining.OreList = {}

TCFT2.Mining.MineralSearch = {}
TCFT2.Mining.MineralSearch.SortMode = 1
TCFT2.Mining.MineralSearch.LastOreUpdate = 0
TCFT2.Mining.MineralSearch.Results = {}
TCFT2.Mining.MineralSearch.LastItem = ""
TCFT2.Mining.MineralSearch.LastItemBg = 0
TCFT2.Mining.MineralSearch.Cancelled = false

TCFT2.Mining.Scanner = {}
TCFT2.Mining.Scanner.Running = false
TCFT2.Mining.Scanner.ScannedRoids = {}
-- ScannerPort id - the port we have equipped a scanner
TCFT2.Mining.Scanner.ScannerPortId = 0
TCFT2.Mining.Scanner.ScannerRange = 0 -- Longest range of equipped scanner
TCFT2.Mining.Scanner.StationTurnedOff = false -- Did we auto-turnoff scanner when docking?
TCFT2.Mining.Scanner.Timer = Timer()
TCFT2.Mining.Scanner.CurrentObjectId = 0
TCFT2.Mining.Scanner.KnownObjects = {}

TCFT2.Mining.InfoTimer = Timer()

TCFT2.Mining.Scanner.ShipsWithScanners = {}
TCFT2.Mining.Scanner.ShipsWithScanners["Warthog Mineral Extractor"] = 500
TCFT2.Mining.Scanner.ShipsWithScanners["Behemoth Heavy Miner MkII"] = 1000

TCFT2.Mining.CurrentSystem = TCFT2.Mining.CurrentSystem or GetCurrentSystemid()
TCFT2.Mining.LoadedSystem = 0
TCFT2.Mining.LoadSystemCancelled = false

TCFT2.Mining.CurrentSectorid = nil
TCFT2.Mining.RoidBuffer = {} -- Buffer for submitting sector info
TCFT2.Mining.RoidInfo = {} -- Info of roids in this sector
TCFT2.Mining.RoidInfoSystem = {} -- Roid infor for all system.... HEAVY STUFF MAYBE...
TCFT2.Mining.RoidSendBuffer = {}

TCFT2.Mining.OrelistPrSystem = {}

TCFT2.Mining.MiningMap = {}
TCFT2.Mining.MiningMap.SectorMinerals = {}

TCFT2.Mining.LoadOrelistCancelled = false

TCFT2.ScannerThread = nil

dofile("ui/ui_mining.lua")


-- Start scanner
TCFT2.Mining.Scanner.Start = function()
	TCFT2.Mining.Scanner.StationTurnedOff = false
	if (TCFT2.Mining.Scanner.Running) then
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Scanner already on." .. TCFT2.colors.NORMAL, true)
		return
	end
	if (TCFT2.Mining.Scanner.FindScannerPort()) then
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Starting scanning with scanner found at port: " .. TCFT2.Mining.Scanner.ScannerPortId .. " - Max range: " .. TCFT2.Mining.Scanner.ScannerRange .. TCFT2.colors.NORMAL, true)
		TCFT2.Mining.Scanner.Running = true
		if(targetless) then targetless.api.radarlock = true end
		gkinterface.GKProcessCommand("RadarNone")
		TCFT2.Mining.Scanner.ScanNext()
	else
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "No scanners found. Could not start scanning." .. TCFT2.colors.NORMAL, true)
		TCFT2.Mining.Scanner.Running = false
	end
	TCFT2.Mining.Scanner.StationTurnedOff = false
end

-- Stop scanner
TCFT2.Mining.Scanner.Stop = function()
	if(targetless) then targetless.api.radarlock = false end
	if (not TCFT2.Mining.Scanner.Running) then
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Scanner not running." .. TCFT2.colors.NORMAL, true)
		return
	end
	TCFT2.Mining.Scanner.Running = false
	--RegisterEvent(HUD, "TARGET_CHANGED")
	TCFT2.Mining.Scanner.KillTimer()
	TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Scanner turned off." .. TCFT2.colors.NORMAL, true)
	TCFT2.Mining.Scanner.StationTurnedOff = false
	gkinterface.GKProcessCommand("RadarNone")
end

-- Toggle scanner
TCFT2.Mining.Scanner.Toggle = function()
	if (TCFT2.Mining.Scanner.Running) then
		TCFT2.Mining.Scanner.Stop()
	else
		TCFT2.Mining.Scanner.Start()
	end
end


TCFT2.Mining.Scanner.KillTimer = function()
	if (TCFT2.Mining.Scanner.Timer:IsActive()) then
		TCFT2.Mining.Scanner.Timer:Kill()
	end
end

TCFT2.Mining.Scanner.ScanNext = function()
	if (TCFT2.Mining.Scanner.Running) and (PlayerInStation()==false) then
		gkinterface.GKProcessCommand("RadarNext")
		local nodeid, objectid = radar.GetRadarSelectionID()
		local name, health, dist, factionid, guild, ship = GetTargetInfo()	
		--nodeid does not identify roid per raybondo
		if (objectid~=nil) and (string.match(name, "Asteroid") or string.match(name, "Ice Crystal")) then -- we hit a roid
			if (not TCFT2.Mining.Scanner.ScannedRoids[objectid]) then -- Already scanned this visit of the sector? - if so dont bother scan again
				local name, health, dist, factionid, guild, ship = GetTargetInfo()
				if (name==nil) then
					TCFT2.Mining.Scanner.Timer:SetTimeout(TCFT2.Settings.Data.Scanner.searchtimeout, TCFT2.Mining.Scanner.ScanNext)
				end
				dist = tonumber(dist or -1)
				if (dist>-1) and (dist<=TCFT2.Mining.Scanner.ScannerRange) then -- In scanner range?
					TCFT2.Mining.Scanner.Timer:SetTimeout(TCFT2.Settings.Data.Scanner.scantimeout, TCFT2.Mining.Scanner.ScanNext)
					-- lisa Give the scanner time to work
				else -- Not in scanner range - just continue to next one after searctimeout millisecs
					TCFT2.Mining.Scanner.Timer:SetTimeout(TCFT2.Settings.Data.Scanner.searchtimeout, TCFT2.Mining.Scanner.ScanNext)
				end
			else -- Already scanned this roid
				TCFT2.Mining.Scanner.Timer:SetTimeout(TCFT2.Settings.Data.Scanner.searchtimeout, TCFT2.Mining.Scanner.ScanNext)
			end
		else -- Not a roid
			--if (objectid~=nil) then
				TCFT2.Mining.Scanner.Timer:SetTimeout(TCFT2.Settings.Data.Scanner.searchtimeout, TCFT2.Mining.Scanner.ScanNext)
		end
	else 
		TCFT2.Mining.Scanner.KillTimer()
		gkinterface.GKProcessCommand("RadarNone") --Lisa added.  To un-target after scanning.
	end	
end

-- FindScannerPort - try to find a port with holding a scanner
TCFT2.Mining.Scanner.FindScannerPort = function()
	local NumPorts = GetActiveShipNumAddonPorts()
	local ScannerFound = false
	local MaxRange = 0
	for PortNo=1,NumPorts do
		local ItemId = GetActiveShipItemIDAtPort(PortNo)
		local ipath,Name,quant,mass,sdesc,desc1,desc2,container,classtype,subtype = GetInventoryItemInfo(ItemId)
		if desc1 then
			local srange = string.match(desc1, "Range: (%d+)")
			local range = -1
			if srange then
				range = tonumber(srange)
			end
			if (Name) then	
				if string.match(Name, 'Scanner') then
					if range > 0 then
						ScannerFound = true
						if MaxRange < range then
							TCFT2.Mining.Scanner.ScannerPortId = PortNo
							MaxRange = range
						end
					end
				end
			end
		end
	end
	-- Also check for built-in scanners - for now we have to check on ship-names
	local shipname = GetActiveShipName()
	if (shipname) then
		for name,range in pairs(TCFT2.Mining.Scanner.ShipsWithScanners) do
			if (shipname==name) then
				range = tonumber(range)
				if (MaxRange < range) then
					MaxRange = range
					TCFT2.Mining.Scanner.ScannerPortId = "(Built-in scanner)"
					ScannerFound = true
				end
			end
		end
	end
	if ScannerFound then
		TCFT2.Mining.Scanner.ScannerRange = tonumber(MaxRange)
		return true
	end
	return false
end


--[[
**********************
MINERAL SEARCH SECTION
**********************
]]

-- Do the search - send request to the server
TCFT2.Mining.MineralSearch.DoSearch = function() -- Search for minerals
	local orename = TCFT2.Mining.MineralSearch.OreNameList[TCFT2.Mining.MineralSearch.OreNameList.value]
	local densityfilter = (TCFT2.Mining.MineralSearch.FilterList.value or 0)
	local syssel = tonumber(TCFT2.Mining.MineralSearch.SearchSystem.value)
	local sysid = -1
	if (syssel>1) then
		local system = string.lower(TCFT2.Trading.LookupSystem[syssel])
		local tmp = string.match(system, "^(%S+)")
		sysid = SystemNames[tmp] or -1
	elseif (syssel==1) then -- current sector
		sysid = GetCurrentSectorid()
		sysid = -sysid
	else
		TCFT2.MessageDialog("Mineral Search", "You have to select a system to search in.")
		return
	end
	if (orename) then
		TCFT2.Mining.MineralSearch.Results = {}
		TCFT2.Mining.MineralSearch.Cancelled = false
		TCFT2.ProgressDialog("Receiving Results", "Receiving Search Result, please wait...", 
			function()
				TCFT2.Mining.MineralSearch.Cancelled=true
			end
		)
		TCFT2.SendData({ cmd="600", orename=orename, densityfilter=densityfilter, systemid=sysid })
	end
end

-- Full list of ores for this sector
TCFT2.Mining.MineralSearch.DoSearchAllThisSector = function()
	local sysid = GetCurrentSectorid()
	sysid = -sysid
	TCFT2.Mining.MineralSearch.Results = {}
	TCFT2.Mining.MineralSearch.Cancelled = false
	TCFT2.ProgressDialog("Receiving Results", "Receiving Search Result, please wait...", 
		function()
			TCFT2.Mining.MineralSearch.Cancelled=true
		end
	)
	TCFT2.SendData({ cmd="600", orename="ALL_ORES_AVAILABLE", densityfilter="", systemid=sysid })
end

-- Update search matrix
TCFT2.Mining.MineralSearch.UpdateMineralSearchTable = function() -- Update the matrix table based on TCFT2.Mining.MineralSearch.ResultTable content - sorted by sortmode
	TCFT2.Mining.MineralSearch.OreList:clear()
	if (#TCFT2.Mining.MineralSearch.Results>0) then
		for i, ore in ipairs(TCFT2.Mining.MineralSearch.Results) do
			ore.sector = ShortLocationStr(ore.sectorid)
			ore.temp = -1.0
			TCFT2.Mining.MineralSearch.OreList:additem(ore)
		end
		TCFT2.Mining.MineralSearch.OreList:update()
	else
		TCFT2.Mining.MineralSearch.OreList.numlin = 2
		TCFT2.Mining.MineralSearch.OreList:setcell(2, 3, "  No Matching")
	end
end

TCFT2.Mining.MineralSearch.UpdateOrelist = function() -- Update list of ores (UI)
	TCFT2.Mining.MineralSearch.OreNameList[1] = nil
	-- TCFT2.Mining.MineralSearch.OreNameList[1] = "Rare Ores"
	for i, orename in ipairs(TCFT2.Mining.OreList) do
		TCFT2.Mining.MineralSearch.OreNameList[i] = orename
	end
	TCFT2.Mining.MineralSearch.OreNameList.value = 1
end

TCFT2.Mining.MineralSearch.UpdateTimer = Timer()
TCFT2.Mining.MineralSearch.UpdateDistAndTemp = function()
	local dist = GetTargetDistance()
	if (dist) then
		local range = string.format("%0.1d", dist)
		TCFT2.Mining.MineralSearch.OreList.data.items[line].range = range
		TCFT2.Mining.MineralSearch.OreList:setcell(line, 5, range .. " m ")
		if (TCFT2.Mining.RoidInfo[objid]) then
			local temp = TCFT2.Mining.RoidInfo[objid].temperature or -1
			if (temp>-1) then
				TCFT2.Mining.MineralSearch.OreList:setcell(line, 6, string.format("%0.1f", tonumber(temp)) .. " K ")
			end
		end
	end
end

-- ********************
-- GENERAL MINING STUFF
-- ********************

-- Submit data for current sector (when entering a new sector)
TCFT2.Mining.SubmitSector = function()
	if (TCFT2.Connection.isLoggedin) and (TCFT2.Mining.CurrentSectorId ~= nil) then
		for objid, data in pairs(TCFT2.Mining.Scanner.ScannedRoids) do
			-- Only submit if we have new records in scannedroids
			TCFT2.Mining.RoidBuffer = {}
			for objid, roid in pairs(TCFT2.Mining.RoidInfo) do
				table.insert(TCFT2.Mining.RoidBuffer, roid)
			end
			TCFT2.SendData({ cmd="500" })
			break
		end
	end
end

-- Load data for entered sector
TCFT2.Mining.LoadSector = function()
	if (TCFT2.Connection.isLoggedin) then
		local sectorid = GetCurrentSectorid()
		TCFT2.Mining.RoidInfo = {}
		TCFT2.SendData({ cmd="700", sectorid=sectorid})
	end
end


--**********
-- ORELIST *
--**********

TCFT2.Mining.OrelistPrSystem.UpdateList = function()
	if (TCFT2.Connection.isLoggedin) then
		TCFT2.Mining.OrelistPrSystem.OreList:clear()
		TCFT2.Mining.LoadOrelistCancelled = false
		TCFT2.ProgressDialog("Ore Pr System", "Receiving result, please wait...", 
			function() 
				TCFT2.Mining.LoadOrelistCancelled=true
			end
		)
		TCFT2.SendData({ cmd="730" })
	else
		TCFT2.ErrorDialog("Ore pr System", "Error - you are not logged in.")
	end
end

-- ********************
-- MINING MAP FUNCTIONS
-- ********************

-- Load data for this system
TCFT2.Mining.MiningMap.LoadSystem = function()
	if (TCFT2.Connection.isLoggedin) then
		local syssel = tonumber(TCFT2.Mining.MiningMap.LookupSystem.value)
		if (syssel>0) then
			local sysname = TCFT2.Mining.MiningMap.LookupSystem[syssel]
			local system = string.lower(sysname)
			local tmp = string.match(system, "^(%S+)")
			local systemid = SystemNames[tmp] or -1
			if (systemid>-1) then
				TCFT2.Mining.RoidInfoSystem = {}
				TCFT2.Mining.LoadSystemCancelled = false
				TCFT2.ProgressDialog("Loading System", "Loading system data for\n" .. TCFT2.colors.GREEN2 .. sysname .. TCFT2.colors.NORMAL .. "\nplease wait...", 
					function()
						TCFT2.Mining.LoadSystemCancelled=true
					end
				)
				TCFT2.Mining.CurrentSystem = systemid
print("XX: "..TCFT2.Mining.CurrentSystem)
				TCFT2.SendData({ cmd="710", systemid=systemid })
			else
				TCFT2.ErrorDialog("Load System", "An error occured while loading Systemdata.")
			end
		end
	end
end

-- Update the map if not loaded already
TCFT2.Mining.MiningMap.UpdateMap = function(orename, force)
	local colors = {
		[0]="255 20 20", -- red
		[1]="255 127 20", -- orange
		[2]="255 255 40", -- yellow
		[3]="0 255 0" -- green
	}
	orename = (orename or "")
	local mappath = string.format("lua/maps/system%02dmap.lua", TCFT2.Mining.CurrentSystem)
	TCFT2.Mining.MiningMap.navmap:loadmap(2, mappath, TCFT2.Mining.CurrentSystem - 1)
	
	local color = ""
	for sector, data in pairs(TCFT2.Mining.MiningMap.SectorMinerals) do
		color = nil
		if (orename~="") then
			if (data[orename]~=nil) then
				if (tonumber(data[orename].max)>75.0) then
					color = colors[3]
				elseif (tonumber(data[orename].max)>50.0) then
					color = colors[2]
				elseif (tonumber(data[orename].max)>25.0) then
					color = colors[1]
				elseif (tonumber(data[orename].max)>0.0) then
					color = colors[0]
				end
			else
				color = "0 0 0 128"
			end
		else
			if (tonumber(data["TCFT2AVERAGE"].max)>75.0) then
				color = colors[3]
			elseif (tonumber(data["TCFT2AVERAGE"].max)>50.0) then
				color = colors[2]
			elseif (tonumber(data["TCFT2AVERAGE"].max)>25.0) then
				color = colors[1]
			elseif (tonumber(data["TCFT2AVERAGE"].max)>0.0) then
				color = colors[0]
			end
		end
		if (color) then
			TCFT2.Mining.MiningMap.navmap["COLOR" .. sector] = color
		end
	end
end

-- Click on miningmap ore
TCFT2.Mining.MiningMapClick = function(text, index, selection)
	local oreselected =""
	local idx = tonumber(index)
	if (selection==1) then
		if (idx>1) then
			oreselected = (string.match(text, "^(.-)%sOre") or "")
		end
		TCFT2.Mining.MiningMap.UpdateMap(oreselected)
	end
end

-- UpdateMapInfo - done after syste is loaded
TCFT2.Mining.MiningMap.UpdateMapInfo = function()
	local orenames = {}
	TCFT2.Mining.MiningMap.SectorMinerals = {}
	TCFT2.Mining.RoidCount = {}
	TCFT2.Mining.RoidCountPrOre = {}
	for idx, roid in ipairs(TCFT2.Mining.RoidInfoSystem) do
		local sectorid = tonumber(roid.sectorid)
		TCFT2.Mining.RoidCount[sectorid] = (TCFT2.Mining.RoidCount[sectorid] or 0) + 1
		TCFT2.Mining.RoidCountPrOre[sectorid] = TCFT2.Mining.RoidCountPrOre[sectorid] or {}
		if (TCFT2.Mining.MiningMap.SectorMinerals[sectorid]==nil) then
			TCFT2.Mining.MiningMap.SectorMinerals[sectorid] = { TCFT2AVERAGE={ max=0 } }
		end
		for idx, ore in ipairs(roid.ores) do
			local orename = tostring(ore.orename)
			local density = tonumber(ore.density)
			if (orename~="") then
				if (TCFT2.Mining.MiningMap.SectorMinerals[sectorid][orename]==nil) then
					TCFT2.Mining.MiningMap.SectorMinerals[sectorid][orename] = { name=orename, max=density, min=density }
				end
				TCFT2.Mining.RoidCountPrOre[sectorid][orename] = (TCFT2.Mining.RoidCountPrOre[sectorid][orename] or 0) + 1
				if (density<TCFT2.Mining.MiningMap.SectorMinerals[sectorid][orename].min) then TCFT2.Mining.MiningMap.SectorMinerals[sectorid][orename].min = density end
				if (density>TCFT2.Mining.MiningMap.SectorMinerals[sectorid][orename].max) then TCFT2.Mining.MiningMap.SectorMinerals[sectorid][orename].max = density end
				if (idx>2) then TCFT2.Mining.MiningMap.SectorMinerals[sectorid]["TCFT2AVERAGE"].max = ((TCFT2.Mining.MiningMap.SectorMinerals[sectorid]["TCFT2AVERAGE"].max + density)/2) end
				orenames[orename .. " Ore"] = orenames[orename .. " Ore"] or orename
			end
		end
	end
	-- Populate the ore-list
	local orenames2 = {}
	for orename, tmp in pairs(orenames) do
		table.insert(orenames2, orename)
	end
	table.sort(orenames2)
	TCFT2.Mining.MiningMap.MineralList[1] = nil
	TCFT2.Mining.MiningMap.MineralList[1] = "Average for all Ores"
	local i = 2
	for _, orename in ipairs(orenames2) do
		TCFT2.Mining.MiningMap.MineralList[i] = orename
		i = i + 1
	end
	TCFT2.Mining.MiningMap.MineralList.value = 1
	TCFT2.Mining.MiningMap.UpdateMap()
end

-- Mouse over mining map
TCFT2.Mining.MiningMap.Map_Mouseover = function(index, str)
	local msg = ""
	local num = tonumber(index)
	string.gsub(str, "(.-)|", function(a) msg = msg .. a .. "\n" end)
	if (TCFT2.Mining.MiningMap.SectorMinerals[num]~=nil) then
		msg = msg .. "\nMinerals:\n"
		for name, ore in pairs(TCFT2.Mining.MiningMap.SectorMinerals[num]) do
			if (name~="TCFT2AVERAGE") then
				local numoreroids = TCFT2.Mining.RoidCountPrOre[num][name] or "?"
				msg = msg .. name .. " Ore" .. TCFT2.colors.GREEN2 .. "  (" .. numoreroids .. " roids - " .. string.format("%0.1f", ore.min) .. "-" .. string.format("%0.1f", ore.max) .."%)" .. TCFT2.colors.NORMAL .. "\n"
			end
		end
		msg = msg .. "\nThere are " .. (TCFT2.Mining.RoidCount[num] or "?") .. " roids in the sector.\n\n" .. GetBotSightedInfoForSector(num)
	end
	TCFT2.Mining.MiningMap.Info.value = msg
end

-- *********
-- AutoEject
-- *********
TCFT2.Mining.AutoEjectOn = function()
	TCFT2.Settings.Data.AutoEjectActive = "ON"
	TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "AutoEject turned on." .. TCFT2.colors.NORMAL, true)
end

TCFT2.Mining.AutoEjectOff = function()
	TCFT2.Settings.Data.AutoEjectActive = "OFF"
	TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "AutoEject turned off." .. TCFT2.colors.NORMAL, true)
end

TCFT2.Mining.AutoEjectToggle = function()
	if (TCFT2.Settings.Data.AutoEjectActive == "ON") then
		TCFT2.Mining.AutoEjectOff()
	else
		TCFT2.Mining.AutoEjectOn()
	end
end

-- ****************
-- **** EVENTS ****
-- ****************

-- Process mining stuff when entering station
TCFT2.Mining.EventEnteredStation = function(_, params)
	if (TCFT2.Mining.Scanner.Running) then
		TCFT2.Mining.Scanner.Stop()
		TCFT2.Mining.Scanner.StationTurnedOff = true
	end
	TCFT2.Mining.CurrentSectorId = GetCurrentSectorid()
	TCFT2.Mining.SubmitSector()
end

-- Do mining stuff when leaving station
TCFT2.Mining.EventLeaveStation = function(_, params)
	TCFT2.Mining.CurrentSectorId = GetCurrentSectorid() -- Just update current sector id
	if (TCFT2.Mining.Scanner.StationTurnedOff) then
		TCFT2.Mining.Scanner.Start()
	end
end

-- Target is scanned
TCFT2.Mining.EventTargetScanned = function(_, info, nodeid, objectid)
	if (TCFT2.CargoList.Active) then return end -- We are searching for cargo - doens mind the roid info
	nodeid = nodeid or 0
--	if (nodeid==2) then -- We hit a roid -- NOT true.  Lisa.
	local name, health, dist, factionid, guild, ship = GetTargetInfo() -- lisa added so we can get name
	if (string.match(name, "Asteroid") or string.match(name, "Ice Crystal")) then -- we hit a roid, lisa
		info = (info or "")
		if ((objectid or -1)>-1) and ((TCFT2.Mining.Scanner.ScannedRoids[objectid] or 0)~=1) then
			if (not string.find(info, "Object too far to scan")) or (string.find(string.lower(info), "ore")) then -- Out of range or no info - check if we have data for this one
				if (string.find(string.lower(info), "ore")) then
					local ores = {}
					local sectorid = GetCurrentSectorid()
					local temp = -1
					string.gsub(info, "(.-)\n", function(xinfo, percent) 
						if (not string.find(xinfo, "Temperature:")) then 
							string.gsub(xinfo, "(.-)%sOre: (.-)%%", function(name, percent) 
								table.insert(ores, { orename=name, density=percent }) 
							end)
						end
					end)
					local roid = { temperature=temp, sectorid=sectorid, objectid=objectid, ores=ores }
					TCFT2.Mining.RoidInfo[objectid] = roid
					TCFT2.Mining.Scanner.ScannedRoids[objectid] = 1
					TCFT2.Mining.EventTargetChanged()
					HUD:PrintSecondaryMsg(TCFT2.colors.GREEN2 .. "(TCFT2)" .. TCFT2.colors.NORMAL .. " Ore saved.")
				else
					local sectorid = GetCurrentSectorid()
					string.gsub(info, "Temperature:(.-) K", function(temp)
						TCFT2.Mining.RoidInfo[objectid] = TCFT2.Mining.RoidInfo[objectid] or { temperature=-1, sectorid=sectorid, objectid=objectid, ores={ { orename="Unknown", density=0 } } }
						TCFT2.Mining.RoidInfo[objectid].temperature=tonumber(temp)
						TCFT2.Mining.EventTargetChanged()
					end)
				end
			else
				TCFT2.Mining.EventTargetChanged()
			end
		end
	end
end

-- Target Changed
TCFT2.Mining.EventTargetChanged = function(_, params)
	if (TCFT2.Mining.Scanner.Running) or (TCFT2.CargoList.Active) or (targetless.api.radarlock==true) then -- If we are scanning we skip this update to save cpu - its also done in target_scanned event handler
		return
	end
	local nodeid, objectid = radar.GetRadarSelectionID()
-- lisa removed one level of IF's
--	if (nodeid~=nil) and (nodeid==2) then --lisa won't work. Can't identify roids via nodeid. per raybondo.
		if (objectid) then
			local name, health, dist, factionid, guild, ship = GetTargetInfo()
			local txt = ""
			if (string.find(name, "Asteroid")) or (string.find(name, "Ice Crystal")) then --lisa now only way to identify roids
				txt = TCFT2.colors.YELLOW .. name .." #" .. objectid .. TCFT2.colors.NORMAL
				local roidInfo = TCFT2.Mining.RoidInfo[objectid]
				if (roidInfo~=nil) then
					--FadeStop(HUD.scaninfo)
					table.sort(roidInfo.ores, function(a,b) return (tonumber(a.density) or 0)>(tonumber(b.density) or 0) end)
					for idx, ore in ipairs(roidInfo.ores) do
						txt = txt .. " | " .. ore.orename .. ": " .. ore.density .. "%"
					end
					if ((roidInfo.temperature or -1)>-1) then
						txt = txt .. " | " .. TCFT2.colors.GREY2 .. "Temp: " .. string.format("%0.1f", roidInfo.temperature) .. " K"
					end
				else
					txt = txt .. " | " .. TCFT2.colors.RED .. "No data"
				end
				TCFT2.Mining.SetInfo(txt)
			else
				TCFT2.Mining.SetInfo("")
			end
		else
			TCFT2.Mining.SetInfo("")
		end
	-- return iup.CONTINUE
end

-- Entering a new sector
TCFT2.Mining.EventSectorChanged = function(_, params)
	-- Update variables used later in this event
	TCFT2.Mining.SubmitSector()
	TCFT2.Mining.CurrentSectorId = GetCurrentSectorid()
	if (TCFT2.Mining.CurrentSystem ~= GetCurrentSystemid()) then
		TCFT2.Mining.MiningMap.LoadSystemButton.active="YES"
	end
	TCFT2.Mining.CurrentSystem = GetCurrentSystemid()

	if (TCFT2.Settings.Data.autoloadsectordata=="ON") then -- Auto load sector data
		TCFT2.Mining.LoadSector()
	end
	TCFT2.Mining.Scanner.ScannedRoids = {}
end

TCFT2.Mining.EventInventoryChange = function(event, params, p2, p3, p4, p5) -- Inventory change - check for auto jettison
	local CargoMax = GetActiveShipMaxCargo() or 0
	local CargoCurrent = GetActiveShipCargoCount() or 0
	if (TCFT2.Settings.Data.AutoEjectActive == "ON") then
		if (CargoCurrent>0) and (CargoMax-CargoCurrent<=16) then
			local CurrentInventory = GetShipInventory(GetActiveShipID())
			for idx,itemid in ipairs(CurrentInventory.cargo) do
				local orename = GetInventoryItemName(itemid)
				local ore = string.match(orename, "^Premium%s(.-%sOre)$") or string.match(orename, "^(.-%sOre)$")
				if (ore) and (TCFT2.Settings.Data.AutoEjectKeep[ore]~="ON") then -- We found ore and dont have setting to keep this one
					if (TCFT2.Settings.Data.SoundCargoAutoeject~= "None") then
						gksound.GKPlaySound(TCFT2.Sounds[TCFT2.Settings.Data.SoundCargoAutoeject], 1.0)
					end
					JettisonSingle(itemid, 200)
				end
			end
		end
	end	
	if (CargoMax>0) and (CargoCurrent==CargoMax) then
		if (TCFT2.Settings.Data.SoundCargoholdFull~= "None") then
			gksound.GKPlaySound(TCFT2.Sounds[TCFT2.Settings.Data.SoundCargoholdFull], 1.0)
		end
	end
end


TCFT2.Mining.AddInfoField = function()
	-- TCFT2.Mining.Info = iup.label{ title="", expand="NO", alignment="ARIGHT", font=Font.H4*HUD_SCALE }
	--[[
	TCFT2.Mining.InfoBox = iup.vbox {
		visible="YES",
		iup.fill {},
		iup.hbox {
			iup.fill {},
			iup.hudrightframe {
				iup.vbox {
					expand="NO",
					iup.hudrightframe {
						border="0 0 0 0",
						iup.hbox {
						bgcolor="255 0 0 192",
							expand="HORIZONTAL",
							iup.fill{},
							iup.label { title=TCFT2.colors.GREEN2 .. "* TCFT2 *", alignment="ACENTER", expand="NO", font=Font.H4*HUD_SCALE*1.05 },
							iup.fill{},
						},
					},
					iup.fill { size=3 },
					iup.hudrightframe {
						border="0 0 0 0",
						iup.vbox {
							TCFT2.Mining.Info,
							expand="YES",
							margin="7x7",
						},
						-- size=HUDSize(.12).."x",
						expand="YES",
						-- size=HUDSize(.15),
					},
				},
			},
			iup.fill { size=HUDSize(.25) },
		},
		iup.fill {},
	}
	]]


	TCFT2.Mining.Info = iup.label{ title="TEST", expand="NO", alignment="ALEFT", font=Font.H4*HUD_SCALE }

	TCFT2.Mining.InfoBox = iup.vbox {
		visible="NO",
		iup.fill {},
		iup.hbox {
			iup.fill {},
			iup.hudrightframe {
				border="2 2 2 2",
				iup.vbox {
					expand="NO",
					iup.hudrightframe {
						border="0 0 0 0",
						iup.hbox {
							iup.fill {},
							iup.label { title=TCFT2.colors.GREEN2 .. " * TCFT2 * ", alignment="ACENTER", expand="NO", font=Font.H4*HUD_SCALE },
							iup.fill {},
							margin="2x2",
						},
					},
					iup.fill { size=3 },
					iup.hudrightframe {
						border="0 0 0 0",
						iup.hbox {
							TCFT2.Mining.Info,
							margin="10x3",
						},
					},
				},
			},
			iup.fill {},
		},
		iup.fill { size=3 },
	}
	
	iup.Append(HUD.pluginlayer, TCFT2.Mining.InfoBox)
	
	--[[
	local scaninfoprnt = iup.GetParent(HUD.scaninfo)
	if (scaninfoprnt~=nil) then
		iup.Detach(HUD.scaninfo)
		local infobox = iup.hbox {
			alignment="ARIGHT",
			HUD.scaninfo, iup.fill { size="20" },
			TCFT2.Mining.Info,
			expand="YES",
		}
		-- HUD.scaninfo.expand="NO"
		iup.Append(scaninfoprnt, infobox)
	end
	]]
end

-- Set info below scaninfo in HUD
TCFT2.Mining.SetInfo = function(infotxt)
	if (TCFT2.Mining.Info~=nil) then
		TCFT2.Mining.Info.title = infotxt
		if (TCFT2.Mining.InfoBox~=nil) then
			TCFT2.Mining.InfoBox.visible= (infotxt~="" and "YES") or "NO"
		end
	end
end


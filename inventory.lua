--[[
Inventory functions
]]

TCFT2.Inventory = TCFT2.Inventory or {}

dofile("ui/ui_inventory.lua")

TCFT2.Inventory.RefreshList = function()
	local items = {}
	local filter = TCFT2.Trading.LookupItemType[TCFT2.Trading.LookupItemType.value] or "All Items"
	if (filter=="All Items") then 
		filter = ""
	end
	local totalitems = 0
	for itemid,v in PlayerInventoryPairs() do
		local itemname = GetInventoryItemName(itemid)
		if (filter~="" and itemname==filter) or (filter=="") then 		
			local stationid = GetInventoryItemLocation(itemid)
			local sectorid = SplitStationID(stationid)
			-- local iconpath, itemname, quantity = GetInventoryItemInfo(itemid)
			local quantity = GetInventoryItemQuantity(itemid)
			local key = stationid .. "_" .. itemname
			local location = ShortLocationStr(sectorid)
			local station = GetStationName(stationid)
			if (location=="0 @-0") then
				location = "-- Active Ship --"
				station = ""
			end
			items[key] = items[key] or { station=station, location=location, itemname=itemname, quantity=0 }
			items[key].quantity = items[key].quantity + tonumber(quantity)
			totalitems = totalitems + quantity
		end
	end
	TCFT2.Inventory.LookupList:clear()
	for key, item in pairs(items) do
		TCFT2.Inventory.LookupList:additem(item)	
	end
	TCFT2.Inventory.TotalItems.title = totalitems
	TCFT2.Inventory.LookupList:update()
end

TCFT2.Inventory.UpdateItemlist = function()
	local names = {}
	for itemid,v in PlayerInventoryPairs() do
		local name = GetInventoryItemName(itemid)
		names[name] = name
	end
	local sortednames = {}
	for _,name in pairs(names) do
		table.insert(sortednames, name)
	end
	table.sort(sortednames)
	TCFT2.Trading.LookupItemType[1] = nil
	TCFT2.Trading.LookupItemType[1] = "All Items"
	local i = 2
	for i, name in ipairs(sortednames) do
		TCFT2.Trading.LookupItemType[i+1] = name
	end
end

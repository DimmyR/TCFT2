--[[
TCFT2
(c) By Plozi
Version 1.0.0

Based on the original TCFT made by Moda

** Changelog

]]

TCFT2.Connection = {}

-- Socket callback handlers
-- Connection
TCFT2.Connection.connect = function(sock,errmsg)
	if (sock) then
		TCFT2.Connection.PeerName = sock.tcp:GetPeerName()
		if (TCFT2.Connection.PeerName ~= nil) then
			TCFT2.Connection.isConnected = true
			TCFT2.Connection.isLoggedin = false
			TCFT2.Connection.Socket = sock.tcp
			TCFT2.Connection.Send = sock.Send
			TCFT2.SendData({ cmd="100", username=TCFT2.Settings.Data.username, password=TCFT2.Settings.Data.password, idstring=TCFT2.IdString })
		else 
			TCFT2.Connection.isLoggedin = false
			TCFT2.Connection.isConnected = false
			TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Connection to server failed.", true)
			if (errmsg) then
				TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Error: " .. errmsg)
			else
				TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Unknown error.")
			end
		end
	else
		TCFT2.Connection.isLoggedin = false
		TCFT2.Connection.isConnected = false
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Connection to server failed.", true)
		if (errmsg) then
			TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Error: " .. errmsg)
		else
			TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Unknown error.")
		end
	end
end

-- Server close the connection... or lines are down
TCFT2.Connection.disconnect = function()
	TCFT2.Connection.isConnected = false
	TCFT2.Connection.isLoggedin = false
	TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED.."Connection to server lost.", true)
	TCFT2.SetStatusLine(TCFT2.colors.RED .. "Not connected to server")
	TCFT2.connectButton.title = "Connect"
end

-- Receive data

TCFT2.Connection.getline = function(sock, line)
	line = string.gsub(line,"[\r\n]","")
	if (TCFT2.debug==1) then TCFT2.AddStatus(TCFT2.colors.YELLOW..'<<< ' .. line) end
	local data = json.decode(line)
	local cmd = tonumber(data.cmd)
	
	if (cmd>0) then
		local evtparams = data
		if (TCFT2.Events[cmd]) then
			if (TCFT2.debug==1) then TCFT2.AddStatus(TCFT2.colors.YELLOW .. "Received Event from server: " .. cmd) end
			TCFT2.Events[cmd](evtparams)
		else
			TCFT2.AddStatus(TCFT2.colors.RED .. 'Unknown Event from server: ' .. cmd)
		end
	else
		if Line ~= '' then
			local length = string.len(Line);
			TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED..'Invalid Event from server, '..length..' characters: >'..Line..'<')
		end
	end
end

-- Check connection - try connect if disconnected or disconnect if connected
TCFT2.Connection.CheckConnection = function()
	if (TCFT2.Connection.isConnected) then
		SetShipPurchaseColor(229)
		TCFT2.SendData({ cmd="101" })
		TCFT2.Connection.Socket:Disconnect()
		TCFT2.Connection.Socket = nil
		TCFT2.Connection.isConnected = false
		TCFT2.Connection.isLoggedin = false
		TCFT2.connectButton.title = "Connect"
		TCFT2.SetStatusLine(TCFT2.colors.RED .. "Not Connected to the server.")
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Connection to server closed.", true)
	else
		TCFT2.Connection.Socket = TCP.make_client(TCFT2.Server,27056,TCFT2.Connection.connect,TCFT2.Connection.getline,TCFT2.Connection.disconnect)
	end
end

-- Clean up the connection
TCFT2.Connection.CleanUp = function()
	if (TCFT2.Connection.isConnected) then
		TCFT2.SendData({ cmd="101" })
		TCFT2.Connection.Socket:Disconnect()
		TCFT2.Connection.isConnected = false
		TCFT2.Connection.isLoggedin = false
	end
end


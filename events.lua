--[[
TCFT2
Event handlers
]]

TCFT2.Events = TCFT2.Events or {}

-- Nop - dummy command from server
TCFT2.Events[9998] = function()
	-- NOP - user not logged in
end

-- Login OK - update everything
TCFT2.Events[500] = function(params)
	TCFT2.Connection.isConnected = true
	TCFT2.Connection.isLoggedin = true
	-- Check if client version is current
	TCFT2.SendData({ cmd="1000", version=TCFT2.Version })
	TCFT2.connectButton.title = "Disconnect"
	TCFT2.SetStatusLine(TCFT2.colors.GREEN2 .. "Connected to the server.")
	TCFT2.PloziNav.Initialize() -- Initialize navigation computer
	TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Logged in to server OK.", true)
	TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) *** Currently logged on users: " .. params.users, true)
	TCFT2.UserOptions = params.useroptions
	TCFT2.SendData({ cmd="1500" })
	if (tonumber(TCFT2.UserOptions.memberedit)==1) then -- We can edit members
		TCFT2.Guild.MemberEditButtons.visible = "YES"
	end
	if (tonumber(TCFT2.UserOptions.navedit)==1) then -- We can edit navroutes
		TCFT2.PloziNav.UI.SendToServerBtn.active = "YES"
	end
	
	-- Activate buttons that might have been disabled
	if (TCFT2.Settings.Data.autoloadsectordata=="ON") then -- Auto load sector data
		TCFT2.Mining.LoadSector()
	end
	-- Display guild members
	if (TCFT2.Settings.Data.displayguildlog=="ON") then
		TCFT2.GuildOnlineTimer:SetTimeout(5000, TCFT2.ShowOnlineGuildMembers)
	end
end

-- Invalid login
TCFT2.Events[501] = function(params)
	TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Login to server failed.", true)
	TCFT2.Connection.Socket:Disconnect()
	TCFT2.Connection.Socket = nil
	TCFT2.Connection.isConnected = false
	TCFT2.Connection.isLoggedin = false
	TCFT2.connectButton.title = " Connect "
	TCFT2.SetStatusLine(TCFT2.colors.RED .. "Not Connected to the server.")
	TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Connection to server closed.", true)
end

-- Notify that other user logged in
TCFT2.Events[502] = function(params)
local user = params.username
TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. string.format("%s logged in.", params.username) .. TCFT2.colors.GREEN2, true)
end
-- 
-- -- Notify that other user logged out
TCFT2.Events[503] = function(params)
local user = params.username or "This shouldnt occour... "
TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. string.format("%s logged out.", params.username) .. TCFT2.colors.GREEN2, true)
end

-- Not compatible versions
TCFT2.Events[505] = function(params)
	--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Your clientversion are not compatible with the current server version.", true)
	--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "To be able to continue using TCFT2 you have to upgrade to latest version.", true)
	--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "You can download at http://vo.aoczone.net/download", true)
end

-- Reveiving list of logged in users
TCFT2.Events[550] = function(params)
	TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. "*** Currently logged on users: " .. params.users .. TCFT2.colors.NORMAL)
end

-- TCFT chat
TCFT2.Events[600] = function(params)
	local msg = params.msg
	if (string.len(msg)>0) then
		local sectorid = tonumber(params.sectorid) or -1
		local locstr = " "
		if (sectorid>-1) then
			locstr = string.format(" [%s]", ShortLocationStr(sectorid))
		end
		local name = params.name or "Unknown"
		local isemote = params.isemote or 0
		local fullmsg
		if (isemote==1) then
			fullmsg = string.format(TCFT2.colors.GREEN2 .. "(TCFT2)%s **%s%s%s %s", locstr, TCFT2.colors.NORMAL, name, TCFT2.colors.GREEN2, msg)
		else
			fullmsg = string.format(TCFT2.colors.GREEN2 .. "(TCFT2)%s <%s%s%s> %s", locstr, TCFT2.colors.NORMAL, name, TCFT2.colors.GREEN2, msg)
		end
		TCFT2.AddStatus(fullmsg)
		if (TCFT2.Settings.Data.tcftchattomainchat=="ON") then
			print(fullmsg)
		end
	end
end

-- Send some lines from tradebuffer
TCFT2.Events[700] = function(params)
	local buffer = {}
	for i=1,25 do
		local item = table.remove(TCFT2.Trading.TradeBuffer)
		if (item==nil) then break end
		table.insert(buffer, item)
	end
	if (#buffer>0) then
		TCFT2.SendData({ cmd="301", items=buffer })
	else
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Station submitted.", true)
	end
end

-- Update tradeitemnames in trade tab
TCFT2.Events[710] = function(params)
	TCFT2.Trading.Itemlist = params.items
	TCFT2.Trading.UpdateLookupItems()
	TCFT2.Trading.LastUpdate = os.time()
end

local function tableSort(a, b)
	return a.name<b.name
end

-- Update stationnames
TCFT2.Events[730] = function(params)
	local num = tonumber(params.numitems)
	if (num>0) then
		TCFT2.Stations = {}
		for _, station in ipairs(params.stations) do
			local stationid = station.id
			local sectorid = SplitStationID(stationid)
			local sectname = ShortLocationStr(sectorid)
			table.insert(TCFT2.Stations, { id=stationid, name=sectname .. " - " .. GetStationName(stationid) })
		end
	end
	for id, name in pairs(SystemNames) do
		local num = tonumber(id) or -1
		if (num>-1) then
			table.insert(TCFT2.Stations, { id=-num, name=name .. " (System)" })
		end
	end
	table.sort(TCFT2.Stations, tableSort)
	TCFT2.Trading.StationCompareUpdateStations()
	TCFT2.Trading.TradeRouteSearchUpdateStations()
end

-- Item lookup response
TCFT2.Events[750] = function(params)
	TCFT2.Trading.Lookup.Result = {}
	local num = tonumber(params.numitems)
	if (num>0) then
		for _, item in ipairs(params.items) do
			table.insert(TCFT2.Trading.Lookup.Result, item)
		end
	end
	TCFT2.Trading.Lookup.UpdateLookupTable()
end

-- Compare stations response
TCFT2.Events[760] = function(params)
	if (TCFT2.Trading.StationCompareCancelled==false) then
		ProcessEvent("TCFT2_UPDATEPROGRESSBAR", params.progress)
		local num = tonumber(params.numitems)
		if (num>0) then
			for _, item in ipairs(params.items) do
				item.type = TCFT2.itemtypesrev[tonumber(item.type)] or "???"
				table.insert(TCFT2.Trading.StationCompareResult, item)
			end
			TCFT2.SendData({ cmd="471" })
		else
			ProcessEvent("TCFT2_PROGRESSBARDONE")
			TCFT2.Trading.UpdateStationCompareTable()
		end
	else
		TCFT2.SendData({ cmd="472" }) -- Tell server we dont need more data
		TCFT2.Trading.UpdateStationCompareTable()
	end
end

-- Trade Route Search Result
TCFT2.Events[770] = function(params)
	if (TCFT2.Trading.TradeRouteSearchCancelled==false) then
		ProcessEvent("TCFT2_UPDATEPROGRESSBAR", params.progress)
		local num = tonumber(params.numitems)
		if (num>0) then
			for _, item in ipairs(params.items) do
				-- item.type = TCFT2.itemtypesrev[tonumber(item.type)] or "???"
				table.insert(TCFT2.Trading.TradeRouteSearchResult, item)
			end
			TCFT2.SendData({ cmd="481" })
		else
			ProcessEvent("TCFT2_PROGRESSBARDONE")
			TCFT2.Trading.UpdateTradeRouteList()
		end
	else
		TCFT2.SendData({ cmd="482" })
		TCFT2.Trading.UpdateTradeRouteList()
	end
end

-- Submit some sector data
TCFT2.Events[800] = function(params)
	local buffer = {}
	for i=1,10 do
		local item = table.remove(TCFT2.Mining.RoidBuffer)
		if (item==nil) then break end
		table.insert(buffer, item)
	end
	if (#buffer>0) then
		TCFT2.SendData({ cmd="501", items=buffer })
	else
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Sector submitted.", true)
		-- Load system AFTER submitted current one
		if (TCFT2.Settings.Data.autoloadsectordata=="ON") then -- Auto load sector data
			--TCFT2.Mining.LoadSystem()
		end
	end
end

-- Reveiving orelist - update 
TCFT2.Events[810] = function(params)
	TCFT2.Mining.OreList = {}
	local num = tonumber(params.numitems)
	if (num>0) then
		for _, orename in ipairs(params.ores) do
			table.insert(TCFT2.Mining.OreList, orename)
		end
	end
	TCFT2.Mining.MineralSearch.LastOreUpdate = os.time()
	TCFT2.Mining.MineralSearch.UpdateOrelist()
end

-- Receving ore-search result - also called mineral search
TCFT2.Events[850] = function(params)
	if (TCFT2.Mining.MineralSearch.Cancelled==false) then	
		ProcessEvent("TCFT2_UPDATEPROGRESSBAR", params.progress)
		local num = tonumber(params.numitems)
		if (num>0) then
			for _, ore in ipairs(params.ores) do
				table.insert(TCFT2.Mining.MineralSearch.Results, ore)
			end
			TCFT2.SendData({ cmd="601" });
		else
			ProcessEvent("TCFT2_PROGRESSBARDONE")
			TCFT2.Mining.MineralSearch.UpdateMineralSearchTable()
		end
	else
		TCFT2.SendData({ cmd="602" })
		TCFT2.Mining.MineralSearch.Results = {}
		TCFT2.Mining.MineralSearch.UpdateMineralSearchTable()
	end
end
	
-- Receiving part of sector data (this can be called repeatly until server buffer is empty)
TCFT2.Events[900] = function(params)
	if (params.numitems>0) then
		local sectorid = GetCurrentSectorid()
		for _, obj in ipairs(params.objects) do
			if (tonumber(obj.sectorid)==sectorid) then
				-- table.insert(TCFT2.Mining.RoidInfo, obj)
				TCFT2.Mining.RoidInfo[tonumber(obj.objectid)] = obj
			end
		end
		TCFT2.SendData({ cmd="701" })
	--else
		--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Sector Loaded.", true)
	end
end

-- Receiving some system information (repeatly called as server sends chunks)
TCFT2.Events[910] = function(params)
	if (TCFT2.Mining.LoadSystemCancelled==false) then -- Did the user cancel this operation?
		ProcessEvent("TCFT2_UPDATEPROGRESSBAR", params.progress)
		if (params.numitems>0) then
			local systemid = GetCurrentSystemid() - 1
 			local startsec = systemid*256+1
			local endsec = systemid*256+257
			for _, obj in ipairs(params.objects) do
				table.insert(TCFT2.Mining.RoidInfoSystem, obj)
			end
			TCFT2.SendData({ cmd="711" })
		else
			ProcessEvent("TCFT2_PROGRESSBARDONE")
			TCFT2.Mining.LoadedSystem = TCFT2.Mining.CurrentSystem
			TCFT2.Mining.MiningMap.UpdateMapInfo()
		end
	else
		TCFT2.SendData({ cmd="712" })
		TCFT2.Mining.RoidInfoSystem =  {}
	end
end

-- Receive list of stations that needs update
TCFT2.Events[950] = function(params)
	if (TCFT2.Trading.StationUpdatesCancelled==false) then -- Did the user cancel this operation?
		ProcessEvent("TCFT2_UPDATEPROGRESSBAR", params.progress)
		if (params.numitems>0) then
			for _, item in ipairs(params.items) do
				item.location = ShortLocationStr(item.sectorid) or "?"
				item.stationname = GetStationName(item.stationid) or "?"
				if (tonumber(item.mark)==1) then
					item.fgcolor = "255 255 255"
				elseif (tonumber(item.mark)==2) then
					item.fgcolor = "240 0 0"
				else
					item.fgcolor = "66 220 66"
				end
				TCFT2.Trading.StationUpdateList:additem(item)
			end
			TCFT2.SendData({ cmd="351" })
		else
			ProcessEvent("TCFT2_PROGRESSBARDONE")
			TCFT2.Trading.StationUpdateList:update()
		end
	else
		TCFT2.SendData({ cmd="352" })
	end
end

-- Receive some orelist data
TCFT2.Events[970] = function(params)
	if (TCFT2.Mining.LoadOrelistCancelled==false) then
		ProcessEvent("TCFT2_UPDATEPROGRESSBAR", params.progress)
		if (params.numitems>0) then
			for _, object in ipairs(params.objects) do
				object.system = SystemNames[object.systemid] or "?"
				TCFT2.Mining.OrelistPrSystem.OreList:additem(object)
				-- printtable(object)
			end
			TCFT2.SendData({ cmd="731" })
		else
			ProcessEvent("TCFT2_PROGRESSBARDONE")
			TCFT2.Mining.OrelistPrSystem.OreList:update()
		end	
	else
		TCFT2.SendData({ cmd="732" })
	end
end

-- Version - check if client version are current
TCFT2.Events[1000] = function(params)
	TCFT2.SendData({ cmd='400'}) -- Update available trade items
	TCFT2.ServerVersion = params.version
	if (params.version ~= TCFT2.Version) then
		--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.YELLOW .. "*** There is a new version of the client available (version " .. params.version .. "). Check changelog and download at http://vo.aoczone.net", true)
	else
		--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "*** Client version is current.")
	end
	TCFT2.SendData({ cmd='450'}) -- Update station list
	TCFT2.SendData({ cmd='610'}) -- Update station list
end

-- Message about stations that need attention
TCFT2.Events[1100] = function(params)
	--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "*** There are stations where trade data are out of date. Check \"Stations\" under Trade Tab in TCFT2.", true)
end

-- Receive navigation data
TCFT2.Events[1200] = function(params)
	TCFT2.PloziNav.Routes = unspickle(params.navdata)
	TCFT2.PloziNav.Editor.LoadData()
	TCFT2.PloziNav.Editor.LoadSystemRoutes()
	local broadcast = true
	if (TCFT2.PloziNav.UI.EditDialog.visible=="YES") then
		broadcast = false
		--TCFT2.MessageDialog("Navigation Computer", "Data successfully fetched from the server.")
	end
	--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Navigation data successfully fetched from server.", broadcast)
end

-- Data saved ok
TCFT2.Events[1211] = function(params)
	TCFT2.MessageDialog("Navigation Editor", "Data successfully saved to server.")
end

-- Error saving data
TCFT2.Events[1212] = function(params)
	TCFT2.ErrorDialog("Navigation Editor", "There was an error saveing data to the server.")
end

-- No access for saving data
TCFT2.Events[1213] = function(params)
	TCFT2.ErrorDialog("Navigation Editor", "You dont have access to save data to the server.")
end

-- Response from last-seen
TCFT2.Events[1260] = function(params)
	console_print("1260 " .. params.numitems)
	TCFT2.LastSeen.List:clear()
	if (tonumber(params.numitems)>0) then
		for _, player in ipairs(params.seenlist) do
			local guildtag = ""
			if (player.guildtag~=nil) and (player.guildtag~="") then
				guildtag = "[" .. player.guildtag .. "]"
			end
			local rec = { name=player.name, date=os.date("%Y-%m-%d %H:%M", player.ts), location=ShortLocationStr(player.sectorid), guildtag=guildtag, shipname=player.shipname, health=player.health }
			TCFT2.LastSeen.List:additem(rec)
		end
		TCFT2.LastSeen.List:update()
	else
		TCFT2.LastSeen.List.NUMLIN=2
		TCFT2.LastSeen.List["2:1"] = "Nothing found."
	end
end

-- Live feed for lastseen
TCFT2.Events[1265] = function(params)
	-- HUD:PrintSecondaryMissionMsg(msg)
	-- HUD:PrintSecondaryMsg(msg)
	local players = params.players or {}
	local msgstr = ""
	for idx, player in ipairs(players) do
		if (player.name~=GetPlayerName()) and (tonumber(player.sectorid)~=GetCurrentSectorid()) then -- NOT Player itself or same sector
			local guild = "" 
			if ((player.guildtag or "")~="") then
				guild = "[" .. player.guildtag .. "] "
			end
			local factioncolor = ""
			if ((tonumber(player.faction) or 0) > 0) then
				factioncolor = rgbtohex(FactionColor_RGB[tonumber(player.faction)])
			else
				factioncolor = TCFT2.colors.RED
			end
			local location = "Unknown Location"
			local sectid = player.sectorid or -1
			if (sectid>0) then
				location = ShortLocationStr(sectid)
			end
			local shipname = (player.shipname) or ""
			if (shipname~="") then shipname = " (Flying " .. shipname .. ")" end
			local txt = factioncolor .. guild .. player.name .. TCFT2.colors.RED .. " spotted in " .. location .. shipname .. TCFT2.colors.NORMAL
			-- Check if this notification already received - if so then dont add again
			local exists = false
			for _, noti in ipairs(TCFT2.LastSeen.Notifications) do
				if (noti.txt==txt) then
					exists=true
					break
				end
			end		
			if (exists==false) then
				table.insert(TCFT2.LastSeen.Notifications, { time=os.time(), txt=txt, label=nil })
			end
		end -- Player itself or same sector
	end
end

local function updateLastSeenBars(res, barnum)
	local max=0
	for fac, num in pairs(res) do
		num = tonumber(num)
		fac = tonumber(fac)
		if (fac>0) and (fac<4) then
			TCFT2.Players.LastSeenBars[barnum][fac]:setvalue(num)
			TCFT2.Players.LastSeenBars[barnum][fac]:settitle(FactionName[fac] .. " " .. num)
			if (num>max) then max = num end
		end
	end
	for fac, num in pairs(res) do
		fac = tonumber(fac)
		if (fac>0) and (fac<4) then
			TCFT2.Players.LastSeenBars[barnum][fac]:setminmax(0, max)
		end
	end
end

-- Last Seen statistics
TCFT2.Events[1300] = function(params)
	updateLastSeenBars(params.spots.overall, 5)
	updateLastSeenBars(params.spots.itani, 1)
	updateLastSeenBars(params.spots.serco, 2)
	updateLastSeenBars(params.spots.uit, 3)
	updateLastSeenBars(params.spots.grey, 4)
end


-- Receive list of guild members
TCFT2.Events[1500] = function(params)
	TCFT2.Guild.MemberList:clear()
	TCFT2.GuildMembers = {}
	local num = tonumber(params.numitems) or 0
	if (num>0) then
		local members = params.members or {}
		if (#members>0) then
			for _, member in ipairs(members) do
				local trank = tonumber(member.rank)
				member.rank = TCFT2.Guild.MemberRanks[tonumber(member.rank)]
				-- 128 0 128
				-- 153 51 102
				-- 255 255 0
				if (trank==0) then -- Probie
					member.fgcolor = "55 185 53"
				elseif (trank==1) then -- Member
					member.fgcolor = "220 145 23"
				elseif (trank==2) then -- Lieutenant
					member.fgcolor = "180 180 180"
				elseif (trank==3) then -- Council/Lt.
					member.fgcolor = "180 180 180"
				elseif (trank==4) then -- Council
					member.fgcolor = "255 185 50"
				elseif (trank==5) then -- Commander
					member.fgcolor = "255 255 255"
				elseif (trank==6) then -- Applicant
					member.fgcolor = "255 20 20"
				else -- Unknown
					member.fgcolor = "15 15 15"
				end
				TCFT2.Guild.MemberList:additem(member)
				TCFT2.GuildMembers[string.lower(member.name)] = tonumber(member.rank) or 0
			end
			TCFT2.Guild.MemberList:update()
		else
			TCFT2.Guild.MemberList.NUMLIN=2
			TCFT2.Guild.MemberList:setcell(2, 2, "No members found.")
		end
	else
		TCFT2.Guild.MemberList.NUMLIN=2
		TCFT2.Guild.MemberList:setcell(2, 2, "No members found.")
	end
end



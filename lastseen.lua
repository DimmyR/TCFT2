--[[
TCFT2
(c) By Plozi
Version 1.0.0

** Changelog

]]

TCFT2.LastSeen = TCFT2.LastSeen or {}
TCFT2.LastSeen.Timer = Timer()
TCFT2.LastSeen.NotifyTimer = Timer()
TCFT2.LastSeen.Timeout = 10 -- 15 seconds
TCFT2.LastSeen.Notifications = {}
TCFT2.LastSeen.NotifyTimeout = 15 -- 8 seconds

dofile("ui/ui_lastseen.lua")

TCFT2.LastSeen.PlayerListCheck = {} -- List of unknown players to check for

TCFT2.DisplayLastSeenList = function()
	if (TCFT2.LastSeen.MainWindow==nil) then
		TCFT2.LastSeen.CreateMainWindow()
	end
	PopupDialog(TCFT2.LastSeen.MainWindow,iup.CENTER, iup.CENTER)
	-- TCFT2.LastSeen.Update()
end

TCFT2.LastSeen.Update = function()
	TCFT2.LastSeen.List:clear()
	TCFT2.LastSeen.List.NUMLIN=2
	if (TCFT2.Connection.isLoggedin~=true) then
		TCFT2.LastSeen.List["2:2"] = "Not logged in..."
	else
		TCFT2.LastSeen.List["2:2"] = "Updating, please wait..."
		local name=""
		local guildtag=""
		local sectorid=GetCurrentSectorid() 
		local typeinput = tonumber(TCFT2.LastSeen.Type.value)
		if (typeinput==1) then -- Current sector
			-- Nothing to do actually
			sectorid = GetCurrentSectorid()
		elseif (typeinput==2) then -- Player name
			name=TCFT2.LastSeen.InputData.value
			if (name=="") then
				TCFT2.MessageDialog("LastSeen", "No Player name given. Try again.")
				return
			end
		elseif (typeinput==3) then -- Guild
			guildtag=TCFT2.LastSeen.InputData.value
			if (guildtag=="") then
				TCFT2.MessageDialog("LastSeen", "No guild-tag given. Try again.")
				return
			end
		elseif (typeinput==4) then
			sectorid = -1
		end
		
		TCFT2.SendData({ cmd="1260", sectorid=sectorid, name=name, guildtag=guildtag })
	end
end

-- Check to see if we have any notification to send
TCFT2.LastSeen.CheckSendData = function()
	local tmplist = {}
	if (#TCFT2.LastSeen.PlayerListCheck>0) then -- Check if we have any unknowns
		for idx, charid in ipairs(TCFT2.LastSeen.PlayerListCheck) do
			if (charid>0) then
				local name = GetPlayerName(charid)
				if (name==nil) then -- no name - we just presume he left sector
					TCFT2.LastSeen.PlayerListCheck[idx] = -1
				else
					if (string.find(name, "reading transponder")==nil) then -- not unknown anymre...
						if (string.sub(name, 1, 1)=="*") then -- No NPC's
							TCFT2.LastSeen.PlayerListCheck[idx] = -1
						else
							if (GetPlayerName()==name) then -- Not our self
								TCFT2.LastSeen.PlayerListCheck[idx] = -1
							else
								local faction = GetPlayerFaction(charid) or 0
								local guildtag = GetGuildTag(charid) or ""
								local health = GetPlayerHealth(charid) or 0.0
								local shipname = GetPrimaryShipNameOfPlayer(charid) or ""
								table.insert(tmplist, { name=name, sectorid=GetCurrentSectorid(), guildtag=guildtag, faction=faction, health=health, shipname=shipname, charid=charid })
								TCFT2.LastSeen.PlayerListCheck[idx] = -1
							end
						end
					end
				end
			else
				TCFT2.LastSeen.PlayerListCheck[idx] = -1
			end
		end
		local tmp = {}
		for idx, charid in ipairs(TCFT2.LastSeen.PlayerListCheck) do
			if (charid~=-1) then
				table.insert(tmp, charid)
			end
		end
		TCFT2.LastSeen.PlayerListCheck = tmp
	end
	if (#tmplist>0) then
		TCFT2.SendData({ cmd="1250", players=tmplist })
	end
	TCFT2.LastSeen.Timer:SetTimeout(TCFT2.LastSeen.Timeout*1000, TCFT2.LastSeen.CheckSendData)
end

-- Check if we have any notifications
TCFT2.LastSeen.CheckNotify = function()
	local now = os.time()
	for idx, n in ipairs(TCFT2.LastSeen.Notifications) do
		if ((TCFT2.Settings.Data.chk_autonotifylastseen or "ON")=="ON") then
			if (n.label==nil) then
				n.label = iup.label{title=n.txt, font=Font.H3*HUD_SCALE, alignment="ACENTER", expand="HORIZONTAL"}
				iup.Append(TCFT2.LastSeen.NotifyBox, n.label)
				TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. n.txt, true)
			end
		end
		if (now - n.time > TCFT2.LastSeen.NotifyTimeout) then
			if (n.label~=nil) then
				iup.Detach(n.label)
				iup.Destroy(n.label)
			end
			n.txt = nil
		end
	end
	local tmp = {}
	for _, n in ipairs(TCFT2.LastSeen.Notifications) do
		if (n.txt~=nil) then table.insert(tmp, n) end
	end
	TCFT2.LastSeen.Notifications = tmp
	if (TCFT2.LastSeen.NotifyBox2~=nil) then
		TCFT2.LastSeen.NotifyBox2.visible = (#TCFT2.LastSeen.Notifications>0 and "YES") or "NO"
	end
	TCFT2.LastSeen.NotifyTimer:SetTimeout(1000, TCFT2.LastSeen.CheckNotify)
end

-- Event
TCFT2.LastSeen.EventPlayerEnteredSector = function(_, charid)
	table.insert(TCFT2.LastSeen.PlayerListCheck, charid)
end

-- Check to see if anybody in the bar when you dock
TCFT2.LastSeen.EventPlayerEnteredStation = function(_, params)
	if (PlayerInStation()) then
		local inbar = GetBarPatrons()
		for idx, charid in ipairs(inbar) do
			table.insert(TCFT2.LastSeen.PlayerListCheck, charid)
		end
	end
end

-- Event
TCFT2.LastSeen.EventHudShow = function()
	-- TCFT2.LastSeen.Notify = TCFT2.LastSeen.Notify or iup.label{title="TEST", font=Font.H4*HUD_SCALE, alignment="ACENTER", expand="HORIZONTAL"}
	if (TCFT2.LastSeen.NotifyBox==nil) then
		TCFT2.LastSeen.NotifyBox = iup.vbox { margin="8x4", expand="YES", alignment="ACENTER" }
		TCFT2.LastSeen.NotifyBox2 = iup.vbox {
			visible="NO",
			iup.fill { size="%69" },
			iup.hbox {
				iup.fill {},
				iup.hudrightframe {
					border="2 2 2 2",
					iup.vbox {
						expand="NO",
						iup.hudrightframe {
							border="0 0 0 0",
							iup.hbox {
								TCFT2.LastSeen.NotifyBox,
							},
						},
					},
				},
				iup.fill {},
			},
			alignment="ACENTER",
			gap=2
		}
		if ((TCFT2.Settings.Data.chk_autonotifyhud or "ON")=="ON") then iup.Append(HUD.pluginlayer, TCFT2.LastSeen.NotifyBox2) end
		TCFT2.LastSeen.NotifyTimer:SetTimeout(1000, TCFT2.LastSeen.CheckNotify)
	end
end

TCFT2.LastSeen.EventSectorChanged = function(_, data)
	-- Add all players in sector to lastseen
	ForEachPlayer(
		function (charid)
			if (charid~=0) then table.insert(TCFT2.LastSeen.PlayerListCheck, charid) end
		end
	)
end

TCFT2.LastSeen.Timer:SetTimeout(TCFT2.LastSeen.Timeout*1000, TCFT2.LastSeen.CheckSendData)

RegisterEvent(TCFT2.LastSeen.EventPlayerEnteredSector, "PLAYER_ENTERED_SECTOR")
RegisterEvent(TCFT2.LastSeen.EventPlayerEnteredStation, "ENTERED_STATION")
RegisterEvent(TCFT2.LastSeen.EventSectorChanged, "SECTOR_CHANGED")
RegisterEvent(TCFT2.LastSeen.EventHudShow, "HUD_SHOW")

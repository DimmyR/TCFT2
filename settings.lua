--[[
TCFT2
(c) By Plozi
Version 1.0.0

** Changelog

]]

TCFT2.Settings = {}
TCFT2.Settings.Debug = 0
TCFT2.Settings.Data = {}
TCFT2.Settings.Data.Scanner = {}
TCFT2.Settings.Data.Binds = {}
TCFT2.Settings.Data.AutoEjectKeep = {}

dofile("ui/ui_settings.lua")

TCFT2.Settings.Update = function() -- Init after changes to settings
	TCFT2.Settings.DoBinds()
	TCFT2.Settings.UpdateButtons()
end

-- Save settings - then call main savesettings to save to ini-file
TCFT2.Settings.Save = function()
	-- General settings
	TCFT2.Settings.Data.username = TCFT2.txt_username.value or ""
	TCFT2.Settings.Data.password = TCFT2.txt_password.value or ""

	TCFT2.Settings.Data.chatecho = TCFT2.chk_chatecho.value or "ON"
	TCFT2.Settings.Data.autoconnect = TCFT2.chk_autoconnect.value or "OFF"
	TCFT2.Settings.Data.displayguildlog = TCFT2.chk_displayguildlog.value or "ON"
	TCFT2.Settings.Data.autorepair = TCFT2.chk_autorepair.value or "ON"
	TCFT2.Settings.Data.autorefill = TCFT2.chk_autorefill.value or "ON"
	TCFT2.Settings.Data.broadcastlocation = TCFT2.chk_broadcastlocation.value or "ON"
	TCFT2.Settings.Data.tcftchattomainchat = TCFT2.chk_tcftchattomainchat.value or "OFF"
	TCFT2.Settings.Data.autoloadsectordata = TCFT2.chk_autoloadsectordata.value or "OFF"
	TCFT2.Settings.Data.stationbuttons = TCFT2.chk_stationbuttons.value or "ON"
	TCFT2.Settings.Data.pdabuttons = TCFT2.chk_pdabuttons.value or "ON"
	TCFT2.Settings.Data.capshipbuttons = TCFT2.chk_capshipbuttons.value or "ON"
	TCFT2.Settings.Data.chk_autonotifylastseen = TCFT2.chk_autonotifylastseen.value or "ON"
	TCFT2.Settings.Data.chk_autonotifyhud = TCFT2.chk_autonotifyhud.value or "ON"
	
	TCFT2.Settings.Data.SoundCargoholdFull = TCFT2.MiningSoundList[TCFT2.MiningSoundList.value] or "None"
	TCFT2.Settings.Data.SoundCargoAutoeject = TCFT2.MiningSoundListAutoeject[TCFT2.MiningSoundListAutoeject.value] or "None"
	-- Asteroidscanner settings
	TCFT2.Settings.Data.Scanner.searchtimeout = tonumber(TCFT2.Mining.Scanner.txt_searchtimeout.value) or 750
	TCFT2.Settings.Data.Scanner.scantimeout = tonumber(TCFT2.Mining.Scanner.txt_scantimeout.value) or 500
	
	local binds = TCFT2.Settings.GetBinds()
	TCFT2.Settings.Data.Binds = {}
	for key, bind in pairs(binds) do
		TCFT2.Settings.Data.Binds[key] = TCFT2.Binds[key].value
	end

	-- Relay settings
	TCFT2.Settings.Data.relayServer = "34.205.215.112" or TCFT2.txt_relayServer.value
	TCFT2.Settings.Data.relayServerPort = "11000" or TCFT2.txt_relayServerPort.value
	TCFT2.Settings.Data.relayServerPassword = "tgftrulez" or TCFT2.txt_relayServerPassword.value

	TCFT2.Settings.Update()
	
	TCFT2.SaveSettings()
	-- TCFT2.AddStatus(TCFT2.colors.YELLOW .. "Settings saved.")
	TCFT2.MessageDialog("Save Settings", "Settings saved.")

end

-- Update settings window when settings tab is activated
TCFT2.Settings.UpdateUI = function()
	-- General settings
	TCFT2.txt_username.value = TCFT2.Settings.Data.username or ""
	TCFT2.txt_password.value = TCFT2.Settings.Data.password or ""
	TCFT2.chk_chatecho.value = TCFT2.Settings.Data.chatecho or "ON"
	TCFT2.chk_autoconnect.value = TCFT2.Settings.Data.autoconnect or "OFF"
	TCFT2.chk_displayguildlog.value = TCFT2.Settings.Data.displayguildlog or "ON"
	TCFT2.chk_autorepair.value = TCFT2.Settings.Data.autorepair or "ON"
	TCFT2.chk_autorefill.value = TCFT2.Settings.Data.autorefill or "ON"
	TCFT2.chk_broadcastlocation.value = TCFT2.Settings.Data.broadcastlocation or "ON"
	TCFT2.chk_tcftchattomainchat.value = TCFT2.Settings.Data.tcftchattomainchat or "OFF"
	TCFT2.chk_autoloadsectordata.value = TCFT2.Settings.Data.autoloadsectordata or "OFF"
	TCFT2.chk_stationbuttons.value = TCFT2.Settings.Data.stationbuttons or "ON"
	TCFT2.chk_pdabuttons.value = TCFT2.Settings.Data.pdabuttons or "ON"
	TCFT2.chk_capshipbuttons.value = TCFT2.Settings.Data.capshipbuttons or "ON"
	TCFT2.chk_autonotifylastseen.value = TCFT2.Settings.Data.chk_autonotifylastseen or "ON"
	TCFT2.chk_autonotifyhud.value = TCFT2.Settings.Data.chk_autonotifyhud or "ON"
	
	for idx, label in ipairs(TCFT2.SoundLabels) do
		if (label==TCFT2.Settings.Data.SoundCargoholdFull) then
			TCFT2.MiningSoundList.value = idx
		end
		if (label==TCFT2.Settings.Data.SoundCargoAutoeject) then
			TCFT2.MiningSoundListAutoeject.value = idx
		end
	end
	
	-- Relay settings
	TCFT2.txt_relayServer.value = TCFT2.Settings.Data.relayServer or "34.205.215.112"
	TCFT2.txt_relayServerPort.value = TCFT2.Settings.Data.relayServerPort or "11000"
	TCFT2.txt_relayServerPassword.value = TCFT2.Settings.Data.relayServerPassword or "tgftrulez"

	-- Asteroidscanner settings
	TCFT2.Mining.Scanner.txt_searchtimeout.value = tonumber(TCFT2.Settings.Data.Scanner.searchtimeout) or 750
	TCFT2.Mining.Scanner.txt_scantimeout.value = tonumber(TCFT2.Settings.Data.Scanner.scantimeout) or 500
	local binds = TCFT2.Settings.GetBinds()
	for key, bind in pairs(binds) do
		TCFT2.Binds[key].value = (TCFT2.Settings.Data.Binds[key] or "")
	end
	TCFT2.AutoEjectActive.value = (TCFT2.Settings.Data.AutoEjectActive or "OFF")
	for orename, value in pairs(TCFT2.Settings.Data.AutoEjectKeep) do
		if (TCFT2.AutoEjectOresKeep[orename]~=nil) then
			TCFT2.AutoEjectOresKeep[orename].value = (value or "OFF")
		end
	end
end


-- Update tcft buttons
TCFT2.Settings.UpdateButtons = function()
	if (TCFT2.ButtonsAdded==true) then return end
	-- TCFT2.PDASecInfo, TCFT2.CapshipSecInfo, TCFT2.StationSecInfo
	-- Station buttons
	if ((TCFT2.Settings.Data.stationbuttons or "ON")=="ON") and (TCFT2.StationSecInfo~=nil) then
		TCFT2.AddButtons(TCFT2.StationSecInfo)
	else
		TCFT2.RemoveButtons(TCFT2.StationSecInfo)
	end
	-- PDAButtons
	if ((TCFT2.Settings.Data.pdabuttons or "ON")=="ON") and (TCFT2.PDASecInfo~=nil) then
		TCFT2.AddButtons(TCFT2.PDASecInfo)
	else
		TCFT2.RemoveButtons(TCFT2.PDASecInfo)
	end

	-- Capship buttons
	if ((TCFT2.Settings.Data.capshipbuttons or "ON")=="ON") and (TCFT2.CapshipSecInfo~=nil) then
		TCFT2.AddButtons(TCFT2.CapshipSecInfo)
	else
		TCFT2.RemoveButtons(TCFT2.CapshipSecInfo)
	end
end

-- Return bind settings
TCFT2.Settings.GetBinds = function()
	local binds = {
		TC2 = { desc="Show TCFT2", alias="", command="tc2", control=TCFT2.Binds.TC2 },
		SAY_TC2 = { desc="TCFT2 chat", alias="TCFT2_SAY 'prompt say_tc2'", command="TCFT2_SAY", control=TCFT2.Binds.SAY_TC2 },
		CARGOLIST = { desc="Show cargolist", alias="TCFT2_CARGOLIST_SHOW 'tc2 cargolist'", command="TCFT2_CARGOLIST_SHOW", control=TCFT2.Binds.CARGOLIST },
		SCANNER_TOGGLE = { desc="Toggle scanner on/off", alias="TCFT2_SCANNER_TOGGLE 'tc2 scanner toggle'", command="TCFT2_SCANNER_TOGGLE", control=TCFT2.Binds.SCANNER_TOGGLE },
		AUTOEJECT_TOGGLE = { desc="Toggle AutoEject on/off", alias="TCFT2_AUTOEJECT_TOGGLE 'tc2 autoeject toggle'", command="TCFT2_AUTOEJECT_TOGGLE", control=TCFT2.Binds.AUTOEJECT_TOGGLE },
		LAST_SEEN_PROMPT = { desc="Last Seen", alias="TCFT2_LAST_SEEN 'tc2 lastseen'", command="TCFT2_LAST_SEEN", control=TCFT2.Binds.LAST_SEEN_PROMPT }
	}
	return binds
end


-- Test if binds conflicts
TCFT2.Settings.TestBinds = function()
	local msg = ""	
	local binds = TCFT2.Settings.GetBinds()
	for key, bind in pairs(binds) do
		local bindkey = TCFT2.Settings.Data.Binds[key] or ""
		if (bindkey~="") then
			local code = gkinterface.GetInputCodeByName(bindkey)
			if (code) then
				local key = gkinterface.GetNameForInputCode(code)
				local cmd = gkinterface.GetCommandForKeyboardBind(code)
				if (cmd) then
					if (not string.match(string.lower(cmd), "tcft2")) and (not string.match(string.lower(cmd), "tc2")) then
						msg = msg .. "The key '" .. key .. "' are already bound to command '" .. cmd .. "'\n"
					end
				end
			end
		end
	end
	if (msg=="") then
		TCFT2.MessageDialog("Keyboard Binds", "No bind conflicts")
	else
		TCFT2.MessageDialog("Keyboard Binds", "The following bind conflicts were found:\n\n" .. msg)
	end
	--local code = gkinterface.GetInputCodeByName('G')
	--print(code)
	--print(gkinterface.GetNameForInputCode(code))
	--print( gkinterface.GetCommandForKeyboardBind(code) )
	--gkinterface.UnbindCommand(cmd)
	--gkinterface.BindCommand(inputcode, cmd)
	--gkinterface.GKProcessCommand("alias TCFT2_SAY 'prompt say_tc2'")
	--gkinterface.GKProcessCommand("bind 'P' TCFT2_SAY")

end

-- Do binds for user
TCFT2.Settings.DoBinds = function()
	local binds = TCFT2.Settings.GetBinds()
	for key, bind in pairs(binds) do
		local bindkey = TCFT2.Settings.Data.Binds[key] or ""
		if (bindkey~="") and ((bind.command or "")~="") then
			local code = gkinterface.GetInputCodeByName(bindkey)
			if ((bind.alias or "")~="") then -- we need some alias here
				gkinterface.GKProcessCommand("alias " .. bind.alias)
			end
			gkinterface.GKProcessCommand("bind '" .. bindkey .. "' " .. bind.command)
		end
	end
end

--[[
TCFT2
(c) By Plozi
Version 1.0.0

** Changelog

]]

TCFT2.CargoList = {}
TCFT2.CargoList.Timer = Timer()
TCFT2.CargoList.SelectedObjectId = nil
TCFT2.CargoList.SelectedNodeId = nil
TCFT2.CargoList.UserSelectedItem = false
TCFT2.CargoList.SortCol = 1
TCFT2.CargoList.SectorScanned = false -- did we alredy scan this sector
TCFT2.CargoList.NumCU = 0
TCFT2.CargoList.Active = false

dofile("ui/ui_cargolist.lua")

TCFT2.DisplayCargolist = function()
	if (TCFT2.CargoList.MainWindow==nil) then
		TCFT2.CargoList.CreateMainWindow()
	end
	PopupDialog(TCFT2.CargoList.MainWindow,iup.CENTER, iup.CENTER)
end

TCFT2.CargoList.ScanNext = function()
	gkinterface.GKProcessCommand("RadarNext")
	local nodeid, objectid = radar.GetRadarSelectionID()
	local tname, health, dist
--	if (objectid~=nil) and (nodeid==2) then -- we hit an object lisa not sure what nodeid will do.  rem'n it out.
	if (objectid~=nil) then -- we hit an object lisa not sure what nodeid will do.  rem'n it out.
		tname, health, dist = GetTargetInfo()
			if (tname~=nil) then
				--print("Item: "..tname.."   ObjectID: "..objectid.."   NodeID: "..nodeid)
			local name, cu = string.match(tname, "^(.+)%s%((.-)cu%)") --lisa HA! Good way to identify cargo.
			if (name~=nil) then
				--print("Item2: "..name.."  CU: "..cu)
				if (string.find(cu, "x")) then
					name = name .. " (" .. cu .. ")"
					--print("Item3: "..name)
					local x1, x2 = string.match(cu, "^(%d+)x%s(%d+)")
					cu = (tonumber(x1) * tonumber(x2)) or 0
				end
				TCFT2.CargoList.ItemList:additem( { itemname=name, distance=tonumber(dist), cargocu=tonumber(cu), objid=objectid, nodeid=nodeid} )
				TCFT2.CargoList.NumCU = TCFT2.CargoList.NumCU + tonumber(cu)
			end
		end
		--print("Starting the timer again.")
		TCFT2.CargoList.Timer:SetTimeout(3, TCFT2.CargoList.ScanNext) --Lisa change to 4ms from 3ms. No diff. back to 3 
	else -- if objectid = nil then we are done
		--if (objectid~=nil) then --lisa; we never would have got here... 
			--TCFT2.CargoList.Timer:SetTimeout(3, TCFT2.CargoList.ScanNext)
		--else
	--print("Got to the ELSE")
			TCFT2.CargoList.Active = false
			if(targetless) then targetless.api.radarlock = false end
			TCFT2.CargoList.SectorScanned = true
			TCFT2.CargoList.UpdateCargoList()
		end
	--end
	
end

TCFT2.CargoList.Refresh = function()
	if(targetless) then targetless.api.radarlock = true end --What does this do?
	TCFT2.CargoList.UserSelectedItem = false
	TCFT2.CargoList.ItemList:clear()
	TCFT2.CargoList.ItemList.numlin=2
	TCFT2.CargoList.ItemList:setcell(2, 1, "  Updating list, please wait...")
	gkinterface.GKProcessCommand("RadarNone")
	TCFT2.CargoList.Timer:Kill()
	TCFT2.CargoList.NumCU = 0
	TCFT2.CargoList.Active = true
	TCFT2.CargoList.ScanNext()
end

TCFT2.CargoList.UpdateCargoList = function(item)
	if (TCFT2.CargoList.ItemList:numitems()>0) then
		TCFT2.CargoList.ItemList:update()
	else
		TCFT2.CargoList.ItemList.numlin=2
		TCFT2.CargoList.ItemList:setcell(2, 1, "  No Cargo found.")
	end
	TCFT2.CargoList.status.value = TCFT2.colors.NORMAL .. TCFT2.CargoList.ItemList:numitems() .. " items (" .. TCFT2.CargoList.NumCU .. " cu)"
end

TCFT2.CargoList.EventSectorChanged = function()
	TCFT2.CargoList.SectorScanned = false
end

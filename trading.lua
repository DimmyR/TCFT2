--[[
TCFT2
(c) By Plozi
Version 1.0.0

** Changelog

]]

TCFT2.Trading = {}

dofile("ui/ui_trading.lua")

TCFT2.Trading.TradeBuffer = {}
TCFT2.Trading.LastUpdate = 0
TCFT2.Trading.LastStationListUpdate = 0
TCFT2.Trading.Itemlist = {}
TCFT2.Trading.LookupType = 1 -- All items as default
TCFT2.Trading.Lookup = {}
TCFT2.Trading.Lookup.Result = {}
TCFT2.Trading.Lookup.SortMode = 1
TCFT2.Trading.SavedCargolist = {}

TCFT2.Trading.StationCompare = {}
TCFT2.Trading.StationCompare.SortMode = 1

TCFT2.Trading.ShipCargo = {}
TCFT2.Trading.ShipCargoForXPCalc = {}
TCFT2.Trading.LastXP = 0
TCFT2.Trading.CargoXp = {} -- If updated - we save found xp here

-------------------------
-- Submit current station
-------------------------

TCFT2.Trading.SubmitStation = function()
	local sectorid = GetCurrentSectorid()
	local buffer = {}
	local desireditems = {}
	if (PlayerInStation() and GetCurrentStationType()~=1) then
		if (TCFT2.Connection.isLoggedin) then
			local stationid = GetStationLocation()

			-- Get all items this station sells
			for i=1, GetNumStationMerch() do 
				-- info : int price, string longdesc, float mass, string sortgroup, int volume, string extendeddesc, string type, string meshfile, bool usable, int itemid, string icon
				local info = GetStationMerchInfo(i)
				if (tonumber(info.mass or 0)==0) then
					info.mass = tonumber(string.match(info.longdesc, "Mass: (%d+) kg") or 0)
					if (info.mass>0) then info.mass = info.mass / 1000 end
				end
				if (tonumber(info.volume or 0)==0) then
					info.volume = tonumber(string.match(info.longdesc, "Volume: (%d+) cu") or 0)
				end
				buffer[info.name] = { name=info.name, sectorid=sectorid, stationid=stationid, sellprice=info.price, buyprice=-1, type=TCFT2.itemtypesraw[info.type], desired=0, mass=info.mass, volume=info.volume }
			end
			
			-- Update prices for selling to this station
			for _,info in StationSellableInventoryPairs() do 
				if (info.type ~= nil) then
					if (tonumber(info.mass or 0)==0) then
						info.mass = tonumber(string.match(info.longdesc, "Mass: (%d+) kg") or 0)
						if (info.mass>0) then info.mass = info.mass / 1000 end
					end
					if (tonumber(info.volume or 0)==0) then
						info.volume = tonumber(string.match(info.longdesc, "Volume: (%d+) cu") or 0)
					end
					-- Check for static price...
					local onepcs = GetStationSellableInventoryPriceByID(info.itemid, 1)
					local many = GetStationSellableInventoryPriceByID(info.itemid, 1000000) / 1000000
					local static = 0
					if (onepcs==many) then
						static = 1
					end
					local itemid=info.itemid
					if (buffer[info.name]) then
						buffer[info.name].buyprice = info.price
						buffer[info.name].staticprice = static
					else 
						buffer[info.name] = { name=info.name, sectorid=sectorid, stationid=stationid, sellprice=-1, buyprice=info.price, type=TCFT2.itemtypesraw[info.type], desired=0, mass=info.mass, volume=info.volume, staticprice = static }
					end
				end
			end
			
			-- Update what this station desires
			for i=1,GetNumStationDesiredItems() do
				local desireditem = GetStationDesiredItem(i)
				if (buffer[desireditem]) then
					buffer[desireditem].desired=1
				else
					buffer[desireditem] = { name=desireditem, sectorid=sectorid, stationid=stationid, buyprice=-1, sellprice=-1, type=-1, desired=1 }
				end
			end
			for _,item in pairs(buffer) do
				table.insert(TCFT2.Trading.TradeBuffer, item)
			end
			TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Submitting station...")
			TCFT2.SendData({ cmd="300" })
		end
	end
end

--------------
-- Item Lookup
--------------

-- Update the items in dropdown
TCFT2.Trading.UpdateLookupItems = function()
	local selectedtype = TCFT2.Trading.LookupType - 1
	local sel = TCFT2.Trading.LookupItemList[TCFT2.Trading.LookupItemList.value]
	TCFT2.Trading.LookupItemList[1] = nil
	local idx = 1
	for _, item in ipairs(TCFT2.Trading.Itemlist) do
		if (selectedtype>0) then -- User have filtered the list
			if (tonumber(item.type)==selectedtype) then
				TCFT2.Trading.LookupItemList[idx] = item.name
				idx = idx + 1
			end
		else
			TCFT2.Trading.LookupItemList[idx] = item.name
			idx = idx + 1
		end
		if (item.name==sel) then TCFT2.Trading.LookupItemList.value = idx-1 end
	end
end

-- User filter on item type
TCFT2.Trading.LookupItemTypeChanged = function(self, type, idx, state)
	if (state==1) then
		TCFT2.Trading.LookupType = idx
		TCFT2.Trading.UpdateLookupItems()
	end
end

-- Trade lookup Item
TCFT2.Trading.DoTradeLookup = function()
	local what = TCFT2.Trading.LookupItemList[TCFT2.Trading.LookupItemList.value or 1]
	local context = TCFT2.Trading.LookupBoughtSold.value or 1
	local syssel = tonumber(TCFT2.Trading.LookupSystem.value)
	local sysid = -1
	if (syssel>1) then
		local system = string.lower(TCFT2.Trading.LookupSystem[syssel])
		local tmp = string.match(system, "^(%S+)")
		sysid = SystemNames[tmp] or 0
	else
		sysid=-1;
	end
	local cmd = { cmd="410", what=what, context=context, system=sysid}
	TCFT2.SendData(cmd)
end

-- Update lookup result from TCFT2.Trading.Lookup.Result
TCFT2.Trading.Lookup.UpdateLookupTable = function()
	TCFT2.Trading.LookupList:clear()
	if (#TCFT2.Trading.Lookup.Result > 0) then
		for index,item in ipairs(TCFT2.Trading.Lookup.Result) do
			item.location = ShortLocationStr(tonumber(item.sectorid)) or "Unknown"
			local context = ''
			if (item.buyprice~="-") then 
				if (context~='') then context = context .. ", " end
				context = context .. 'Bought'
			end
			if (item.sellprice~="-") then
				if (context~='') then context = context .. ", " end
				context = context .. 'Sold'
			end
			if (item.desired=='1') then
				if (context~='') then context = context .. ", " end
				context = context .. 'Wanted'
			end
			item.context = context
			item.station = GetStationName(tonumber(item.stationid)) or "Unknown"
			if (tonumber(item.staticprice)==1) then
				item.fgcolor = "66 220 66"
			end
			TCFT2.Trading.LookupList:additem(item)
		end
		TCFT2.Trading.LookupList:update()
	else
		TCFT2.Trading.LookupList.numlin = 2
		TCFT2.Trading.LookupList:setcell(2, 4, "  No matches.")
	end
end

------------------
-- Station Compare
------------------

-- Update station lists for station compare
TCFT2.Trading.StationCompareUpdateStations = function()
	local sel1 = TCFT2.Trading.StationCompareStation1[TCFT2.Trading.StationCompareStation1.value] or ""
	local sel2 = TCFT2.Trading.StationCompareStation2[TCFT2.Trading.StationCompareStation2.value] or ""
	TCFT2.Trading.StationCompareStation1[1] = nil
	TCFT2.Trading.StationCompareStation2[1] = nil
	for num, station in ipairs(TCFT2.Stations) do
		TCFT2.Trading.StationCompareStation1[num] = station.name
		TCFT2.Trading.StationCompareStation2[num] = station.name
		if (station.name==sel1) then TCFT2.Trading.StationCompareStation1.value = num end
		if (station.name==sel2) then TCFT2.Trading.StationCompareStation2.value = num end
	end
end

-- Compare stations
TCFT2.Trading.DoStationCompare = function()
	TCFT2.Trading.StationCompareResult = {}
	local froms = TCFT2.Trading.StationCompareStation1[TCFT2.Trading.StationCompareStation1.value]
	local tos = TCFT2.Trading.StationCompareStation2[TCFT2.Trading.StationCompareStation2.value]
	local fromsid = nil
	local tosid = nil
	for _, station in ipairs(TCFT2.Stations) do
		if (station.name==froms) then
			fromsid = tonumber(station.id)
		end
		if (station.name==tos) then
			tosid = tonumber(station.id)
		end
	end

	if (fromsid~=nil and tosid~=nil) then
		TCFT2.Trading.StationCompareCancelled = false
		TCFT2.ProgressDialog("Station Compare", "Loading results, please wait...", 
			function() 
				TCFT2.Trading.StationCompareCancelled=true
			end
		)
		TCFT2.SendData({ cmd="470", fromstation=fromsid, tostation=tosid })
	else
		TCFT2.ErrorDialog("Error", "No stations selected.\nAre you connected to the server?")
		TCFT2.Trading.StationCompareList.numlin = 2
		TCFT2.Trading.StationCompareList:setcell(2, 1, TCFT2.colors.RED .. "  Error!")
	end
end

-- Update station compare with results
TCFT2.Trading.UpdateStationCompareTable = function()
	TCFT2.Trading.StationCompareList:clear() -- Empty the matrix
	if (#TCFT2.Trading.StationCompareResult>0) then
		for index,item in ipairs(TCFT2.Trading.StationCompareResult) do
			item.buylocation = ShortLocationStr(item.fromsector) -- .. " - " .. GetStationName(item.fromstation)
			item.selllocation = ShortLocationStr(item.tosector) -- .. " - " .. GetStationName(item.tostation)
			if (tonumber(item.staticprice)==1) then
				item.fgcolor = "66 220 66"
			end
			TCFT2.Trading.StationCompareList:additem(item)
		end
		TCFT2.Trading.StationCompareList:update()
	else
		TCFT2.Trading.StationCompareList.numlin = 2
		TCFT2.Trading.StationCompareList:setcell(2, 1, "  No matches.")
	end
end

--------------------------
-- Search for trade routes
--------------------------

-- Update stationlist for traderoutes
TCFT2.Trading.TradeRouteSearchUpdateStations = function()
	local sel1 = TCFT2.Trading.TradeRouteStation1[TCFT2.Trading.TradeRouteStation1.value] or ""
	local sel2 = TCFT2.Trading.TradeRouteStation2[TCFT2.Trading.TradeRouteStation2.value] or ""
	TCFT2.Trading.TradeRouteStation1[1] = nil
	TCFT2.Trading.TradeRouteStation2[1] = nil
	local currpos = ""
	if (PlayerInStation()) then
		currpos = ShortLocationStr(GetCurrentSectorid()) .. " - " .. GetStationName(GetStationLocation())
	end
	for num, station in ipairs(TCFT2.Stations) do
		TCFT2.Trading.TradeRouteStation1[num] = station.name
		if (sel1 == station.name) then TCFT2.Trading.TradeRouteStation1.value=num end
		TCFT2.Trading.TradeRouteStation2[num] = station.name
		if (sel2 == station.name) then TCFT2.Trading.TradeRouteStation2.value=num end
		if (currpos==station.name) then
			TCFT2.Trading.TradeRouteStation1.value = num
		end
	end
end

-- Perform trade route search
TCFT2.Trading.TradeRouteSearch = function()
	TCFT2.Trading.TradeRouteSearchResult = {}
	local froms = TCFT2.Trading.TradeRouteStation1[TCFT2.Trading.TradeRouteStation1.value]
	local tos = TCFT2.Trading.TradeRouteStation2[TCFT2.Trading.TradeRouteStation2.value]
	local fromsid = nil
	local tosid = nil
	for _, station in ipairs(TCFT2.Stations) do
		if (station.name==froms) then
			fromsid = tonumber(station.id)
		end
		if (station.name==tos) then
			tosid = tonumber(station.id)
		end
	end
	local excludes = {}
	if (TCFT2.Trading.TradeRouteSearchSkipShips.value=="ON") then
		table.insert(excludes, 2)
	end
	if (TCFT2.Trading.TradeRouteSearchSkipWeps.value=="ON") then
		table.insert(excludes, 0)
		table.insert(excludes, 3)
	end
	if (TCFT2.Trading.TradeRouteSearchSkipBatts.value=="ON") then
		table.insert(excludes, 4)
	end
	
	local minprofit = tonumber(TCFT2.Trading.TradeRouteSearchMinProfit[TCFT2.Trading.TradeRouteSearchMinProfit.value]) or 4000
	
	if (fromsid~=nil and tosid~=nil) then
		TCFT2.Trading.TradeRouteSearchCancelled = false
		TCFT2.ProgressDialog("Search for Trade Routes", "Loading results, please wait...", 
			function() 
				TCFT2.Trading.TradeRouteSearchCancelled=true
			end
		)
		TCFT2.SendData({ cmd="480", fromstation=fromsid, tostation=tosid, exclude=excludes, minprofit=minprofit })
	else
		TCFT2.ErrorDialog("Error", "No stations selected.\nAre you connected to the server?")
		TCFT2.Trading.TradeRouteList.numlin = 2
		TCFT2.Trading.TradeRouteList:setcell(2, 1, TCFT2.colors.RED .. "  Error!")
	end
end

TCFT2.Trading.UpdateTradeRouteList = function()
	TCFT2.Trading.TradeRouteList:clear()
	if (#TCFT2.Trading.TradeRouteSearchResult>0) then
		for index,item in ipairs(TCFT2.Trading.TradeRouteSearchResult) do
			item.buylocation = ShortLocationStr(item.fromsector) .. " - " .. GetStationName(item.fromstation)
			item.selllocation = ShortLocationStr(item.tosector) .. " - " .. GetStationName(item.tostation)
			local path = GetFullPath(item.fromsector, { item.tosector })
			item.hops = #path - 1
			if (tonumber(item.staticprice)==1) then
				item.fgcolor = "66 220 66"
			end
			TCFT2.Trading.TradeRouteList:additem(item)
		end
		TCFT2.Trading.TradeRouteList:update()
	else
		TCFT2.Trading.TradeRouteList.numlin = 2
		TCFT2.Trading.TradeRouteList:setcell(2, 1, "  No matches.")
	end
end

-------------------------------------
-- List of Stations that needs update
-------------------------------------

TCFT2.Trading.FetchUpdateStations = function()
	TCFT2.Trading.StationUpdateList:clear()
	TCFT2.Trading.StationUpdatesCancelled = false
	TCFT2.ProgressDialog("Stations needing update", "Loading results, please wait...", 
		function() 
			TCFT2.Trading.StationUpdatesCancelled=true
		end
	)
	TCFT2.SendData({ cmd="350" })
end

-----------------------------------------------
-- Update XP catched for sales - send to server
-----------------------------------------------
TCFT2.Trading.UpdateCargoXP = function()
	for name, xp in pairs(TCFT2.Trading.CargoXp) do
		TCFT2.SendData({ cmd="750", items=TCFT2.Trading.CargoXp })
		break
	end
	TCFT2.Trading.CargoXp = {}
end

---- **** EVENTS **** ----

-- Do trade stuff when entering station
TCFT2.Trading.EventEnteredStation = function(_, params)
	if (TCFT2.Connection.isLoggedin) then
		TCFT2.Trading.SubmitStation() -- Submit station data
	end
	TCFT2.Trading.UpdateShipCargo()
end
	
-- Do trade stuff when leaving station
TCFT2.Trading.EventLeaveStation = function(_, params)

end

-- Sector changed
TCFT2.Trading.EventSectorChanged = function(_, params)

end

-- Update current station cargo - all included
TCFT2.Trading.UpdateShipCargo = function()
	if (not PlayerInStation()) then return end -- Only do this when inside station
	TCFT2.Trading.ShipCargo = {}
	-- Sellable cargo in station (not in ship)
	for _,item in StationSellableInventoryPairs() do 
		if (item.type ~= nil) then
			local name = item.name
			TCFT2.Trading.ShipCargo[name] = TCFT2.Trading.ShipCargo[name] or { count=0, itemids={} }
			if (not TCFT2.Trading.ShipCargo[name].itemids[item.itemid]) then
				TCFT2.Trading.ShipCargo[name].itemids[item.itemid] = item.itemid
			end
			TCFT2.Trading.ShipCargo[name].count = TCFT2.Trading.ShipCargo[name].count + GetInventoryItemQuantity(item.itemid)
		end
	end
end

-- Inventory updated - update what we have in hold for calculation XP if sold
TCFT2.Trading.EventInventoryUpdate = function(event, params)
	TCFT2.Trading.EventInventoryRemove(event, params) -- Update cargo hold - check if we sell and get XP
end

TCFT2.Trading.EventInventoryAdd = function(event, params)
	TCFT2.Trading.UpdateShipCargo()
end

TCFT2.Trading.GetShipcargoByItemId = function(itemid)
	for name,val in pairs(TCFT2.Trading.ShipCargo) do
		if (val.itemids[itemid]) then
			return name, val
		end
	end
	return nil, nil
end

TCFT2.Trading.EventInventoryRemove = function(event, params)
	if (TCFT2.Trading.LastXP>0) then -- we have go some XP
		local itemid = tonumber(params)
		local name, val = TCFT2.Trading.GetShipcargoByItemId(itemid)
		if (name) then
			table.insert(TCFT2.Trading.ShipCargoForXPCalc, { name=name, itemid=itemid, xp = TCFT2.Trading.LastXP, numinhold=val.count })
		end
	end
	TCFT2.Trading.LastXP = 0
	TCFT2.Trading.UpdateShipCargo()
end

TCFT2.Trading.EventTransactonComplete = function(event, params)
	if (not PlayerInStation()) then return end -- Only do this when inside station
	TCFT2.Trading.UpdateShipCargo()
	local numupdate = 0
	for _, item in ipairs(TCFT2.Trading.ShipCargoForXPCalc) do
		local old = item.numinhold
		local curritem = TCFT2.Trading.ShipCargo[item.name] or { count=0 }
		local curr = curritem.count
		local diff = old - curr
		if (diff>0) then
			local xp_pr_item = item.xp / diff -- Find XP pr item
			TCFT2.Trading.CargoXp[item.name] = { name=item.name, xp=xp_pr_item, stationid=GetStationLocation(), sectorid=GetCurrentSectorid() }
			numupdate = numupdate + 1
		end
	end
	if (numupdate>0) then
		TCFT2.Trading.UpdateCargoXP() -- We have soimething to update - do it
	end
	TCFT2.Trading.ShipCargoForXPCalc = {}
	TCFT2.Trading.LastXP = 0
end



-- Mission update stuff
-- Event: CHAT_MSG_SECTORD_MISSION - Data:	missionid=0,msg="Trading/Commerce +6" - we care only if missionid == 0 - means no mission
TCFT2.Trading.EventChatSectordMission = function(event, params)
	if (params.missionid~=0) then return end
	local msg = params.msg
	local xp = string.match(msg, "^Trading/Commerce %+(%d+)$")
	if (xp) then -- we got some trade xp
		-- Check what item are sold and how many - then calculate the xp for each item
		TCFT2.Trading.LastXP = tonumber(xp)
	end
end


-- Save current cargo list (for later loading)
TCFT2.Trading.SaveCargoHold = function()
	if (not PlayerInStation()) then return end
	local stationitemids = GetStationCargoList()
	local tmplist = {}
	for idx, itemid in ipairs(stationitemids) do
		local name = nil
		name = GetInventoryItemName(itemid)
		if (name) then
			-- print("\127ffffffAdding: " .. name .. " (" .. itemid .. ")")
			tmplist[name] = itemid
		end
	end
	
	TCFT2.Trading.SavedCargolist = {}
	local shipinv = GetShipInventory(GetActiveShipID())
	for idx, itemid in ipairs(shipinv.cargo) do
		local name = nil
		name = GetInventoryItemName(itemid)
		if (name) then
			local stationitemid = tmplist[name]
			if (not stationitemid) then
				print("\127ffffffCould not save " .. name .. " - not found in station (" .. itemid .. ")")
			else
				-- print("\127ffffffSaving " .. name .. " (" .. itemid .. ")")
				table.insert(TCFT2.Trading.SavedCargolist, {itemid=stationitemid, quantity=GetInventoryItemQuantity(itemid)})
			end
		end
	end
end

-- Load saved cargo list
TCFT2.Trading.LoadCargoHold = function()
	if (not PlayerInStation()) then return end
	LoadCargo(TCFT2.Trading.SavedCargolist)
end




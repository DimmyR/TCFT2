-- TCFT2
-- Matrix class


TCFT2.MatrixList = {
	-- Column definition - all needed info about the column
	column = {
		-- fieldname can be fieldname:format - eg: itemname:%0.1d
		-- Sortfields format is TYPE:DIRECTION:FIELDNAME - Type can be N or S for number or string - Direction can be A or D for ascending or decending sort
		new = function(fieldname, title, alignment, sortfields, width, params)
			local class = {
				fieldname = fieldname,
				title = title or "ColumnTitle",
				alignment = alignment or "ALEFT",
				sortfields = {}, -- Can be more than one fieldnames if sorting are done on more than one level
				width = width or 10, -- percent - sizes of ALL colums total have to be 100
				prefix = (params~=nil and params.prefix) or "", 
				postfix = (params~=nil and params.postfix) or "",
				format = (params~=nil and params.format) or "",
				replace = (params~=nil and params.replace) or {},
				commavalue = (params~=nil and params.commavalue) or false
			}
			
			-- Title
			function class:settitle(title)
				self.title = title
			end
			
			-- Alignment
			function class:setalignment(align)
				self.alignment = align
			end
			
			-- Sort function
			function class:setsortfields(fields)
				self.sortfields = fields
			end
			
			-- Set size
			function class:setsize(size)
				self.size = size -- percent of total width
			end
			
			function class:setprefix(prefix)
				self.prefix = prefix
			end
			
			function class:setpostfix(postfix)
				self.postfix = postfix
			end

			for idx, sortfield in ipairs(sortfields) do
				local fldtype, dir, sortfld = string.match(sortfield, "^(.):(.):(.-)$")
				local sort = {}
				if (dir==nil) or (sortfld==nil) or (fldtype==nil) then 
					sort = { fldtype="X", dir="X", field="X", format="X" }
				else
					sort = { fldtype=string.lower(fldtype), dir=string.lower(dir), field=sortfld, format=class.format }
				end
				table.insert(class.sortfields, sort)
			end
			
			return class
		end
	},
	
	-- Constructor
	new = function()
		local data = {
			bgcolor_cb = nil,
			expand="YES",
			numcols = 0,
			sortmode = "A", -- Can be A or D for asc or desc
			sortcol = 1, -- Wich column to sort on
			columns = {}, -- List of TGFT2.MatrixListColumn - column header definitions
			items = {}, -- Holds the data - list of TCFT2.MatrixListItem
			initialized = false,
			click_cb = nil,
			enteritem_cb = nil,
			leaveitem_cb = nil,
			selectedindex = 0,
			selecteditem = nil
		}

		local matrix = iup.pdasubsubmatrix { expand="YES", bgcolor="0 0 0 128 *" }
		matrix.data = data -- local data
		
		-- List background colors
		local alpha, selalpha = ListColors.Alpha, ListColors.SelectedAlpha
		local even, odd, sel = ListColors[0], ListColors[1], ListColors[2]
		matrix.data.bg = {
			[0] = even.." "..alpha,
			[1] = odd.." "..alpha,
			[2] = sel.." "..selalpha
		}
		
		function matrix:edition_cb()
			return iup.IGNORE
		end

		-- Update one line in the matrix
		function matrix:updateentry(index, item)
			for idx, column in ipairs(self.data.columns) do
				local fieldname = column.fieldname
				local itemdata = (item[fieldname] or "??")
				if (column.format~="") then
					itemdata = string.format(column.format, (tonumber(itemdata) or 0))
				end
				if (column.replace[tostring(itemdata)]) then
					itemdata = column.replace[tostring(itemdata)]
				end
				if (column.commavalue) then
					itemdata = comma_value(itemdata)
				end
				local displayvalue = " " .. column.prefix .. itemdata .. column.postfix .. " "
				self:setcell(index, idx, displayvalue)
				if (item.cellfgcolors) then
					if (item.cellfgcolors[idx]~=nil) then
						self:setattribute("FGCOLOR", index, idx, item.cellfgcolors[idx])
					end
				end
			end
			self:setattribute("BGCOLOR", index, -1, self.data.bg[math.fmod(index,2)])
			if (item.fgcolor~=nil) then
				self:setattribute("FGCOLOR", index, -1, item.fgcolor)
			end
		end
		
		-- clear - empty the matrix
		function matrix:clear()
			self.data.items = {}
			self:update()
		end
		
		-- Additem - adds an item a table to the list
		function matrix:additem(item)
			table.insert(self.data.items, item)
		end
		
		--GetItem - return item at index
		function matrix:getitem(idx)
			return self.data.items[idx]
		end
		
		-- set sortmode - param can be ASC or DESC
		function matrix:set_sortmode(mode)
			mode = string.lower(mode)
			if (mode=="desc") then
				self.data.sortmode = "D"
			else
				self.data.sortmode = "A"
			end
		end
		
		-- setsortcol - set column to sort on
		function matrix:set_sortcol(col)
			local numcols = #self.data.columns
			if (col>numcols) then 
				self.data.sortcol = numcols
			else
				self.data.sortcol = col
			end
		end

		-- addColumn - adds a column to the list ()
		function matrix:addcolumn(column)
			table.insert(self.data.columns, column)
		end
		
		-- Set custom callbacks...
		function matrix:setclickcb(func)
			self.data.click_cb = func
		end
		
		function matrix:setenteritemcb(func)
			self.data.enteritem_cb = func
		end
		
		function matrix:setleaveitemcb(func)
			self.data.leaveitem_cb = func
		end
		
		-- Local click_cb
		function matrix:click_cb(line, col)
			if (self.data.click_cb) then
				self.data.click_cb(self, line, col)
			end
			if (line==0) then -- sort
				if (self.data.sortcol==col) then -- same col - just switch direction
					self.data.sortmode = (self.data.sortmode=="A" and "D") or "A"
				else
					self.data.sortmode = "A"
				end
				self.data.sortcol = col
				self:update()
			end
			return iup.IGNORE
		end
		
		-- Local enteritem cb
		function matrix:enteritem_cb(line, col)
			if (self.data.enteritem_cb) then
				self.data.enteritem_cb(self, line, col)
			end
			self:setattribute("BGCOLOR", line, -1, self.data.bg[2])
			self.data.selectedindex = line
			self.data.selecteditem = self.data.items[line]
		end
		
		-- Local leaveitem cb
		function matrix:leaveitem_cb(line, col)
			if (self.data.leaveitem_cb) then
				self.data.leaveitem_cb(self, line, col)
			end
			self:setattribute("BGCOLOR", line, -1, self.data.bg[math.fmod(line,2)])
			self.data.selectedindex = -1
			self.data.selecteditem = nil
		end
		
		-- SelectedIndex - current selected index
		function matrix:selectedindex()
			return self.data.selectedindex
		end
		
		-- selecteditem - current selected item
		function matrix:selecteditem()
			return self.data.selecteditem
		end
		
		-- Map callback - set up sizes
		function matrix:map()
			self:OnEvent("TCFT2_MAIN_MAP", nil)
		end
		
		function matrix:OnEvent(eventname, params)
			if (eventname=="TCFT2_MAIN_MAP") then
				if (self.data.initialized==false) then
					self:init()
				end
				local wid = math.floor(getwidth(self) - Font.Default) -- / #self.columns)
				for idx, column in ipairs(self.data.columns) do
					self["width"..idx] = math.floor(wid * (column.width/100))
				end
			end
		end
		
		-- Return number of items in list
		function matrix:numitems()
			return #self.data.items
		end
		
		function matrix.sortfunc(self, a, b)
			local sortfields = self.data.columns[self.data.sortcol].sortfields
			if (#sortfields<1) then return true end
			-- Sortfield should be in this format: N|S:A|D:fieldname - N = tonumber - S = String -- A = asc - D = desc
			for idx, sortfield in ipairs(sortfields) do
				-- Check if we need tonumber on field to sort - starting witn N: mean tonumber it
				if (sortfield.dir~="X") then
					local flda, fldb -- The field data itself
					if (sortfield.fldtype=="n") then
						flda = (tonumber(a[sortfield.field]) or 0)
						fldb = (tonumber(b[sortfield.field]) or 0)
					else
						flda = a[sortfield.field] or ""
						fldb = b[sortfield.field] or ""
					end
					if (flda~=fldb) or (idx==#sortfields) then -- Not equal or last sortfield - just return the diff
						if (self.data.sortmode=="A") then -- ascending sort?
							if (sortfield.dir=="a") then return flda<fldb end
							return flda>fldb
						else -- Decending sort
							if (sortfield.dir=="a") then return flda>fldb end
							return flda<fldb
						end
					end
				else
					-- console_print("RETURNING HARD FALSE")
					return false
				end
			end
		end
		
		function matrix:sort()
			table.sort(self.data.items, function(a, b) return matrix.sortfunc(self, a, b) end)
		end
		
		-- init - prepare the list AFTER all is set up
		function matrix:init()
			self.numcol = #self.data.columns
			for idx, column in ipairs(self.data.columns) do
				self["ALIGNMENT"..idx] = column.alignment
				self["0:"..idx] = column.title
			end
			self.data.initialized = true
			-- self:update()
		end
		
		-- Update - update items after added objects, sorting etc
		function matrix:update()
			self.dellin = "1--1"
			if (#self.data.items) then
				self:sort()
				self.numlin = #self.data.items
				for idx, item in ipairs(self.data.items) do
					self:updateentry(idx, item)
				end
			end
			self.data.selectedindex = -1
			self.data.selecteditem = nil
		end
		RegisterEvent(matrix, "TCFT2_MAIN_MAP")
		return matrix
	end
}

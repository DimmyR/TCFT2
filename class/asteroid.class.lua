-- TCFT2
-- Asteroid class - keeps an asteroid and its data


TCFT2.Asterioid = {
	new = function(objectid)
		local roid = {
			temp = -1
			minerals = {} -- Keeps a list of minerals for this roid
			
			function roid:addMineral(name, density)
				if (self.minerals[name]==nil) then
					self.minerals[name] = {}
				end
				self.minerals[name].density = density
			end
			
			function roid:getMinerals()
				return self.minerals
			end
			
			function roid:contains(name)
				return (self.minerals[name]~=nil and true) or false)
			end
			
			function roid:setTemp(temp)
				self.temp = temp
			end
			
			function roid:getTemp()
				return self. temp
			end
			
			function roid:sortMinerals()
				
			end
		}
		return roid
	end
}

-- TCFT2
-- Matrix class


TCFT2.MatrixList = {
	-- Column definition - all needed info about the column
	column = {
		new = function(title, alignment, sortfields, width, prefix, postfix)
			local class = {
				title = title or "ColumnTitle",
				alignment = alignment or "ALEFT",
				sortfields = sortfields or {}, -- Can be more than one fieldnames if sorting are done on more than one level
				width = width or 10, -- percent - sizes of ALL colums total have to be 100
				prefix = prefix or "", 
				postfix = postfix or ""
			}
			
			-- Title
			function class:settitle(title)
				self.title = title
			end
			
			-- Alignment
			function class:setalignment(align)
				self.alignment = align
			end
			
			-- Sort function
			function class:setsortfields(fields)
				self.sortfields = fields
			end
			
			-- Set size
			function class:setsize(size)
				self.size = size -- percent of total width
			end
			
			function class:setprefix(prefix)
				self.prefix = prefix
			end
			
			function class:setpostfix(postfix)
				self.postfix = postfix
			end
			return class
		end
	},
	
	-- Constructor
	new = function()
		local class = {
			bgcolor_cb = nil,
			expand="YES",
			numcols = 0,
			sortmode = "A", -- Can be A or D for asc or desc
			sortcolumn = 1, -- Wich column to sort on
			columns = {}, -- List of TGFT2.MatrixListColumn - column header definitions
			items = {}, -- Holds the data - list of TCFT2.MatrixListItem
			matrix = iup.pdasubsubmatrix { expand="YES", bgcolor="255 255 255 128 *" },
			initialized = false,
			click_cb = nil,
			enteritem_cb = nil,
			leaveitem_cb = nil,
			this = nil
		}

		
		-- List background colors
		local alpha, selalpha = ListColors.Alpha, ListColors.SelectedAlpha
		local even, odd, sel = ListColors[0], ListColors[1], ListColors[2]
		class.bg = {
			[0] = even.." "..alpha,
			[1] = odd.." "..alpha,
			[2] = sel.." "..selalpha
		}

		-- Update one line in the matrix
		function class:updateentry(index, item)
			for idx,listitem in ipairs(item) do
				local column = self.columns[idx]
				local displayvalue = column.prefix .. listitem .. column.postfix
				self.matrix:setcell(index, idx, displayvalue)
			end
			if (self.bgcolor_cb) then
				self.matrix:setattribute("BGCOLOR", index, -1, self.bgcolor_cb(index, item))
			else
				self.matrix:setattribute("BGCOLOR", index, -1, self.bg[math.fmod(index,2)])
			end
		end
		
		-- clear - empty the matrix
		function class:clear()
			self.items = {}
			self:update()
		end
		
		-- Additem - adds an item a table to the list
		function class:additem(item)
			item["matrixclass"] = self
			table.insert(self.items, item)
		end
		
		-- Set bgcolor_cb - function that return bgcolor for line based on index and item data
		function class:setbgcolorcb(func)
			self.bgcolor_cb = func
		end
		
		-- set sortmode - param can be ASC or DESC
		function class:set_sortmode(mode)
			mode = string.lower(mode)
			if (mode=="desc") then
				self.sortmode = "D"
			else
				self.sortmode = "A"
			end
		end
		
		-- setsortcol - set column to sort on
		function class:set_sortcol(col)
			if (col>numcols) then 
				self.sortcolumn = numcols
			else
				self.sortcolumn = col
			end
		end

		-- Set BgColor
		function class:setbgcolor(color)
			self.matrix.bgcolor = color
		end
		
		-- addColumn - adds a column to the list ()
		function class:addcolumn(column)
			table.insert(self.columns, column)
		end
		
		-- set click callback
		function class:set_clickcb(func)
			self.click_cb = func
		end
		
		-- Local click_cb
		function class:click_cb(line, col)
			local matrixclass = self.matrixclass
			if (line==0) then -- sort
				if (matrixclass.sortcol==col) then -- same col - just switch direction
					matrixclass.sortmode = (matrixclass.sortmode=="A" and "D") or "A"
				end
				matrixclass.sortcol = col
				matrixclass:update()
			end
			return iup.IGNORE
		end
		
		-- set enteritem callback
		function class:set_enteritemcb(func)
			self.enteritem_cb = func
		end
		
		-- Local enteritem cb
		function class:enteritem_cb(line, col)
			local matrixclass = self.matrixclass
			if (matrixclass.enteritem_cb) then
				matrixclass.enteritem_cb(matrixclass, line, col)
			else
				matrixclass.matrix:setattribute("BGCOLOR", line, -1, self.bg[2])
			end
		end
		
		-- set leaveitem callback
		function class:set_leaveitemcb(func)
			self.leaveitemcb_cb = func
		end
		
		-- Local leaveitem cb
		function class:leaveitem_cb(line, col)
			local matrixclass = self.matrixclass
			if (matrixclass.leaveitem_cb) then
				matrixclass.leaveitem_cb(matrixclass, line, col)
			else
				matrixclass.matrix:setattribute("BGCOLOR", line, -1, self.bg[math.fmod(line,2)])
			end
		end
		
		-- Set expand param
		function class:noexpand()
			self.matrix.expand = "NO"
		end

		-- getObject return the iup.pdasubsubmatrix {} object configured
		function class:getobject()
			if (not self.initialized) then 
				console_print("ERROR: Matrix not initialized.")
				return nil
			end
			return self.matrix
		end

		-- Map callback - set up sizes
		function class:map()
			local wid = math.floor(getwidth(self.matrix) - Font.Default) -- / #self.columns)
			for idx, column in ipairs(self.columns) do
				self.matrix["width"..idx] = math.floor(wid * (column.width/100))
			end
		end
		
		function class:sortfunc(a, b)
			local matrixclass
			matrixclass = a.matrixclass
			local sortfields = matrixclass.columns[matrixclass.sortcolumn].sortfields
			if (#sortfields<1) then return true end
			-- Sortfield should be in this format: N|S:A|D:fieldname - N = tonumber - S = String -- A = asc - D = desc
			for idx, sortfield in ipairs(sortfields) do
				-- Check if we need tonumber on field to sort - starting witn N: mean tonumber it
				local flda, fldb -- The field data itself
				local tonum, dir, sortfld = string.match(sortfield, "^.:.:(.-)")
				if (dir==nil) or (sortfld==nil) or (tonum==nil) then return true end
				dir = string.lower(dir)
				flda = a[sortfld]
				fldb = b[sortfld]
				if (tonum) then
					flda = (tonumber(flda) or 0)
					fldb = (tonumber(fldb) or 0)
				end
				if (flda~=fldb) then -- Not equal - just return the diff
					if (matrixclass.sortmode=="A") then -- ascending sort?
						if (dir=="a") then return flda<fldb end
						return flda>fldb
					else -- Decending sort
						if (dir=="a") then return flda>fldb end
						return flda<fldb
					end
				else -- Items equal - check if more levels
					if (idx==#sortfields) then -- Last level? if so then return result
						if (matrixclass.sortmode=="A") then -- ascending sort?
							if (dir=="a") then return flda<fldb end
							return flda>fldb
						else -- Decending sort
							if (dir=="a") then return flda>fldb end
							return flda<fldb
						end
					end
				end
			end
			return true
		end
		
		-- Sort - sort items
		function class:sort()
			table.sort(self.items, self.sortfunc)
		end
		
		-- init - prepare the list AFTER all is set up
		function class:init()
			print("Init ")
			self.matrix.click_cb = self.click_cb
			self.matrix.enteritem_cb = self.enteritem_cb
			self.matrix.leaveitem_cb = self.leaveitem_cb
			self.matrix.numcol = #self.columns
			for idx, column in ipairs(self.columns) do
				self.matrix["ALIGNMENT"..idx] = column.alignment
				self.matrix["0:"..idx] = column.title
			end
			self.initialized = true
			self:update()
		end
		
		-- Update - update items after added objects, sorting etc
		function class:update()
			print("Updating ")
			self.matrix.dellin = "1--1"
			if (#self.items) then
				self:sort()
				self.matrix.numlin = #self.items
				for idx, item in ipairs(self.items) do
					self:updateentry(idx, item)
				end
			end
		end
		
		class.matrix.matrixclass = class
		return class
	end
}
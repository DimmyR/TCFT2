--Relay plugin by CrazySpence 0.6 with stolen code from slimes relay that was then chopped to bits by me! Apparantly he stole it originally and also chopped it to bits, so this is like..a VO family heirloom
--updated by DimmyR to plug into TCFT2 2017-09-28

TCP = dofile("relay/tcpsock.lua")

TCFT2.relay = {}

local relayServer = TCFT2.Settings.Data.relayServer or "34.205.215.112"
local relayServerPort = TCFT2.Settings.Data.relayServerPort or "11000"
local relayServerPassword = TCFT2.Settings.Data.relayServerPassword or "tgftrulez"

--Relay Configuration Vars, uncomment features to enable--
--TCFT2.relay.sector = 1 --Sector chatter
--TCFT2.relay.system = 1 -- System Chatter
--TCFT2.relay.group = 1 -- Display group chats
--TCFT2.relay.channel = 1 -- 100 channel relay
TCFT2.relay.debugprint = 1
TCFT2.relay.updatebank = 1 --Show guild bank updates
TCFT2.relay.motd = 1 --Show motd changes
TCFT2.relay.guildconnects = 1 --Show when members join/quit
TCFT2.relay.allguildchat = 1 --Guild chat
--TCFT2.relay.deneb = 1 --Deneb !score
--TCFT2.relay.nation = 1 --ch11 support
--TCFT2.relay.nationchannel = "GUILD" --Destination of the relay would either be "GUILD" or "RELAY"
--TCFT2.relay.ctcchannel = "RELAY"    -- ^
--TCFT2.relay.itanictc = 1 --Show CtC
--TCFT2.relay.sercoctc = 1 -- ^
TCFT2.relay.guild = "TGFT" -- (cap sensitive) Set this to your guild so that the relay isn't accidentally turned on by your alts if you are doing guild chat relay, leave commented out to not care. 
--End Setup

-- Local variables and functions
local relay_handle = nil
local relayon = 0
local lookforbankupdate = false
local sc = string.char
local r = sc(3) -- IRC color code modifier

local c = { -- irc colors
	[-1]=r.."10", -- teal
	[0]=r.."15", -- silver/grey
	[1]=r.."12", -- itani (blue)
	[2]=r.."04", -- serco (red)
	[3]=r.."07", -- orange/yellow (uit)
	b=sc(2), -- bold
	n=sc(15), -- normal
	purple=r.."06",
	yellow=r.."08",
	white=r.."00",
	black=r.."01",
	blue=r.."02", -- dark blue
	pink=r.."13",
	aqua=r.."11",
	green=r.."09", -- green that hurts your eyes...
	grey=r.."14",
}

local ranks = {
	[0] = "Member",
	[1] = "Lieutenant",
	[2] = "Council",
	[3] = "Council and Lt.",
	[4] = "Commander"
}

local function irc_colorname(name, faction)
	if type(name) == "number" then
		faction = GetPlayerFaction(name) or 1
		name = GetPlayerName(name)
	end
	return c[faction]..name..c[-1]
end

local function irc_prettymsg(msg)
	local prettymsg = c.b..c[0].."> "..c.n..c[-1]..msg
	return prettymsg
end

local function formatguildmsg(msg, name, factionid, isemote, location)
	local formatname = not isemote and "<%s%s%s>" or "%s%s%s"
	local formatstr = not location and "%s(guild) "..formatname.." %s" or "%s(guild) ["..location.."] "..formatname.." %s"
	return string.format(formatstr, c[-1], c[factionid], name, c[-1], msg)
end

local function getrank(charid, rank, name)
	local color = c[GetPlayerFaction(charid)]
	local rankstr = ranks[rank]
	return string.format("%s%s %s(%s)", color, name, c[-1], rankstr)
end

local function getguildmembers()
	if not relayon then return "Error: Not logged in!" end
	local members = {}
	for i = 1, GetNumGuildMembers() do
		members[i] = getrank(GetGuildMemberInfo(i))
	end
	 
	return table.concat(members, ', ')
end

local function getsectorplayers()
	if not relayon then return "Error: Not logged in!" end
	local players = {}
	local function parseplayer(charid)
		if (not charid) or (charid == 0) then return end
		local name = GetPlayerName(charid)
		if name and not name:match("^*") then
			table.insert(players, irc_colorname(name, GetPlayerFaction(charid)))
		end
	end
	ForEachPlayer(parseplayer)
	return table.concat(players, ", ")
end

local function denebscore()
	local fontcolor = '\127ffffff'
	local conq = GetConqueredSectorsInSystem(4)
	local itani, serco = 0, 0

	for _, v in pairs(conq) do
		if     (v == 1) then itani = itani + 1
		elseif (v == 2) then serco = serco + 1
		end
	end

	return ('Deneb Score: Itani: '..itani..' / Serco: '..serco)
end


local function send(val)
    if relayon == 0 then
        TCFT2.relay.print("Not Relayer, this shouldn't happen")
    else
        relay_handle:Send(val)
    end
end

local function send_msg(msg,type)
	send(type .. " " .. msg)
end

TCFT2.relay.relay_stop = function()
   if relay_handle then
   	--   print("Disconnecting from the IRC Relay server")
	  TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Disconnecting from the IRC Relay server...", true)
      relay_handle:Send("LOGOUT\n")
      relay_handle.tcp:Disconnect()
      relay_handle = nil
      relayon = 0
   end
end

function TCFT2.relay:OnEvent(event, data, ...)
	if relayon == 0 then return end
	if event == "UNLOAD_INTERFACE" then
		TCFT2.relay.relay_stop()
	elseif event == "CHAT_MSG_GUILD" or event == "CHAT_MSG_GUILD_EMOTE" then
		local msg = data.msg:lower()
		if self.allguildchat then
			send_msg(formatguildmsg(data.msg, data.name, data.faction, event:match("EMOTE") and true, SystemNames[GetSystemID(data.location)]),"GUILD")
		end
	elseif (event == "CHAT_MSG_CHANNEL_ACTIVE" or event == "CHAT_MSG_CHANNEL_EMOTE_ACTIVE") then
		if data.channelid == 100 and self.channel then
			local formatstr = not event:match("EMOTE") and "%s[%d]%s <%s> %s" or "%s[%d]%s %s %s"
			local sendmsg = string.format(formatstr, c[-1], data.channelid, c[-1], irc_colorname(data.name, data.faction), data.msg)
			send_msg(sendmsg,"RELAY")
		elseif data.channelid == 11 and self.nation then
			local formatstr = not event:match("EMOTE") and "%s[%d]%s <%s> %s" or "%s[%d]%s %s %s"
			local sendmsg = string.format(formatstr, c[-1], data.channelid, c[-1], irc_colorname(data.name, data.faction), data.msg)
			send_msg(sendmsg,self.nationchannel)
		elseif data.channelid == 201 and self.itanictc then
			local formatstr = not event:match("EMOTE") and "%s[%d]%s <%s> %s" or "%s[%d]%s %s %s"
			local sendmsg = string.format(formatstr, c[-1], data.channelid, c[-1], irc_colorname(data.name, data.faction), data.msg)
			send_msg(sendmsg,self.ctcchannel)
		elseif data.channelid == 202 and self.sercoctc then
			local formatstr = not event:match("EMOTE") and "%s[%d]%s <%s> %s" or "%s[%d]%s %s %s"
			local sendmsg = string.format(formatstr, c[-1], data.channelid, c[-1], irc_colorname(data.name, data.faction), data.msg)
			send_msg(sendmsg,self.ctcchannel)
		end
	elseif (event == "CHAT_MSG_SECTOR" or event == "CHAT_MSG_SECTOR_EMOTE") and self.sector then
		local formatstr = not event:match("EMOTE") and "%s(sector) <%s%s> %s" or "%s(sector) %s%s %s"
		local sendmsg = string.format(formatstr, c.aqua, irc_colorname(data.name, data.faction), c.aqua, data.msg)
		send_msg(sendmsg,"GUILD")
	elseif event == "CHAT_MSG_SYSTEM" and self.system then
		send_msg(c.purple.."(system) ["..ShortLocationStr(data.location).."] <"..irc_colorname(data.name, data.faction)..c.purple.."> "..data.msg,"GUILD")
	elseif event == "CHAT_MSG_GROUP" and self.group then
		send_msg(c.yellow.."(group) <"..irc_colorname(data.name, data.faction)..c.yellow.."> "..data.msg,"GUILD")
		
	elseif event == "CHAT_MSG_SERVER_GUILD" then
		if not data.msg:match(" logged (%w+).$") then
			local msg
			if data.msg:match("joined the guild") then
				local membername = data.msg:match("(.-) joined the guild")
				local membercharid
				for i=1, GetNumGuildMembers() do
					local charid, rank, name1 = GetGuildMemberInfo(i)
					if name1 == membername then membercharid = charid break end
				end
				local faction = GetPlayerFaction(membercharid) or 1
				membername = c[faction]..membername..c[-1]
				msg = irc_prettymsg(membername.." joined the guild.")
			else
				msg = irc_prettymsg(msg)
			end
			send_msg(msg,"GUILD")
		end
	elseif event == "GUILD_MEMBER_ADDED" and self.guildconnects then
		local t= Timer()
		t:SetTimeout(700, function()
			local msg = irc_colorname(data)
			send_msg(irc_prettymsg(msg.." logged on."),"GUILD")
		end)
	elseif event == "GUILD_MEMBER_REMOVED" and self.guildconnects then
		local msg = irc_colorname(data)
		send_msg(irc_prettymsg(msg.." logged off."),"GUILD")
	elseif event == "GUILD_BALANCE_UPDATED" and self.updatebank then
		lookforbankupdate = true
		Guild.getbanklogpage(1)
	elseif event == "GUILD_BANK_LOG" and lookforbankupdate then
		lookforbankupdate = false
		local bankdata = data[1]
		local str = "added to"
		local reason = bankdata.description ~= "" and " (reason: "..bankdata.description..")" or "" 
		local amount
		if bankdata.action == "deposit" then
			amount = bankdata.current - bankdata.previous
		else
			str = "subtracted from"
			amount = bankdata.previous - bankdata.current
		end
		local plural = amount ~= 1 and "s have" or " has"
		send_msg(irc_prettymsg(c.n..amount.." credit"..plural.." been "..str.." the Guild bank by "..bankdata.charname..reason),"GUILD")
	elseif event == "SECTOR_LOADED" and self.guild then
	   if (not GetGuildTag() or GetGuildTag() ~= self.guild) then
	   	  TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Disconnecting your relay since you're not logged on as a TGFT alt...", true)
	      TCFT2.relay.relay_stop()	
	   end
	elseif (event == "CHAT_MSG_CHANNEL" or event == "CHAT_MSG_CHANNEL_EMOTE") then
	    if data.channelid == 11 then
		   if self.nation then
		      local formatstr = not event:match("EMOTE") and "%s[%d]%s <%s> %s" or "%s[%d]%s %s %s"
		      local sendmsg = string.format(formatstr, c[-1], data.channelid, c[-1], irc_colorname(data.name, data.faction), data.msg)
		      send_msg(sendmsg,self.nationchannel)
		   end
		end
	elseif event == "CHAT_MSG_SERVER_CHANNEL" then
	   if data.channelid == 201 then
           if self.itanictc then
		      local formatstr = "%s[%d]%s %s"
		      local sendmsg = string.format(formatstr, c[-1], data.channelid ,c[1], data.msg)
		      send_msg(sendmsg,self.ctcchannel)
		   end
		elseif data.channelid == 202 then		
		   if self.sercoctc then
		      local formatstr = "%s[%d]%s %s"
		      local sendmsg = string.format(formatstr, c[-1], data.channelid,c[2], data.msg)
		      send_msg(sendmsg,self.ctcchannel)
		   end
	    end
	end
end

TCFT2.relay.events = {
	["UNLOAD_INTERFACE"] = true,
	["GUILD_MEMBER_ADDED"] = true,
	["GUILD_MEMBER_REMOVED"] = true,
	["CHAT_MSG_GUILD"] = true,
	["CHAT_MSG_GUILD_EMOTE"] = true,
	["CHAT_MSG_SECTOR"] = true,
	["CHAT_MSG_SECTOR_EMOTE"] = true,
	["GUILD_BALANCE_UPDATED"] = true,
	["CHAT_MSG_SYSTEM"] = true,
	["CHAT_MSG_SERVER_GUILD"] = true,
	["SECTOR_LOADED"] = true,
	["PLAYER_LOGGED_OUT"] = true,
	["CHAT_MSG_GROUP"] = true,
	["GUILD_BANK_LOG"] = true,
	["CHAT_MSG_CHANNEL_ACTIVE"] = true,
	["CHAT_MSG_CHANNEL_EMOTE_ACTIVE"] = true,
	["CHAT_MSG_SERVER_CHANNEL"] = true,
	["CHAT_MSG_CHANNEL"] = true,
	["CHAT_MSG_CHANNEL_EMOTE"] = true,
}

for event in pairs(TCFT2.relay.events) do RegisterEvent(TCFT2.relay, event) end

local function Connected(conn,success)
   if conn then
    --   print("Connected to the IRC Relay Server")
	  TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Connected to the IRC Relay server.", true)
      conn:Send("REGISTER " .. "\"" .. GetPlayerName() .. "\"" .. " " .. relayServerPassword)
   else
      print(success)
      relay_handle = nil
   end
end

local function Disconnected()
--    print("Connection to Relay Server interrupted")
   TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.RED .. "Connection to Relay server interrupted.", true)
   if relay_handle then
      relay_handle = nil
      relayon = 0
   end
end

local function Incoming(conn,line)
   local output
   if line == "PING" then 
      relay_handle:Send("PONG")
   elseif line == "ACTIVATED" then
      relayon = 1
   elseif line == "ONLINE" then
      send_msg(irc_prettymsg("Online: "..getguildmembers()),"GUILD")
   elseif line == "BANK" then
      send_msg(irc_prettymsg("Bank: "..c.n..GetGuildBalance() ..c[-1].." credits."),"GUILD")
   elseif line == "SCORE" and self.deneb then
      send_msg(irc_prettymsg(denebscore()),"GUILD")  
   elseif line == "MOTD" and self.motd then
      local motd = filter_colorcodes(GetGuildMOTD())
	  for motdline in motd:gmatch("([^\n]+)") do
	     send_msg(irc_prettymsg("MOTD: " .. motdline),"GUILD")
	  end
   elseif string.sub(line,1,6) == "GUILD " then
      output = "say_guild " .. string.sub(line,6)
      gkinterface.GKProcessCommand(output)
   elseif string.sub(line,1,6) == "RELAY " then
	  output = "say_channel " .. string.sub(line,6)
	  gkinterface.GKProcessCommand(output)
   else
      print(line)
   end
end

TCFT2.relay.relay_init = function()
   if not relay_handle then
      relay_handle = TCP.make_client(relayServer, relayServerPort, Connected, Incoming, Disconnected)
	--   print("Connecteding to the IRC Relay server")
      TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Connecting to the IRC Relay server...", true)
   else 
    --   print("Already connected to the IRC Relay server")
      TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.YELLOW .. "Already connected to the IRC Relay server.", true)
   end
end

RegisterUserCommand("relayon",TCFT2.relay.relay_init)
RegisterUserCommand("relayoff",TCFT2.relay.relay_stop)

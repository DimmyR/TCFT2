local TargetNodeID = nil
local TargetObjectID = nil
local TargetCharID = 1
local TargetName = nil
local TargetActive = false
local gkpc = gkinterface.GKProcessCommand
local getradar = radar.GetRadarSelectionID
local setradar = radar.SetRadarSelection

function AToggle()
	if TargetActive then
		TargetActive = false
		UnregisterEvent(AHudShow, "HUD_SHOW")
		UnregisterEvent(AGrpMsg, "CHAT_MSG_GROUP")
		UnregisterEvent(ASecJumpMsg, "CHAT_MSG_SECTORD_SECTOR")
		print("Attack mode De-Activated.")
	else
		TargetActive = true
		RegisterEvent(AHudShow, "HUD_SHOW")
		RegisterEvent(AGrpMsg, "CHAT_MSG_GROUP")
		RegisterEvent(ASecJumpMsg, "CHAT_MSG_SECTORD_SECTOR")
		print("Attack mode Activated.")
	end
end

function AGetPlayerIDs(TargetCharID)
	TargetCharID = RequestTargetStats() or 1
	--if not TargetCharID then return end
	TargetNodeID = GetPlayerNodeID(TargetCharID)
	TargetObjectID = GetPrimaryShipIDOfPlayer(TargetCharID)
	return TargetNodeID, TargetObjectID, TargetCharID
end

function ASetShip(_, data)
	TargetNodeID, TargetObjectID, TargetCharID = AGetPlayerIDs(TargetCharID)
	local msg ="say_group TargetSpecifiedAs: "..TargetCharID 
	TargetName = GetPlayerName(TargetCharID)
	gkpc(msg)
end

function AReTargetCharID(data)
if tonumber(data) == tonumber(TargetCharID) then
	TargetNodeID = GetPlayerNodeID(data)
	TargetObjectID = GetPrimaryShipIDOfPlayer(data)
	setradar(TargetNodeID, TargetObjectID)
	end
end

function AHudShow(_, data)
if not TargetActive then return end
if not TargetCharID then return end
ForEachPlayer(AReTargetCharID) 
end

function AGrpMsg(_, data)
if not TargetActive then return end
local TheSystem, TheSector, TheCharID, TheName
	if (string.find(data["msg"],"TargetSpecifiedAs:")) then
		_, _, TargetCharID = string.find(data["msg"], " (%d+)")
		--print("Target Data Received: "..TargetCharID)
		TargetName = GetPlayerName(tonumber(TargetCharID))
		print("Targeted Player is:"..TargetName)
		setradar(TargetNodeID, TargetObjectID)		
	end
	if (string.find(data["msg"],"TargetJumpedTo:")) then
		TheName, TheSystem, TheSector = string.match(data["msg"], "(%S+) jumped to (%S+) System, Sector (%S+)")
		if  (TheName == TargetName) then
			local location = TheSystem.." "..TheSector
			NavRoute.SetFinalDestination(SectorIDFromLocationStr(location))
			gkpc("Activate")
			end
	end
end

function ASecJumpMsg(_, data)
local TheSystem, TheSector, TheCharID, TheName
if not TargetActive then return end
TheName, TheSystem, TheSector = string.match(data["msg"], "(%S+) jumped to (%S+) System, Sector (%S+)")
if (string.find(data["msg"]," jumped to ")) then
	if (TheName == TargetName) then
		local msg ="say_group TargetJumpedTo: "..data["msg"] 
		gkpc(msg)
		end
	else
	print("Thename:".. TheName.."   TargetName:"..TargetName)
	end
end 

-------------------------------------------------------------
RegisterUserCommand("a1",AToggle)
RegisterUserCommand("a2",ASetShip)

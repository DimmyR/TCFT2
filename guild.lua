--[[
Guild bank functions
	Lisa added on line 33 the "Applicant" option to the member pull down.
	Seems to work fine if you have this added.  Without it, type shows as ? 
	In the Guild list.
]]

TCFT2.Guild = {}
TCFT2.Guild.MembersFirstUpdate = false

TCFT2.GuildBank = TCFT2.GuildBank or {}
TCFT2.GuildBankLog = TCFT2.GuildBankLog or {}
TCFT2.GuildBankLog.CurrentPage = TCFT2.GuildBankLog.CurrentPage or 1
TCFT2.GuildBankLog.UpdateIdx = 0
TCFT2.GuildBankLog.FirstUpdate = false 

TCFT2.GuildActivity = TCFT2.GuildActivity or {}
TCFT2.GuildActivity.CurrentPage = TCFT2.GuildActivity.CurrentPage or 1
TCFT2.GuildActivity.UpdateIdx = 0
TCFT2.GuildActivity.FirstUpdate = false

TCFT2.GuildMembers = TCFT2.GuildMembers or {} 

TCFT2.GuildBank.PageSize = 15

TCFT2.Guild.MemberRanks = {
	[0] = "Probie",
	[1] = "Member",
	[2] = "Lieutenant",
	[3] = "Council/Lt.",
	[4] = "Council",
	[5] = "Commander",
	[6] = "Applicant"
}

dofile("ui/ui_guild.lua")


TCFT2.GuildBank.UpdateActivity = function()
	TCFT2.GuildBank.Activity:clear()
	if (GetGuildTag()~="") then
		local page = (TCFT2.GuildActivity.CurrentPage * TCFT2.GuildBank.PageSize) - (TCFT2.GuildBank.PageSize - 1)
		TCFT2.GuildBank.ActivityPage.title = "Guild Activity Page " .. TCFT2.GuildActivity.CurrentPage
		TCFT2.GuildActivity.UpdateIdx = 1
		TCFT2.GuildBank.Activity.NUMLIN=2
		TCFT2.GuildBank.Activity:setcell(2, 2, "Updating, please wait...")
		Guild.getactivitylogpage(page)
	else
		TCFT2.GuildBank.Activity.NUMLIN=2
		TCFT2.GuildBank.Activity:setcell(2, 2, "You are not in a guild.")
	end
end

TCFT2.GuildBank.UpdateBankLog = function()
	TCFT2.GuildBank.Log:clear()
	if (GetGuildTag()~="") then
		local page = (TCFT2.GuildBankLog.CurrentPage * TCFT2.GuildBank.PageSize) - (TCFT2.GuildBank.PageSize - 1)
		TCFT2.GuildBank.LogPage.title = "Guild Bank Log Page " .. TCFT2.GuildBankLog.CurrentPage
		TCFT2.GuildBankLog.UpdateIdx = 1
		TCFT2.GuildBank.Log.NUMLIN=2
		TCFT2.GuildBank.Log:setcell(2, 2, "Updating, please wait...")
		Guild.getbanklogpage(page)
	else
		TCFT2.GuildBank.Log.NUMLIN=2
		TCFT2.GuildBank.Log:setcell(2, 2, "You are not in a guild.")
	end
end

TCFT2.GuildBank.DumpToLog = function()
	local cols = TCFT2.GuildBank.Log.NUMCOL - 1
	local rows = TCFT2.GuildBank.Log.NUMLIN
	
	for row = 0, rows do
		local line = ""
		if (TCFT2.GuildBank.LogDumpLinenumbers==true) then
			line = row .. "\t"
		end
		for col = 1, cols do
			line = line .. TCFT2.GuildBank.Log[row..":"..col].."\t" 
		end 
		console_print(line)
	end
end

--[[ Memberlist functions ]]
TCFT2.Guild.SaveMember = function(member)
	TCFT2.SendData({ cmd="1510", member=member })
end

TCFT2.Guild.CreateMember = function()
	TCFT2.EditMemberName.value = ""
	TCFT2.EditMemberJoin.value = "yyyy-mm-dd"
	TCFT2.EditMemberDialogTitle.title="Create new member"
	TCFT2.EditMemberRank.value = 1
	TCFT2.EditMemberDialog.saveid = -1
	PopupDialog(TCFT2.EditMemberDialog, iup.CENTER, iup.CENTER)
end

TCFT2.Guild.EditMember = function()
	local member = TCFT2.Guild.MemberList:selecteditem()
	if (member==nil) then
		TCFT2.MessageDialog("Edit Member", "Select a member in the list for editing.")
		return
	end
	local member = TCFT2.Guild.MemberList:selecteditem() or { id=-1, name="", rank="Probie" }
	if (tonumber(member.id) or -1>0) then
		local mrank = 1
		for idx=0, #TCFT2.Guild.MemberRanks do
			if (TCFT2.Guild.MemberRanks[idx]==member.rank) then
				mrank = idx+1
				break
			end
		end
		TCFT2.EditMemberRank.value = mrank
		TCFT2.EditMemberName.value = member.name or ""
		TCFT2.EditMemberJoin.value = (member.joindate~="0" and member.joindate) or "yyyy-mm-dd"
		TCFT2.EditMemberDialog.saveid = member.id
		TCFT2.EditMemberDialogTitle.title="Edit member"
		PopupDialog(TCFT2.EditMemberDialog, iup.CENTER, iup.CENTER)
	else
		TCFT2.ErrorDialog("Edit Member", "An error occured while trying to edit a member.")
	end
end

TCFT2.Guild.DeleteMember2 = function(id)
	id = tonumber(id) or 0
	TCFT2.SendData({ cmd="1520", memberid=id })
end

TCFT2.Guild.DeleteMember = function()
	local member = TCFT2.Guild.MemberList:selecteditem()
	if (member==nil) then
		TCFT2.MessageDialog("Delete Member", "Select a member in the list to delete.")
		return
	end
	local member = TCFT2.Guild.MemberList:selecteditem()
	if (member~=nil) then
		TCFT2.ConfirmDialog("Delete Member", "Delete member\n" .. TCFT2.colors.RED .. member.name .. TCFT2.colors.NORMAL .. "\nAre you sure?", "Yes", 
			function()
				TCFT2.Guild.DeleteMember2(member.id)
			end,
			"No",
			nil
		)	
	end	
end

TCFT2.Guild.UpdateGuildMembers = function()
	TCFT2.Guild.MemberList:clear()
	-- Fetch members from the server
	TCFT2.SendData({ cmd="1500" })
end

-- Events
TCFT2.GuildBank.EventActivityPage = function(_, params)
	local num = #params
	for i, log in ipairs(params) do
		local act = { date = os.date("%Y-%m-%d %H:%M", log.timestamp), activity=log.activity }
		TCFT2.GuildBank.Activity:additem(act)
	end
	TCFT2.GuildBank.ActivityPrevBtn.active = (TCFT2.GuildActivity.CurrentPage>1 and "YES") or "NO"
	TCFT2.GuildBank.ActivityFirstBtn.active = (TCFT2.GuildActivity.CurrentPage>1 and "YES") or "NO"
	TCFT2.GuildBank.ActivityNextBtn.active = (num>0 and "YES") or "NO"
	if (TCFT2.GuildActivity.UpdateIdx<TCFT2.GuildBank.PageSize) and (#params) then
		Guild.getactivitylogpage(((TCFT2.GuildActivity.CurrentPage * TCFT2.GuildBank.PageSize) - (TCFT2.GuildBank.PageSize - 1)) + TCFT2.GuildActivity.UpdateIdx)
	else
		 TCFT2.GuildBank.Activity:update()
	end
	TCFT2.GuildActivity.UpdateIdx = TCFT2.GuildActivity.UpdateIdx + 1
end

-- One page of bank log
TCFT2.GuildBank.EventBankLogPage = function(_, params)
	--[[
	current: 4239000000
	charname: "electric_sheep"
	previous: 4239002783
	timestamp: 1295207351
	description: "OCD"
	action: "withdrawal"
	]]
	-- console_print("Banklog page ")
	local num = #params
	for i, log in ipairs(params) do
		local amount = log.current - log.previous
		local deposit = (amount>-1 and amount) or 0
		local withdraw = (amount<0 and amount) or 0
		
		local act = { date = os.date("%Y-%m-%d %H:%M", log.timestamp), name=log.charname, desc=log.description, balance=log.current, withdraw=withdraw, deposit=deposit }
		if (amount<0) then
			act.cellfgcolors = {}
			act.cellfgcolors[3]="220 66 66"
		end
		TCFT2.GuildBank.Log:additem(act)
	end
	TCFT2.GuildBank.LogPrevBtn.active = (TCFT2.GuildBankLog.CurrentPage>1 and "YES") or "NO"
	TCFT2.GuildBank.LogFirstBtn.active = (TCFT2.GuildBankLog.CurrentPage>1 and "YES") or "NO"
	TCFT2.GuildBank.LogNextBtn.active = (num>0 and "YES") or "NO"
	
	if (TCFT2.GuildBankLog.UpdateIdx<TCFT2.GuildBank.PageSize) and (#params) then
		Guild.getbanklogpage(((TCFT2.GuildBankLog.CurrentPage * TCFT2.GuildBank.PageSize) - (TCFT2.GuildBank.PageSize - 1)) + TCFT2.GuildBankLog.UpdateIdx)
	else
		 TCFT2.GuildBank.Log:update()
	end
	TCFT2.GuildBankLog.UpdateIdx = TCFT2.GuildBankLog.UpdateIdx + 1
end

RegisterEvent(TCFT2.GuildBank.EventBankLogPage, "GUILD_BANK_LOG")
RegisterEvent(TCFT2.GuildBank.EventActivityPage, "GUILD_ACTIVITY_LOG") 
